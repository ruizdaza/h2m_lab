#!/usr/bin/bash
datapath=/home/teleuser/h2m/tb_desy_202405/data
#corryconfig=prealignment_dut.conf
#corryconfig=alignment_dut.conf
corryconfig=analysis_grazingangles.conf

run=${1}
#latest_telescope=$(find $datapath -name "*tel*" -print0 | xargs -r -0 ls -1 -t | head -n1)
latest_telescope=$(find $datapath -name "*${run}_tel*" -print0 | xargs -r -0 ls -1 -t | head -n1)
latest_h2m=$(find $datapath -name "*${run}_h2m*" -print0 | xargs -r -0 ls -1 -t | head -n1)
#latest_telepix=$(find $datapath -name "*single*${run}*" -print0 | xargs -r -0 ls -1 -t | head -n1)

telescope_run=$(echo $latest_telescope | grep -o "run[0-9]*")
h2m_run=$(echo $latest_h2m | grep -o "run[0-9]*")
#telepix_run=$(echo $latest_telepix | grep -o "run[0-9]*")

#if [[ $telescope_run != $apts_run ]]; then
#    echo "$latest_telescope and $latest_h2m seem to be from different runs" ;
#else
    echo "*********************************************"
    echo "* opening $latest_telescope and $latest_h2m"
    echo "*********************************************"

/home/teleuser/corryvreckan/bin/corry -c ${corryconfig} \
				      -o histogram_file=histograms_${run}_online.root \
				      -o detectors_file=geo/geometry_70rot_aligned.geo \
				      -o detectors_file_updated=geo/trash.geo \
				      -o EventLoaderEUDAQ2:TLU_0.file_name=${latest_telescope} \
				      -o EventLoaderEUDAQ2.file_name=${latest_telescope} \
				      -o EventLoaderEUDAQ2:H2M_0.file_name=${latest_h2m} 
#-o EventLoaderMuPixTelescope.file_name=${latest_telepix}

