[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.128113deg,181.991deg,-0.666751deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -1.10134mm,63.355um,8um
roi = [[620,150],[620,380],[730,380],[730,150]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.814116deg,180.211deg,0.149828deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -166.808um,486.175um,156mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.6653deg,179.75deg,-0.0961996deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -541.45um,93.846um,183mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 179.3deg,85.49deg,89.8deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -6.31194mm,-610.28um,270mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,180deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,349mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.811537deg,179.472deg,-0.127827deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -646.423um,77.775um,376mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.43761deg,181.998deg,-0.178877deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -875.574um,-138.002um,519mm
roi = [[670,150],[670,380],[720,380],[720,150]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

