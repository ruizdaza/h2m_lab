[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.39431deg,182.266deg,-0.643546deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -1.18645mm,67.347um,8um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.273014deg,180.143deg,0.159855deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -178.39um,512.121um,156mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.63029deg,179.833deg,-0.0892668deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -557.998um,125.81um,183mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 180.251deg,0.308996deg,90.5995deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = 1.10984mm,-470.511um,222mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,180deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,246mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.75831deg,179.015deg,-0.0714478deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -650.14um,81.366um,273mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.49594deg,181.82deg,-0.109836deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -1.12331mm,-80.781um,423mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

