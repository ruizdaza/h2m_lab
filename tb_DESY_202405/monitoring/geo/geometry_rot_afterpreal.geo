[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.39431deg,182.266deg,-0.643546deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -1.12048mm,63.678um,8um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.273014deg,180.143deg,0.159855deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -166.62um,487.222um,156mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.63029deg,179.833deg,-0.0892668deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -541.059um,93.379um,183mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 180.769deg,1.50224deg,89.6001deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -6.102mm,1.34705mm,270mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,180deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,349mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.75831deg,179.015deg,-0.0714478deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -646.619um,80.56um,376mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.49594deg,181.82deg,-0.109836deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -872.634um,-136.914um,519mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

