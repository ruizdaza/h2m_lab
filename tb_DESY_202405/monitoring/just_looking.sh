#!/usr/bin/bash
datapath=/home/teleuser/h2m/tb_desy_202405/data
corryconfig=finn_analysis.conf
#corryconfig=prealignment_dut.conf

run=${1}
prevrun=$(($((run))-1))

#latest_telescope=$(find $datapath -name "*tel*" -print0 | xargs -r -0 ls -1 -t | head -n1)
latest_telescope=$(find $datapath -name "*${run}_tel*" -print0 | xargs -r -0 ls -1 -t | head -n1)
latest_h2m=$(find $datapath -name "*${run}_h2m*" -print0 | xargs -r -0 ls -1 -t | head -n1)
#latest_telepix=$(find $datapath -name "*single*${run}*" -print0 | xargs -r -0 ls -1 -t | head -n1)

telescope_run=$(echo $latest_telescope | grep -o "run[0-9]*")
h2m_run=$(echo $latest_h2m | grep -o "run[0-9]*")
#telepix_run=$(echo $latest_telepix | grep -o "run[0-9]*")

#if [[ $telescope_run != $apts_run ]]; then
#    echo "$latest_telescope and $latest_h2m seem to be from different runs" ;
#else
    echo "*********************************************"
    echo "* opening $latest_telescope and $latest_h2m"
    echo "*********************************************"

/home/teleuser/corryvreckan/bin/corry -c ${corryconfig} \
				      -o histogram_file=just_looking_${run}.root \
				      -o detectors_file=geo/align_run_${run}.geo \
				      -o detectors_file_updated=geo/align_run_${run}.geo \
				      -o EventLoaderEUDAQ2:TLU_0.file_name=${latest_telescope} \
				      -o EventLoaderEUDAQ2.file_name=${latest_telescope} \
				      -o EventLoaderEUDAQ2:H2M_0.file_name=${latest_h2m} 
#-o EventLoaderMuPixTelescope.file_name=${latest_telepix}

