# IP-environment variables are set by user/eudet/misc/environments/setup_eudaq2_aida-tlu.sh
# Define port
export RUNCONTROLIP=192.168.22.1
export RPCPORT=44000

killall xterm

# Start Run Control
xterm -T "Run Control" -e '/home/teleuser/tangerine/h2m_2405/soft/eudaq/bin/euRun' &
sleep 2

# Start Logger
xterm -T "Log Collector" -e '/home/teleuser/tangerine/h2m_2405/soft/eudaq/bin/euLog -r tcp://${RUNCONTROLIP}' &
sleep 1

# Start one DataCollector
# name (-t) in conf file
# or: -n TriggerIDSyncDataCollector
xterm -T "Data Collector H2M" -e '/home/teleuser/tangerine/h2m_2405/soft/eudaq/bin/euCliCollector -n DirectSaveDataCollector -t h2m_dc -r tcp://${RUNCONTROLIP}:${RPCPORT}' &
sleep 1
xterm -T "Data Collector ADENIUM" -e '/home/teleuser/tangerine/h2m_2405/soft/eudaq/bin/euCliCollector -n DirectSaveDataCollector -t tel_dc -r tcp://${RUNCONTROLIP}:${RPCPORT}' &
#xterm -T "Data Collector ADENIUM" -e '/opt/eudaq2/bin/euCliCollector -n TriggerIDSyncDataCollector >
sleep 1

# Start TLU Producer
xterm -T "AidaTluProducer" -e '/home/teleuser/tangerine/h2m_2405/soft/eudaq/bin/euCliProducer -n AidaTluProducer -t aida_tlu -r tcp://${RUNCONTROLIP}:${RPCPORT}' &
sleep 1

# Adenium
xterm -T "Adenium" -e '/opt/eudaq2/bin/euCliProducer -n AltelNiProducer -t altel -r tcp://${RUNCONTROLIP}:${RPCPORT}' &
sleep 1

xterm -T "H2M Producer" -e 'ssh root@192.168.22.137 "euCliProducer -n CaribouProducer -t H2M -r tcp://${RUNCONTROLIP}:${RPCPORT}"' &
