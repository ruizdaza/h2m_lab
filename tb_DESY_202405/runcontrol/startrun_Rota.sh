#!/usr/bin/env sh
export BINPATH=/opt/eudaq2/bin/

killall xterm

#$BINPATH/euRun &
xterm -T "Run Control" -e '$BINPATH/euRun -n Ex0RunControl' &
sleep 1
xterm -T "Log Collector" -e '$BINPATH/euLog' &
sleep 1
xterm -T "Stage Producer" -e '$BINPATH/euCliProducer -n PIStageProducer -t Stage' &
