import numpy as np
import matplotlib.pyplot as plt

# Read the data from the text file
with open('tuned_1V2_h2m2_baseline99_thr99.mask', 'r') as file:
    lines = file.readlines()

data = []
for line in lines:
    if not line.startswith('#'):
        line_data = line.split()
        data.append([int(x) for x in line_data])

columns = [row[0] for row in data]
rows = [row[1] for row in data]
tdac_values = [row[4] for row in data]

# Create a matrix to represent the heatmap
heatmap = np.zeros((np.max(rows) + 1, np.max(columns) + 1))

# Update the matrix with masked pixels
masked_pixels = 0
for col, row, tdac in zip(columns, rows, tdac_values):
    if tdac == -1:
        heatmap[row, col] = 1
        masked_pixels += 1

# Plot the heatmap
plt.figure(figsize=(12, 3))
plt.title(f'Total number of masked pixels: {masked_pixels}', fontsize=16)
plt.imshow(heatmap, cmap='binary', origin='lower', aspect='equal')
colorbar = plt.colorbar(label='Masked', cmap='viridis', shrink=1.0)
colorbar.set_label('Masked', fontsize=16)
plt.xlabel('Column', fontsize=16)
plt.ylabel('Row', fontsize=16)

plt.show()
