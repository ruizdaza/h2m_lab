firing_pixels = set()

with open("pixelsfiring_thr102.txt", "r") as f:
    for line in f:
        if "col:" in line and "row:" in line:
            col = int(line.split("col:")[1].split("row:")[0].strip())
            row = int(line.split("row:")[1].strip())
            firing_pixels.add((col, row))


# Mask the pixels in tuned_1V2_h2m2_baseline99_thr105.mask
masked_file_lines = []
with open("tuned_1V2_h2m2_baseline99_thr101.mask", "r") as f:
    for line in f:
        if line.startswith("#"):
            # Skip comments
            masked_file_lines.append(line)
            continue
        parts = line.split()
        col = int(parts[0])
        row = int(parts[1])
        if (col, row) in firing_pixels:
            # Mask the pixel by setting tdac=-1 and mask=1
            parts[3] = "1"
            parts[4] = "-1"
            line = " ".join(parts) + "\n"
        masked_file_lines.append(line)

# Write the masked content back to the file
with open("tuned_1V2_h2m2_baseline99_thr101.mask", "w") as f:
    f.writelines(masked_file_lines)
