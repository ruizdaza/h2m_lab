import uproot
import matplotlib.pyplot as plt
import numpy as np
import os

# Define the folder containing ROOT files
folder_path = "data"

# Get a list of all ROOT files in the folder
root_files = [file for file in os.listdir(folder_path) if file.endswith(".root")]

# Initialize arrays to store accumulated values and bin edges
all_values = []
all_bin_edges = []

# Loop through each ROOT file
for file_name in root_files:
    # Open ROOT file
    file = uproot.open(os.path.join(folder_path, file_name))

    # Get histogram
    hist = file["different_pixels_firing"]

    # Convert histogram to NumPy arrays
    values, bin_edges = hist.to_numpy()

    # Accumulate values and bin edges
    all_values.append(values)
    all_bin_edges.append(bin_edges)

# Stack all values
all_values_stacked = np.sum(all_values, axis=0)

# Calculate bin centers
bin_centers = (all_bin_edges[0][:-1] + all_bin_edges[0][1:]) / 2

# Normalize histograms
norm = np.sum(all_values_stacked) * (bin_centers[1] - bin_centers[0])


# Create a plot using matplotlib
plt.figure(figsize=(8, 6))

# Plot histograms
plt.plot(bin_centers, all_values_stacked, color='red', label='All ROOT Files', drawstyle='steps-mid', zorder=2)

# Fill the area under the curve
plt.fill_between(bin_centers, 0, all_values_stacked, color='red', alpha=1, zorder=1, step='mid')

# Set axis labels
plt.xlabel('Threshold [DAC]', fontsize=16)
plt.ylabel('Number pixels firing', fontsize=16)

# Set axis limits
plt.xlim(99, 109)
plt.ylim(0, 800)  # Adjust the y-axis limit as per your data

# Add legend
#plt.legend(loc='upper right', fontsize='large', frameon=False)

# Show the plot
plt.show()
