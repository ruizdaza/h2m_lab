import uproot
import matplotlib.pyplot as plt
import numpy as np
import os

# Define the folder containing ROOT files
folder_path = "data"

# Get a list of all ROOT files in the folder
root_files = [file for file in os.listdir(folder_path) if file.endswith(".root")]

# Initialize arrays to store accumulated values and bin edges
all_values_firing = []
all_values_masked = []
all_bin_edges = []

# Loop through each ROOT file
for file_name in root_files:
    # Open ROOT file
    file = uproot.open(os.path.join(folder_path, file_name))

    # Get histograms
    hist_firing = file["different_pixels_firing"]
    hist_masked = file["pixel_firing_vs_threshold"]

    # Convert histograms to NumPy arrays
    values_firing, bin_edges = hist_firing.to_numpy()
    values_masked, _ = hist_masked.to_numpy()

    # Accumulate values and bin edges
    all_values_firing.append(values_firing)
    all_values_masked.append(values_masked)
    all_bin_edges.append(bin_edges)

# Stack all values for firing and masked pixels
all_values_firing_stacked = np.sum(all_values_firing, axis=0)
all_values_masked_stacked = np.sum(all_values_masked, axis=0)

# Shift masked pixels by one bin
all_values_masked_shifted = np.roll(all_values_masked_stacked, -1)
all_values_masked_shifted[-1] = 0  # Set the last bin to 0 as it does not have a corresponding previous bin

# Reverse and calculate the cumulative sum for masked pixels
cumulative_masked = np.cumsum(all_values_masked_shifted[::-1])[::-1]

# Sum of firing and masked pixels
sum_firing_masked = all_values_firing_stacked + cumulative_masked

# Calculate bin centers
bin_centers = (all_bin_edges[0][:-1] + all_bin_edges[0][1:]) / 2

# Normalize histograms
norm_firing_masked = np.sum(sum_firing_masked) * (bin_centers[1] - bin_centers[0])

# Create a plot using matplotlib
plt.figure(figsize=(8, 6))

# Plot sum of firing and masked pixels in red
plt.plot(bin_centers, sum_firing_masked, color='red', label='Sum of firing and masked pixels', drawstyle='steps-mid', zorder=2)

# Plot masked pixels in black
plt.plot(bin_centers, cumulative_masked, color='black', label='Pixels masked', drawstyle='steps-mid', zorder=2)

# Fill the area under the curves
plt.fill_between(bin_centers, 0, sum_firing_masked, color='red', alpha=0.5, zorder=1, step='mid')
plt.fill_between(bin_centers, 0, cumulative_masked, color='black', alpha=0.5, zorder=1, step='mid')


file_tuned = uproot.open("tuned_result.root")
h_tuned = file_tuned["different_pixels_firing"]
values_tuned, bin_edges_tuned = h_tuned.to_numpy()
bin_centers_tuned = (bin_edges_tuned[:-1] + bin_edges_tuned[1:]) / 2
norm_tuned = np.sum(values_tuned) * (bin_centers_tuned[1] - bin_centers_tuned[0])
plt.plot(bin_centers_tuned, values_tuned, color='blue', label='Without masking', drawstyle='steps-mid', zorder=2)



# Set axis labels
plt.xlabel('Threshold [DAC]', fontsize=16)
plt.ylabel('Number of pixels', fontsize=16)

# Set axis limits
plt.xlim(99, 109)
plt.ylim(0, 1050)  # Adjust the y-axis limit as per your data

# Add legend
plt.legend(loc='upper right', fontsize='large', frameon=False)

# Show the plot
plt.show()
