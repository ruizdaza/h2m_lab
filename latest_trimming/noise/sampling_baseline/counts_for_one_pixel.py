import uproot
import matplotlib.pyplot as plt
import numpy as np
import glob
import os

# Folder containing the ROOT files
folder_path = "data_thr99_10us/"

# Get all ROOT files in the folder
root_files = glob.glob(os.path.join(folder_path, "tuned_result_*.root"))

# Sort files by their numeric suffix
root_files.sort(key=lambda x: int(os.path.basename(x).split('_')[-1].split('.')[0]))

# Initialize lists to store the means and errors
means = []
errors = []

# Loop over all ROOT files
for file_path in root_files:
    # Open the ROOT file
    file = uproot.open(file_path)

    # Extract the histogram. pixels interesting [3,4], [3,5], [14,3]
    h_hist = file["single_pixel_plots/h_pixel_response_23_10"]
    #h_hist = file["response_sum"]
    #h_hist = file["Pixel Count Sum"]
    # Convert histogram to NumPy arrays
    values, bin_edges = h_hist.to_numpy()

    # Calculate bin centers
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2

    # Calculate the mean of the histogram
    mean = np.sum(bin_centers * values) / np.sum(values)

    # Calculate the error as mean/sqrt(mean)
    error = mean / np.sqrt(mean)

    # Store the mean and error
    means.append(mean)
    errors.append(error)

# Create the x-axis as the measurement number
measurements = np.arange(1, len(means) + 1)

# Create a plot using matplotlib
plt.figure(figsize=(8, 6))

# Plot the means against the measurement numbers with error bars
plt.errorbar(measurements, means, yerr=errors, fmt='o', linestyle='', color='blue', capsize=5, elinewidth=2, markeredgewidth=2)

# Set axis labels
plt.xlabel('Measurement number', fontsize=16)
plt.ylabel('Counts', fontsize=16)

# Set axis limits for better visualization (optional)
plt.xlim(0.5, len(measurements) + 0.5)

# Show the plot
plt.show()
