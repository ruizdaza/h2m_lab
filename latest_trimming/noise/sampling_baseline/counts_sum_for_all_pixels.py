import uproot
import matplotlib.pyplot as plt
import numpy as np
import glob
import os

# Folder containing the ROOT files
folder_path = "data_thr99_10us/"

# Get all ROOT files in the folder
root_files = glob.glob(os.path.join(folder_path, "tuned_result_*.root"))

# Sort files by their numeric suffix
root_files.sort(key=lambda x: int(os.path.basename(x).split('_')[-1].split('.')[0]))

# Initialize lists to store the number of entries and errors
entries = []
errors = []

# Loop over all ROOT files
for file_path in root_files:
    # Open the ROOT file
    file = uproot.open(file_path)

    # Extract the histogram. pixels interesting [3,4], [3,5], [14,3]
    #h_hist = file["single_pixel_plots/h_pixel_response_23_10"]
    #h_hist = file["response_sum"]
    h_hist = file["Pixel Count Sum"]
    # Convert histogram to NumPy arrays
    values, bin_edges = h_hist.to_numpy()

    # Calculate the total number of entries (sum of values)
    total_entries = np.sum(values)

    # Calculate the error as sqrt of the total number of entries
    error = np.sqrt(total_entries)

    # Store the total number of entries and error
    entries.append(total_entries)
    errors.append(error)

# Create the x-axis as the measurement number
measurements = np.arange(1, len(entries) + 1)

# Create a plot using matplotlib
plt.figure(figsize=(8, 6))

# Plot the number of entries against the measurement numbers with error bars
plt.errorbar(measurements, entries, yerr=errors, fmt='o', linestyle='', color='blue', capsize=5, elinewidth=2, markeredgewidth=2)

# Set axis labels
plt.xlabel('Measurement number', fontsize=16)
plt.ylabel('Counts', fontsize=16)

# Set axis limits for better visualization (optional)
plt.xlim(0.5, len(measurements) + 0.5)

# Show the plot
plt.show()
