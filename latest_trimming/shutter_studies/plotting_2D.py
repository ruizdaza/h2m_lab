import uproot
import matplotlib.pyplot as plt
import numpy as np

# Open ROOT files
file_tuned = uproot.open("data_h2m2_1V2_baseline99_kapton_roomT_shutter10us_repe10_fixtrimming/tuned_result.root")
file_small = uproot.open("data_h2m2_1V2_baseline99_kapton_roomT_shutter10us_repe100_fixtrimming/tuned_result.root")
file_large = uproot.open("data_h2m2_1V2_baseline99_kapton_roomT_shutter10us_repe1000_fixtrimming/tuned_result.root")
file_new = uproot.open("data_h2m2_1V2_baseline99_kapton_roomT_shutter10us_repe10000_fixtrimming/tuned_result.root")

# Get histograms
h_tuned = file_tuned["Pixel Count Sum"]
h_small = file_small["Pixel Count Sum"]
h_large = file_large["Pixel Count Sum"]
h_new = file_new["Pixel Count Sum"]

# Convert uproot histograms to NumPy arrays
values_tuned, bin_edges_tuned = h_tuned.to_numpy()
values_small, bin_edges_small = h_small.to_numpy()
values_large, bin_edges_large = h_large.to_numpy()
values_new, bin_edges_new = h_new.to_numpy()

# Define bin centers
bin_centers = (bin_edges_new[:-1] + bin_edges_new[1:]) / 2

# Concatenate all histogram values
values_combined = np.concatenate([values_tuned, values_small, values_large, values_new])

# Determine the maximum frequency value
max_frequency = np.max(values_combined)

# Create a meshgrid for the histogram values
X, Y = np.meshgrid(bin_centers, np.arange(4))

# Transpose values for plotting
Z = np.array([values_tuned, values_small, values_large, values_new])

# Create the 2D histogram plot
plt.figure(figsize=(10, 6))
plt.pcolormesh(X, Y, Z, cmap='viridis', vmax=max_frequency)
cbar = plt.colorbar(label='Frequency')
cbar.set_label('Frequency', fontsize=16)

# Set axis labels
plt.xlabel('Threshold [DAC]', fontsize=16)
plt.ylabel('Repetitions', fontsize=16)

# Set y-axis ticks to show discrete values
plt.yticks(np.arange(4), ['10', '100', '1000', '10000'])

# Set axis limits
plt.xlim(80, 120)

# Baseline
plt.axvline(x=99, color='gray', linestyle='--')

# Show the plot
plt.show()
