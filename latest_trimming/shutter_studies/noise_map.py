import uproot
import matplotlib.pyplot as plt
import numpy as np

# Open the ROOT file and extract the histogram
file = uproot.open("data_h2m2_1V2_baseline99_kapton_roomT_shutter10us_repe10_fixtrimming/tuned_result.root")
histogram = file["noise2D"].to_numpy()

# Access the actual histogram data from the tuple and convert it into a numpy array
histogram_data = np.array(histogram[0])

# Flip x and y axes by transposing the histogram data
histogram_data_flipped = histogram_data.T

# Plot the histogram using Matplotlib
plt.figure(figsize=(12, 3))
plt.imshow(histogram_data_flipped, origin='lower', aspect='equal')
colorbar = plt.colorbar(label='Single-pixel noise [DACs]', cmap='viridis', shrink=1.0)
colorbar.set_label('Single-pixel noise [DAC]', fontsize=16)
plt.xlabel('Row', fontsize=16)  # Flipped, so x-axis label becomes 'Row'
plt.ylabel('Column', fontsize=16)  # Flipped, so y-axis label becomes 'Column'
plt.show()
