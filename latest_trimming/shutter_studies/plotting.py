import uproot
import matplotlib.pyplot as plt
import numpy as np

# Open ROOT files
file_tuned = uproot.open("data_h2m2_1V2_baseline99_kapton_roomT_shutter10us_repe10_fixtrimming/tuned_result.root")
file_small = uproot.open("data_h2m2_1V2_baseline99_kapton_roomT_shutter100us_repe10_fixtrimming/tuned_result.root")
file_large = uproot.open("data_h2m2_1V2_baseline99_kapton_roomT_shutter10us_repe100_10times_fixtrimming/tuned_result.root")
file_new = uproot.open("data_h2m2_1V2_baseline99_kapton_roomT_shutter1000us_repe10_fixtrimming/tuned_result.root")

# Get histograms
h_tuned = file_tuned["different_pixels_firing"]
h_small = file_small["different_pixels_firing"]
h_large = file_large["different_pixels_firing"]
h_new = file_new["different_pixels_firing"]
#different_pixels_firing
#pixel_firing_vs_threshold

# Convert uproot histograms to NumPy arrays
values_tuned, bin_edges_tuned = h_tuned.to_numpy()
values_small, bin_edges_small = h_small.to_numpy()
values_large, bin_edges_large = h_large.to_numpy()
values_new, bin_edges_new = h_new.to_numpy()

# Calculate bin centers
bin_centers_tuned = (bin_edges_tuned[:-1] + bin_edges_tuned[1:]) / 2
bin_centers_small = (bin_edges_small[:-1] + bin_edges_small[1:]) / 2
bin_centers_large = (bin_edges_large[:-1] + bin_edges_large[1:]) / 2
bin_centers_new = (bin_edges_new[:-1] + bin_edges_new[1:]) / 2

# Normalize histograms.  The normalization is done to the total area under the histogram, making the integral of each histogram equal to 1.
norm_tuned = np.sum(values_tuned) * (bin_centers_tuned[1] - bin_centers_tuned[0])
norm_small = np.sum(values_small) * (bin_centers_small[1] - bin_centers_small[0])
norm_large = np.sum(values_large) * (bin_centers_large[1] - bin_centers_large[0])
norm_new = np.sum(values_new) * (bin_centers_new[1] - bin_centers_new[0])

# Create a plot using matplotlib
plt.figure(figsize=(8, 6)) # Modify for different desired figsize

# Plot histograms normalized
#plt.plot(bin_centers_new, values_new/norm_new, color='blue', label='1000 repetitions', drawstyle='steps-mid', zorder=2)
#plt.plot(bin_centers_large, values_large/norm_large, color='seagreen', label='100 repetitionsx10', drawstyle='steps-mid', zorder=2)
#plt.plot(bin_centers_small, values_small/norm_small, color='darkmagenta', label='100 repetitions', drawstyle='steps-mid', zorder=2)
#plt.plot(bin_centers_tuned, values_tuned/norm_tuned, color='red', label='10 repetitions', drawstyle='steps-mid', zorder=2)



plt.plot(bin_centers_new, values_new, color='green', label='1000 µs', drawstyle='steps-mid', zorder=2)
#plt.plot(bin_centers_large, values_large, color='seagreen', label='100 repetitionsx10', drawstyle='steps-mid', zorder=2)
plt.plot(bin_centers_small, values_small, color='blue', label='100 µs', drawstyle='steps-mid', zorder=2)
plt.plot(bin_centers_tuned, values_tuned, color='red', label='10 µs', drawstyle='steps-mid', zorder=2)




# Fill the area under the curves
#plt.fill_between(bin_centers_new, 0, values_new, color='blue', alpha=1, zorder=1, step='mid')
#plt.fill_between(bin_centers_tuned, 0, values_tuned, color='red', alpha=1, zorder=1, step='mid')
#plt.fill_between(bin_centers_small, 0, values_small, color='darkmagenta', alpha=1, zorder=1, step='mid')
#plt.fill_between(bin_centers_large, 0, values_large, color='seagreen', alpha=1, zorder=1, step='mid')

# Set axis labels
plt.xlabel('Threshold [DAC]', fontsize=16)
plt.ylabel('Number of different pixels firing', fontsize=16)

# Set axis limits
#plt.ylim(0, plt.ylim()[1])  # Set y-axis limit to start at 0
#plt.xlim(80, 120)
plt.ylim(0, 80)  # Set y-axis limit to start at 0
plt.xlim(105.5, 109)

# Add legend
plt.legend(loc='upper right', fontsize='large', frameon=False)

# Show the plot
plt.show()
