import uproot
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import ScalarFormatter

calibration_factors = {
    "H2M-8": 31,
    "H2M-8_notp": 31
}

calibration_error = 1  # in e-

files = {
    "H2M-8": {
        0.8: "/Users/atlaslap97/Desktop/h2m_lab/latest_trimming/study_tp/data_h2m8_0V8_ikrum10/tuned_result_tpenable.root",
        1.2: "/Users/atlaslap97/Desktop/h2m_lab/latest_trimming/study_tp/data_h2m8_1V2_ikrum10/tuned_result_tpenable.root",
        2.4: "/Users/atlaslap97/Desktop/h2m_lab/latest_trimming/study_tp/data_h2m8_2V4_ikrum10/tuned_result_tpenable.root",
        3.6: "/Users/atlaslap97/Desktop/h2m_lab/latest_trimming/study_tp/data_h2m8_3V6_ikrum10/tuned_result_tpenable.root"
    },
    "H2M-8_notp": {
        0.8: "/Users/atlaslap97/Desktop/h2m_lab/latest_trimming/study_tp/data_h2m8_0V8_ikrum10/tuned_result.root",
        1.2: "/Users/atlaslap97/Desktop/h2m_lab/latest_trimming/study_tp/data_h2m8_1V2_ikrum10/tuned_result.root",
        2.4: "/Users/atlaslap97/Desktop/h2m_lab/latest_trimming/study_tp/data_h2m8_2V4_ikrum10/tuned_result.root",
        3.6: "/Users/atlaslap97/Desktop/h2m_lab/latest_trimming/study_tp/data_h2m8_3V6_ikrum10/tuned_result.root"
    }
}

fig, ax1 = plt.subplots()
colors = {"H2M-8": "red", "H2M-8_notp": "black"}
markers = {"H2M-8": "o", "H2M-8_notp": "^"}
legend_name = {"H2M-8": "tp enabled", "H2M-8_notp": "tp disabled"}

for module, file_dict in files.items():
    calibration_factor = calibration_factors[module]
    bias_voltages = []
    thl_dispersions = []
    thl_errors = []  # Store errors

    for bias, file_path in file_dict.items():
        root_file = uproot.open(file_path)
        h_tuned = root_file["Pixel Count Sum"]
        values_tuned, bin_edges_tuned = h_tuned.to_numpy()
        bin_centers_tuned = (bin_edges_tuned[:-1] + bin_edges_tuned[1:]) / 2
        mean_tuned = np.average(bin_centers_tuned, weights=values_tuned)
        rms_tuned = np.sqrt(np.average((bin_centers_tuned - mean_tuned) ** 2, weights=values_tuned))

        # Calculate error in RMS
        rms_error = np.sqrt(np.sum(values_tuned * (bin_centers_tuned - mean_tuned) ** 4) / np.sum(values_tuned) - rms_tuned**2) / np.sqrt(len(values_tuned))

        thl_dispersion = rms_tuned * calibration_factor
        thl_error = np.sqrt((rms_error * calibration_factor) ** 2 + calibration_error ** 2)  # Combine errors

        bias_voltages.append(bias)
        thl_dispersions.append(thl_dispersion)
        thl_errors.append(thl_error)

    # Plot
    plt.errorbar(bias_voltages, thl_dispersions, yerr=thl_errors, color=colors[module], label=legend_name[module],
                 marker=markers[module], linestyle="-", markersize=6, capsize=4, capthick=1.5, elinewidth=1.2)

ax1.set_xlabel('|Bias voltage| [V]', fontsize=16)
ax1.set_ylabel('Threshold dispersion [e-]', fontsize=16)
ax1.legend(frameon=False, fontsize=12)

ax1.set_xlim(0.5, 4.0)
ax1.set_ylim(20, 90)
ax1.minorticks_on()
ax1.tick_params(axis='both', which='major', length=6, width=1.5)
ax1.tick_params(axis='both', which='minor', length=3, width=1)
ax1.yaxis.set_major_formatter(ScalarFormatter(useMathText=True))
ax1.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))

fig.subplots_adjust(bottom=0.12)
plt.show()
