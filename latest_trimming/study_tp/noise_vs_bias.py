import uproot
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import ScalarFormatter

# Calibration factors
calibration_factors = {
    "H2M-8": 31,
    "H2M-8_notp": 31
}

calibration_factor_error = 1  # in e-

files = {
    "H2M-8": {
        0.8: "/Users/atlaslap97/Desktop/h2m_lab/tb_DESY_202502/data_h2m8_0V8_ikrum10/tuned_result.root",
        1.2: "/Users/atlaslap97/Desktop/h2m_lab/tb_DESY_202410/data_h2m8_1V2_ikrum10/tuned_result.root",
        2.4: "/Users/atlaslap97/Desktop/h2m_lab/tb_DESY_202410/data_h2m8_2V4_ikrum10/tuned_result.root",
        3.6: "/Users/atlaslap97/Desktop/h2m_lab/tb_DESY_202410/data_h2m8_3V6_ikrum10/tuned_result.root"
    },
    "H2M-8_notp": {
        0.8: "/Users/atlaslap97/Desktop/h2m_lab/latest_trimming/study_tp/data_h2m8_0V8_ikrum10/tuned_result.root",
        1.2: "/Users/atlaslap97/Desktop/h2m_lab/latest_trimming/study_tp/data_h2m8_1V2_ikrum10/tuned_result.root",
        2.4: "/Users/atlaslap97/Desktop/h2m_lab/latest_trimming/study_tp/data_h2m8_2V4_ikrum10/tuned_result.root",
        3.6: "/Users/atlaslap97/Desktop/h2m_lab/latest_trimming/study_tp/data_h2m8_3V6_ikrum10/tuned_result.root"
    }
}

fig, ax1 = plt.subplots()
colors = {"H2M-8": "red", "H2M-8_notp": "black"}
markers = {"H2M-8": "o", "H2M-8_notp": "^"}
legend_name = {"H2M-8": "tp enabled", "H2M-8_notp": "tp disabled"}

for module, file_dict in files.items():
    calibration_factor = calibration_factors[module]
    bias_voltages = []
    mean_noise_values = []
    mean_noise_errors = []

    for bias, file_path in file_dict.items():
        root_file = uproot.open(file_path)
        h_noise = root_file["noise"]

        # Get the histogram data
        values_noise, bin_edges_noise = h_noise.to_numpy()
        bin_centers_noise = (bin_edges_noise[:-1] + bin_edges_noise[1:]) / 2

        # Calculate the mean value of the calibrated noise
        mean_noise_before_calibration = np.average(bin_centers_noise, weights=values_noise)
        mean_noise = mean_noise_before_calibration * calibration_factor

        # Calculate the error in the mean noise
        # Standard deviation of the weighted values as error in mean
        error_mean = np.sqrt(np.average((bin_centers_noise - mean_noise_before_calibration)**2, weights=values_noise))

        # Apply the error propagation formula for the total error
        mean_noise_error = np.sqrt((error_mean * calibration_factor)**2 + (calibration_factor_error)**2)

        # Store the mean value and corresponding bias voltage
        bias_voltages.append(bias)
        mean_noise_values.append(mean_noise)
        mean_noise_errors.append(mean_noise_error)

    # Plot
    ax1.errorbar(bias_voltages, mean_noise_values, yerr=mean_noise_errors, fmt=markers[module],
                 color=colors[module], label=legend_name[module], linestyle="-", markersize=6, capsize=4, capthick=1.5, elinewidth=1.2)

ax1.set_xlabel('|Bias voltage| [V]', fontsize=16)
ax1.set_ylabel('Single-pixel noise [e-]', fontsize=16)

ax1.set_xlim(0.5, 4.0)
ax1.set_ylim(20, 90)
ax1.minorticks_on()
ax1.tick_params(axis='both', which='major', length=6, width=1.5)
ax1.tick_params(axis='both', which='minor', length=3, width=1)
ax1.yaxis.set_major_formatter(ScalarFormatter(useMathText=True))
ax1.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))

ax1.legend(frameon=False, fontsize=12)

fig.subplots_adjust(bottom=0.12)
plt.show()
