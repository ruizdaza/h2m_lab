import uproot
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import matplotlib.ticker as ticker
from matplotlib.ticker import ScalarFormatter

# Open ROOT files
file_tuned = uproot.open("data/tuned_result.root")
file_small = uproot.open("data/noise_scan_0.root")
file_large = uproot.open("data/noise_scan_14.root")


# Get histograms
h_tuned = file_tuned["Pixel Count Sum"]
h_small = file_small["Pixel Count Sum"]
h_large = file_large["Pixel Count Sum"]

# Convert uproot histograms to NumPy arrays
values_tuned, bin_edges_tuned = h_tuned.to_numpy()
values_small, bin_edges_small = h_small.to_numpy()
values_large, bin_edges_large = h_large.to_numpy()

# Calculate bin centers
bin_centers_tuned = (bin_edges_tuned[:-1] + bin_edges_tuned[1:]) / 2
bin_centers_small = (bin_edges_small[:-1] + bin_edges_small[1:]) / 2
bin_centers_large = (bin_edges_large[:-1] + bin_edges_large[1:]) / 2

mean_tuned = np.average(bin_centers_tuned, weights=values_tuned)
rms_tuned = np.sqrt(np.average((bin_centers_tuned - mean_tuned) ** 2, weights=values_tuned))
mean_error = rms_tuned / np.sqrt(np.sum(values_tuned))
rms_error = np.sqrt(np.sum(values_tuned * (bin_centers_tuned - mean_tuned) ** 4) / np.sum(values_tuned) - rms_tuned ** 4) / (2 * rms_tuned * np.sqrt(np.sum(values_tuned)))

# Create a plot using matplotlib
fig, ax1 = plt.subplots()

# Plot histograms
plt.plot(bin_centers_tuned, values_tuned, color='red', label='Equalised', drawstyle='steps-mid', zorder=2)
plt.plot(bin_centers_small, values_small, color='darkmagenta', label='Lowest Trim Value', drawstyle='steps-mid', zorder=2)
plt.plot(bin_centers_large, values_large, color='seagreen', label='Highest Trim Value', drawstyle='steps-mid', zorder=2)

# Fill the area under the curves
plt.fill_between(bin_centers_tuned, 0, values_tuned, color='red', alpha=1, zorder=1, step='mid')
plt.fill_between(bin_centers_small, 0, values_small, color='darkmagenta', alpha=1, zorder=1, step='mid')
plt.fill_between(bin_centers_large, 0, values_large, color='seagreen', alpha=1, zorder=1, step='mid')

# Set axis labels
ax1.set_xlabel('Threshold [THL_DAC]', fontsize=16)
ax1.set_ylabel('Counts', fontsize=16)
ax1.legend(frameon=False, fontsize=12)

# Set axis limits
plt.ylim(0, plt.ylim()[1])  # Set y-axis limit to start at 0
plt.xlim(60, 140)  # Set x-axis limit from 30 to 110. Modify for different baselines.

ax1.minorticks_on()
ax1.tick_params(axis='both', which='major', length=6, width=1.5)  # Customize major ticks
ax1.tick_params(axis='both', which='minor', length=3, width=1)    # Customize minor ticks
ax1.yaxis.set_major_formatter(ScalarFormatter(useMathText=True))
ax1.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))

textstr = '\n'.join((
    r'$\mu=%.3f \pm %.3f$' % (mean_tuned, mean_error),
    r'$\sigma=%.3f \pm %.3f$' % (rms_tuned, rms_error)))
plt.text(0.95, 0.6, textstr, transform=plt.gca().transAxes, fontsize=12,
         verticalalignment='bottom', horizontalalignment='right', color='red')
# Add legend
#plt.legend(loc='upper right', fontsize='large')

# Show the plot
plt.show()
