# IP-environment variables are set by user/eudet/misc/environments/setup_eudaq2_aida-tlu.sh 
# Define port
export RUNCONTROLIP=131.169.133.67
export RPCPORT=44000

killall xterm

# Start Run Control
xterm -T "Run Control" -e '~/eudaq/bin/euRun' &
sleep 2

# Start Logger
 xterm -T "Log Collector" -e '~/eudaq/bin/euLog -r tcp://${RUNCONTROLIP}' &
sleep 1

# Start one DataCollector
# name (-t) in conf file
# or: -n TriggerIDSyncDataCollector
xterm -T "Data Collector H2M" -e '~/eudaq/bin/euCliCollector -n DirectSaveDataCollector -t h2m_dc -r tcp://${RUNCONTROLIP}:${RPCPORT}' &
sleep 1

xterm -T "H2M Producer" -e 'ssh root@131.169.133.243 "euCliProducer -n CaribouProducer -t H2M -r tcp://${RUNCONTROLIP}:${RPCPORT}"' &
