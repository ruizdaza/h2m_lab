## Running EUDAQ in the lab
Modify the IP of your computer in setup.sh and H2M.ini

```
bash setup.sh
bash H2M.conf
```
This starts EUDAQ. Load the initialization and config files for H2M: H2M.ini, H2M.conf.
Now you can adquire data in the lab!



# Notes from Finn:
- Using H2M3, 1.2 V, trimmed target 65, threshold 78, ikrum 21
- Logic setup
    -Pulse generator provides trigger signals
    -Discriminator discriminates
    -3 fold logic unit applies veto from caribou busy
        - the busy comes from caribou, via level adapter (TTL to NIM)
        - 1 output to per-scaler (factor 2) and to the Laser via level adapter (NIM to TTL)
        - 1 output to caribou, via level adapter and 64 ns delay (NIM to TTL)
    - Mimic t0 by sending single gate at run start before switching trigger output on
    - Delays:
        - Discriminator threshold 100 mV (VM saying 1V), width is 260 ns
        - Diode signal comes after 120 ns
        - Caribou busy comes after 220 ns
    - Measurements old pattern generator (pg_problems.conf)
        - Square trigger input
            - 200 Hz -> 507, 10k events
            - 500 Hz -> 508, 10k events
            - 1000 Hz -> 512, 10k events
        - Noise based triggers, random time distribution
            -  800 Hz (179 mV) -> 514 10k events
            -  240 Hz (174 mV) -> 515 10 k events (14.26 Uhr)
    - Measurements new pattern generator (pg_solution.conf)
        - Noise based triggers, random time distribution
            - 240 Hz (174 mV) -> 516 10k events
            - 800 Hz (179 mV) -> 517 10k events
        - Square trigger input
            - 200 Hz -> 518 10k events (14.45 Uhr)
            - 500 Hz -> 519 10k events
            - 1000 Hz -> 520 10k events
