from os import remove
from os.path import join, basename, splitext, exists
from subprocess import run


class NoiseScan:
    def __init__(self, scan_number):
        # Initialize the paths to the specified files
        self.scan_number = scan_number
        self.config_file_path = join("config", "config.cfg")
        self.run_file_path = join("script", "take_data.txt")
        self.executable = "pearycli"

        # create a temporary run file with the modified threshold
        self.update_configfile()


    def display(self):
        print(f"Running for scan number {self.scan_number}: ")
        print(f"- Config File Path: {self.config_file_path}")
        print(f"- Run File Path: {self.run_file_path}")
        print(f"- Executable: {self.executable}")


    def update_configfile(self):
        with open(self.config_file_path, 'r') as file:
            lines = file.readlines()

        for index, line in enumerate(lines):
            if "dac_vthr = " in line:
                lines[index] = f"dac_vthr = {self.scan_number}\n"

        for index, line in enumerate(lines):
            if "occupancy_filename" in line:
                lines[index] = f"occupancy_filename = occupancy_filename_{self.scan_number}\n"

        # write to /tmp
        config_file_name, ext = splitext(basename(self.config_file_path))
        output_path = join('/tmp', f"{config_file_name}_{self.scan_number}{ext}")
        with open(output_path, 'w') as file:
            file.writelines(lines)
        self.config_file_path = output_path


    def run(self):
        # show paths before running
        self.display()
        # run command
        command = [self.executable, "-r", self.run_file_path, "-c", self.config_file_path]
        run(command)
        # clean up after running
        if exists(self.config_file_path) and '/tmp' in self.config_file_path:
            remove(self.config_file_path)


def main(args=None):

    noise_scans = [NoiseScan(i) for i in range(115, 170)]

    # run jobs
    for scan in noise_scans:
        scan.run()


if __name__ == "__main__":
    main()

