add_device H2M
powerOn 0
setVoltage v_pwell 0 0
setVoltage v_sub 0 0
configure 0

setRegister conf 1 0

# which is which?
setRegister tp_polarity 1 0
getRegister tp_polarity 0

setRegister link_shutter_tp 1 0 

setRegister shutter_tp_lat 0x8 0
getRegister shutter_tp_lat 0

#number of pulses
setRegister tp_num 4 0
getRegister tp_num 0

setRegister tp_on 0x5 0
getRegister tp_on 0

setRegister tp_off 0x5 0
getRegister tp_off 0

setRegister dac_vtpulse 0xFF 0
getRegister dac_vtpulse 0

#counting mode
setRegister acq_mode 2 0

# replace with proper mask
setRegister px_cnt_0 0xBDBDBDBD 0
setRegister px_cnt_0 0xBDBDBDBD 0
setRegister px_cnt_0 0xBDBDBDBD 0
setRegister px_cnt_0 0xBDBDBDBD 0

setRegister conf 0 0 

getRegister px_cnt_0 0
getRegister px_cnt_0 0
getRegister px_cnt_0 0
getRegister px_cnt_0 0


setRegister acq_start 1 0

setRegister matrix_ctrl 1 0
#define number of 100MHz clk cycles to keep the shutter open 
setMemory shutter 150 0
setRegister acq_start 0 0

#get all Data later
getRegister px_cnt_0 0
getRegister px_cnt_0 0
getRegister px_cnt_0 0
getRegister px_cnt_0 0

