import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys

def process_data(filename):
    # read data from the given filename
    data = pd.read_excel(filename)

    # take absolute values
    data['voltage'] = abs(data['voltage'])
    data['Current'] = abs(data['Current'])

    return data

# define ploting function
def plot_data(voltage, current, label, linestyle, color):
    plt.plot(voltage, current, linestyle, label=label, color=color, markersize=3)

def main():
    if len(sys.argv) < 2: # error message if no files provided as argument
        print("Usage: python script.py filename_h2m2.xlsx [filename_h2m3.xlsx ...]")
        sys.exit(1)

    filenames = sys.argv[1:]
    colors = ['r', 'b', 'g', 'c', 'm', 'y', 'k']  # add more colors if needed

    for i, filename in enumerate(filenames):
        data = process_data(filename)
        plot_label = filename.split('.xlsx')[0]  # extracting label from filename

        color = colors[i]

        plot_data(data['voltage'], data['Current'], plot_label, 'o-', color)

    plt.xlabel('|bias substrate voltage| (V)', fontsize=16)
    plt.ylabel('|substrate current| (µA)', fontsize=16)
    plt.legend(fontsize=12, loc = 'upper left')
    plt.show()

if __name__ == "__main__":
    main()
