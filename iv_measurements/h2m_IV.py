import numpy as np
from Keithley import KeithleySMU2400Series
import yaml
import time
import sys
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from random import randrange
from datetime import datetime



def main():
        # open configuration file
        confFileName = "config_keithley__IV.yaml"
        print(confFileName)
        configuration_file = ''
        with open(confFileName, 'r') as file:
            configuration_file = yaml.load(file, Loader=yaml.SafeLoader)

        # connect to Keithley
        vsource = KeithleySMU2400Series(configuration_file)
        print("Keithley Initialised.\n")
        startTime = time.time()

        # voltage from config file
        vstart = configuration_file["SetVoltage"]["V_Start"]
        vstep  = configuration_file["SetVoltage"]["V_Step"]
        vstep_back  = configuration_file["SetVoltage"]["V_Step_Back"]
        vset   = configuration_file["SetVoltage"]["V_Set"]
        twait  = configuration_file["SetVoltage"]["t_Wait"]
        t_Wait_1st = configuration_file["SetVoltage"]["t_Wait_1st"]
        IV_Filename = configuration_file["H2M"]["IV_Filename"]

        try:
            # set V_Start
            vsource.disable_output()
            vsource.set_voltage(vstart, unit='V')
            vsource.enable_output()

        except ValueError:
            exit()



        # take first measurement
        print( "  Waiting " + str(t_Wait_1st) + "s after 1st Voltage")
        time.sleep(t_Wait_1st)
        d = vstart
        current = vsource.get_current()
        runtime = time.time() - startTime
        print( "  Running for " + str('{:6.2f}'.format(runtime)) + "s, voltage " + str('{:6.2f}'.format(d)) + " V, measured current " + str('{:.5f}'.format(current[0]))  + " uA ")
        #log.write( str('{:6.2f}'.format(runtime)) + " " + str(d) + " " + str(current[0]) + " " + str(current[1]) + "\n" )

        #plot
        fig = plt.figure(figsize=(6, 3))
        xp: float = []
        yp: float = []

        #outfile
        f = open(IV_Filename, "a")

        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

        f.write('Current Time = ' + dt_string + '\n')
        f.write( 'time' + '\t' + 'voltage' + '\t' + 'voltage_err' + '\t' + 'Current' + '\t' + 'Current_err' + '\n' )

        try:
            print("  Voltage Ramp for IV measurement. If the script stops here, the output is turned off.")

            d = vstart
            print("  Ramping output from " + str('{:6.2f}'.format(d)) + "V to " + str(vset) + "V")

            while( (d-vstep) >= vset ): # Ramping up
                try:
                    vsource.set_voltage(d-vstep, unit='V')
                    d = vsource.get_voltage()[0]
                    print("Setting voltage " , d)
                    print( "  Waiting " + str(twait) + "s after new voltage")
                    time.sleep(twait)

                    runtime = time.time() - startTime
                    print( "  Running for " + str('{:6.2f}'.format(runtime)+ '\t') + "s, voltage " + str('{:6.2f}'.format(vsource.get_voltage()[0])) + " V, measured current " + str('{:.5f}'.format(vsource.get_current()[0]))  + " uA ")
                    now = datetime.now()
                    f.write( now.strftime("%H:%M:%S") + '\t' + str(vsource.get_voltage()[0]) + '\t' + str(vsource.get_voltage()[1]) + '\t' + str(vsource.get_current()[0]) + '\t' + '\t' + str(vsource.get_current()[1]) + '\n' )

                    xp.append(float(vsource.get_voltage()[0]))
                    yp.append(float(vsource.get_current()[0]))


                    ln, = plt.plot(xp, yp, 'b-')
                    ln.set_data(xp, yp)
                    plt.draw()
                    plt.pause(0.001)

                except ValueError:
                    print("  Error occured, check comliance and voltage.")
                    vsource.set_voltage(   0,unit='V')
                    raise ValueError("Voltage ramp failed")
                except KeyboardInterrupt:
                    vsource.vramp(vstart, vstep, 'V')
                    log.close()
                    break

        except ValueError:
            exit()

        f.close()
        vsource.vramp(0.0, vstep_back, 'V')
        vsource.disable_output()



        print("wait 2 sec before closing")
        time.sleep(2)

        fig.savefig(IV_Filename + '.pdf')

if __name__ == '__main__':
    import sys
    main()
