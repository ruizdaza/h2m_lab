import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# read data
data_h2m2 = pd.read_excel('H2M2.xlsx')
data_h2m3 = pd.read_excel('H2M3.xlsx')

# take absolute values
data_h2m2['voltage'] = abs(data_h2m2['voltage'])
data_h2m2['Current'] = abs(data_h2m2['Current'])

data_h2m3['voltage'] = abs(data_h2m3['voltage'])
data_h2m3['Current'] = abs(data_h2m3['Current'])

# iv plot with smooth line between points
plt.plot(data_h2m2['voltage'], data_h2m2['Current'], 'ko-', label='H2M-2', markersize=3)
plt.fill_between(data_h2m2['voltage'], data_h2m2['Current'] - data_h2m2['Current_err'], data_h2m2['Current'] + data_h2m2['Current_err'], alpha=0.2, color='r')

plt.plot(data_h2m3['voltage'], data_h2m3['Current'], 'ko--', label='H2M-3', markersize=3)
plt.fill_between(data_h2m3['voltage'], data_h2m3['Current'] - data_h2m3['Current_err'], data_h2m3['Current'] + data_h2m3['Current_err'], alpha=0.2, color='b')

# labels and legend
plt.xlabel('|bias voltage| (V)', fontsize=16)
plt.ylabel('|current| (µA)', fontsize=16)

plt.legend(fontsize=16)

# calculate the relative derivative
derivative_h2m2 = np.gradient(data_h2m2['Current'], data_h2m2['voltage'])
derivative_h2m3 = np.gradient(data_h2m3['Current'], data_h2m3['voltage'])

# calculate k-value
k_h2m2 = derivative_h2m2 * data_h2m2['voltage']/data_h2m2['Current']
k_h2m3 = derivative_h2m3 * data_h2m3['voltage']/data_h2m3['Current']

# add labels to the right y-axis
plt.ylabel('|current| (µA)', fontsize=16)

# create the second y-axis on the right side
ax2 = plt.gca().twinx()
ax2.plot(data_h2m2['voltage'], k_h2m2, 'r-', linewidth=2, label='k-value (H2M-2)')
ax2.plot(data_h2m3['voltage'], k_h2m3, 'r--', linewidth=2, label='k-value (H2M-3)')
ax2.set_ylabel('k-value', fontsize=16, color='red')
ax2.set_ylim(0, 15)

# set the color of the y-axis tick labels to red
for label in ax2.get_yticklabels():
    label.set_color('red')

# add dashed line at k=4
ax2.axhline(4, color='gray', linestyle='dashed', label='k-value = 4')

plt.savefig('iv_der.pdf', format='pdf')

plt.show()
