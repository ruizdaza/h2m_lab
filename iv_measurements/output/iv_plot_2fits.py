import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# read data
data_h2m2 = pd.read_excel('H2M2.xlsx')
data_h2m3 = pd.read_excel('H2M3.xlsx')

# take absolute values
data_h2m2['voltage'] = abs(data_h2m2['voltage'])
data_h2m2['Current'] = abs(data_h2m2['Current'])

data_h2m3['voltage'] = abs(data_h2m3['voltage'])
data_h2m3['Current'] = abs(data_h2m3['Current'])


# regions for fits in H2M-2
region_h2m2_fit_1 = (data_h2m2['voltage'] >= 0.5) & (data_h2m2['voltage'] <= 2.5)
fit_h2m2_1 = np.polyfit(data_h2m2['voltage'][region_h2m2_fit_1], data_h2m2['Current'][region_h2m2_fit_1], 1)

region_h2m2_fit_2 = (data_h2m2['voltage'] >= 5.7) & (data_h2m2['voltage'] <= 6.0)
fit_h2m2_2 = np.polyfit(data_h2m2['voltage'][region_h2m2_fit_2], data_h2m2['Current'][region_h2m2_fit_2], 1)

# the intersection defines the breakdown voltage
intersection_voltage_h2m2 = (fit_h2m2_2[1] - fit_h2m2_1[1]) / (fit_h2m2_1[0] - fit_h2m2_2[0])

# regions for fits in H2M-3
region_h2m3_fit_1 = (data_h2m3['voltage'] >= 0.5) & (data_h2m3['voltage'] <= 2.5)
fit_h2m3_1 = np.polyfit(data_h2m3['voltage'][region_h2m3_fit_1], data_h2m3['Current'][region_h2m3_fit_1], 1)

region_h2m3_fit_2 = (data_h2m3['voltage'] >= 5.7) & (data_h2m3['voltage'] <= 6.0)
fit_h2m3_2 = np.polyfit(data_h2m3['voltage'][region_h2m3_fit_2], data_h2m3['Current'][region_h2m3_fit_2], 1)

# the intersection defines the breakdown voltage
intersection_voltage_h2m3 = (fit_h2m3_2[1] - fit_h2m3_1[1]) / (fit_h2m3_1[0] - fit_h2m3_2[0])


# iv plot with smooth line between points
plt.plot(data_h2m2['voltage'], data_h2m2['Current'], 'ro-', label=f'H2M-2, $V_{{BD}}$ = {intersection_voltage_h2m2:.2f} V', markersize=3)
plt.fill_between(data_h2m2['voltage'], data_h2m2['Current'] - data_h2m2['Current_err'], data_h2m2['Current'] + data_h2m2['Current_err'], alpha=0.2, color='r')

plt.plot(data_h2m3['voltage'], data_h2m3['Current'], 'bo-', label=f'H2M-3, $V_{{BD}}$ = {intersection_voltage_h2m3:.2f} V', markersize=3)
plt.fill_between(data_h2m3['voltage'], data_h2m3['Current'] - data_h2m3['Current_err'], data_h2m3['Current'] + data_h2m3['Current_err'], alpha=0.2, color='b')

# labels and legend
plt.xlabel('|bias voltage| (V)', fontsize=16)
plt.ylabel('|current| (µA)', fontsize=16)

plt.legend(fontsize=16)

# print the breakdown voltage in the terminal
print("Breakdown point for H2M-1:", intersection_voltage_h2m2)
print("Breakdown point for H2M-2:", intersection_voltage_h2m3)

# plot the lineal fits in black
plt.plot(data_h2m2['voltage'][region_h2m2_fit_1], np.polyval(fit_h2m2_1, data_h2m2['voltage'][region_h2m2_fit_1]), 'k-', linewidth=2)
plt.plot(data_h2m2['voltage'][region_h2m2_fit_2], np.polyval(fit_h2m2_2, data_h2m2['voltage'][region_h2m2_fit_2]), 'k-', linewidth=2)
plt.plot(data_h2m3['voltage'][region_h2m3_fit_1], np.polyval(fit_h2m3_1, data_h2m2['voltage'][region_h2m3_fit_1]), 'k-', linewidth=2)
plt.plot(data_h2m3['voltage'][region_h2m3_fit_2], np.polyval(fit_h2m3_2, data_h2m2['voltage'][region_h2m3_fit_2]), 'k-', linewidth=2)

plt.savefig('iv.pdf', format='pdf')

plt.show()

