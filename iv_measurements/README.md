## IV measurements

# For a Keithley connected via USB

1. Open ```config_keithley__IV.yaml```. Modify if needed the connection port for the Keithley. Parameters such as the voltage, number of measurements per point or range or steps in the measurements can also be modified.  

2. Run the iv-measeurement:
 ```
 (sudo) python3 h2m_IV.py
 ```

 Now, the H2M.txt file should have been created. It contains a timestamp, the voltage, error in voltage, current and error in current. The errors are obtained from the STD of several measurements.


3. Since the columns of the txt file could be wrongly split, the txt file can be copied and and saved into an excel: ```h2m.xlsx```. Now the columns should be well defined and you can use the following to plot:
```
python3 plotting.py h2m.xlsx
```

Also, different files can be plotted together using:
```
python3 plotting.py h2m_1.xlsx h2m_2.xlsx [h2m_3.xlsx ...]
```


Credits to Gianpiero Vignola, contact him for questions
