import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import os

#######DEFINE THE BASELINE TARGET OBTAINED WITH equalize.py#########
target_baseline = 63

# gaussian function for fitting
def gaussian(x, amp, mean, sigma):
    return amp * np.exp(-(x - mean)**2 / (2 * sigma**2))

def process_data(filename):
    with open(filename, "r") as file:
        lines = file.readlines()

    data = []
    for line in lines:
        if line.startswith("occupancy"):
            continue

        values = line.strip().split()
        numeric_values = []
        for value in values:
            try:
                numeric_values.append(int(value))
            except ValueError:
                numeric_values.append(value)  #the string dac_name mesh up things
        data.append(numeric_values)

    dac_values = []
    num_pixels_firing = []

    for row in data:
        pixel_firing_counts = row[:1024]
        num_pulses = row[1028] #this is 100
        dac_value = row[1024]
        if isinstance(pixel_firing_counts[0], int):  #the string dac_name mesh up things
            num_firing_pixels = sum(pixel_firing_counts)
            num_pixels_firing.append(num_firing_pixels)
            dac_values.append(dac_value)

    return dac_values, num_pixels_firing, data


# read and process the data from the low and high local thresholds
dac_values_1, num_pixels_firing_1, data_1 = process_data("data_1.2V/noise_scan_1.txt")
dac_values_14, num_pixels_firing_14, data_14 = process_data("data_1.2V/noise_scan_14.txt")

plt.bar(dac_values_14, num_pixels_firing_14, align='center', label = 'DAC = 14')
plt.bar(dac_values_1, num_pixels_firing_1, align='center', label = 'DAC = 1')
plt.xlabel('DAC_VTHR')
plt.ylabel('Number of Pixels Firing')
plt.legend()
plt.show()

# get turn-on point for each pixel at each distributions
#data_0 is the first dac scan
# data_0[0] is the first value of DAC_VTHR in the first dac scan
# data_0[0][0] is the value of the first pixel in the ...
#data_{DAC}[{DAC_VTHR}][{pixel}]

# Create an output directory if it doesn't exist
output_directory = 'output_plots_1-14'
os.makedirs(output_directory, exist_ok=True)

output_directory_1 = 'output_plots_meanVsDAC_1-14'
os.makedirs(output_directory_1, exist_ok=True)


# Define the number of pixels, rows, and columns per canvas
num_pixels = 1024
pixels_per_canvas = 16
num_canvases = num_pixels // pixels_per_canvas
canvas_rows = 4
canvas_cols = 4
mean_1_values = []
sigma_1_values = []
mean_14_values = []
sigma_14_values = []
trimming_dac_values = []
# Loop through each canvas
for canvas_index in range(num_canvases):
    fig, axes = plt.subplots(canvas_rows, canvas_cols, figsize=(15, 10))
    fig.tight_layout()

    # loop through each pixel in the current canvas
    for relative_pixel_index in range(pixels_per_canvas):
        pixel_index = canvas_index * pixels_per_canvas + relative_pixel_index


        pixel_counts_1 = [row[pixel_index] for row in data_1]

        pixel_counts_14 = [row[pixel_index] for row in data_14]


        dac_values_1 = [row[1024] for row in data_1]

        dac_values_14 = [row[1024] for row in data_14]


        row = relative_pixel_index // canvas_cols
        col = relative_pixel_index % canvas_cols
        ax = axes[row, col]

        ax.plot(dac_values_1, pixel_counts_1, label='1')

        ax.plot(dac_values_14, pixel_counts_14, label='14')
        ax.set_title(f'Pixel {pixel_index}')
        ax.set_xlabel('DAC_VTHR')
        ax.set_ylabel('Number of times firing')
        ax.legend()

        # fit gaussians
        try:

            params_1, _ = curve_fit(gaussian, dac_values_1, pixel_counts_1, p0=[1, np.mean(dac_values_1), np.std(dac_values_1)])
            params_14, _ = curve_fit(gaussian, dac_values_14, pixel_counts_14, p0=[1, np.mean(dac_values_14), np.std(dac_values_14)])


            # save mean, sigma

            mean_1_values.append(params_1[1])
            sigma_1_values.append(params_1[2])
            mean_14_values.append(params_14[1])
            sigma_14_values.append(params_14[2])



            # calculate chi_squared/ndof for each gaussian fit


            chi_squared_1 = np.sum((pixel_counts_1 - gaussian(dac_values_1, *params_1)) ** 2)
            ndof_1= len(dac_values_1) - len(params_1)
            chi_ndof_1 = chi_squared_1 / ndof_1
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_1} (1 Local Threshold)')



            chi_squared_14 = np.sum((pixel_counts_14 - gaussian(dac_values_14, *params_14)) ** 2)
            ndof_14= len(dac_values_14) - len(params_14)
            chi_ndof_14 = chi_squared_14 / ndof_14
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_14} (14 Local Threshold)')


            # display mean, sigma, and chi_squared/ndof values in the legend
            #legend_text = f'Low Local THR: $\mu$={params_low[1]:.2f}, $\sigma$={params_low[2]:.2f}, $\chi^2/ndof$={chi_ndof_low:.2f}\nHigh Local THR: $\mu$={params_high[1]:.2f}, $\sigma$={params_high[2]:.2f}, $\chi^2/ndof$={chi_ndof_high:.2f}'
            #ax.legend(title='Gaussian Fits', fontsize='small', labelspacing=0.8, borderpad=0.8).set_title(legend_text)


        except RuntimeError as e:
            print(f"Fitting failed for pixel {pixel_index}: {e}")

            mean_1_values.append(0)
            sigma_1_values.append(0)

            mean_14_values.append(0)
            sigma_14_values.append(0)

            continue  # Move to the next pixel

    #Save the current canvas to the output directory
    canvas_filename = os.path.join(output_directory, f'column_{canvas_index + 1}.png')
    plt.savefig(canvas_filename)
    plt.close()


import matplotlib.pyplot as plt


# Create a list of the mean arrays
mean_arrays = [mean_1_values, mean_14_values]





for canvas_index in range(num_canvases):
    fig, axes = plt.subplots(canvas_rows, canvas_cols, figsize=(15, 10))
    fig.tight_layout()

    # loop through each pixel in the current canvas
    for relative_pixel_index in range(pixels_per_canvas):
        pixel_index = canvas_index * pixels_per_canvas + relative_pixel_index

        mean_values_pixel = []  # Collect mean values for each DAC scan

        # Loop through all DAC scans and collect mean values for the current pixel
        for data in [ data_1, data_14]:
            pixel_counts = [row[pixel_index] for row in data]
            dac_values = [row[1024] for row in data]

            try:
                params, _ = curve_fit(gaussian, dac_values, pixel_counts, p0=[1, np.mean(dac_values), np.std(dac_values)])
                mean_values_pixel.append(params[1])
            except RuntimeError:
                mean_values_pixel.append(0)  # Fitting failed

        row = relative_pixel_index // canvas_cols
        col = relative_pixel_index % canvas_cols
        ax = axes[row, col]
        ax.plot([1, 13], [mean_values_pixel[0], mean_values_pixel[1]], color='red', linestyle='-', linewidth=2) # correcting for 2 identical values
        ax.plot([2, 14], [mean_values_pixel[0], mean_values_pixel[1]], color='red', linestyle='-', linewidth=2) # correcting for 2 identical values
        x_intersection_1 = (target_baseline - mean_values_pixel[0]) * (13 - 1) / (mean_values_pixel[1] - mean_values_pixel[0]) + 1
        x_intersection_2 = (target_baseline - mean_values_pixel[0]) * (14 - 2) / (mean_values_pixel[1] - mean_values_pixel[0]) + 2
        print(f"Pixel {pixel_index}: Intersection points: x1 = {x_intersection_1:.2f}, x2 = {x_intersection_2:.2f}")
        denominator = mean_values_pixel[1] - mean_values_pixel[0]
        if denominator != 0:
            x_intersection_1 = (target_baseline - mean_values_pixel[0]) * (13 - 1) / denominator + 1
            x_intersection_2 = (target_baseline - mean_values_pixel[0]) * (14 - 2) / denominator + 2
            if not np.isnan(x_intersection_1) and not np.isnan(x_intersection_2):
                trimming_dac = x_intersection_1 if x_intersection_1 <= 7 else x_intersection_2
            else:
                trimming_dac = 7 # if the calculation of the intersection fails, set trimming dac to 7
        else:
            trimming_dac = 7 # if there's an error calculating the intersection, set trimming dac to 7

        # check if the trimming_dac is within the valid DAC range
        if trimming_dac < -0.5:
            trimming_dac = 0
        if trimming_dac > 15.5:
            trimming_dac = 15

        trimming_dac_values.append(round(trimming_dac))
        print(f"trimming dac {round(trimming_dac)}")
        ax.plot([1,14], mean_values_pixel, marker='o', linestyle='-', color='blue')
        ax.set_title(f'Pixel {pixel_index}')
        ax.set_xlabel('DAC Index')
        ax.set_ylabel('Mean Value')
        print(f'plotting Pixel {pixel_index}')

    # add horizontal gray line for target baseline
    for ax_row in axes:
        for ax_single in ax_row:
            ax_single.axhline(y=target_baseline, color='gray', linestyle='dashed', label="Target baseline", alpha=0.5)






    # Save the current canvas to the output directory
    canvas_filename = os.path.join(output_directory_1, f'column_{canvas_index + 1}.png')
    plt.savefig(canvas_filename)
    plt.close()



pixel_index = 0

print(trimming_dac_values)
with open("tuned_fast.mask", "w") as output_file:
    output_file.write("# col row tb_enable mask tdac not top\n")

    for row in range(16):
        for col in range(64):
            print(pixel_index)
            output_line = f"{col} {row} 1 0 {trimming_dac_values[pixel_index]} 0\n"
            output_file.write(output_line)
            pixel_index = pixel_index + 1


# read the contents of the two mask files with the two methods investigated:
# - having all the DACs distributions
# - only accesing the DAC 1 and 14, and extrapolate the rest.
