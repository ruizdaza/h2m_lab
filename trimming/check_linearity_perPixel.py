import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import os

#######DEFINE THE BASELINE TARGET OBTAINED WITH equalize.py#########
target_baseline = 63

# gaussian function for fitting
def gaussian(x, amp, mean, sigma):
    return amp * np.exp(-(x - mean)**2 / (2 * sigma**2))

def process_data(filename):
    with open(filename, "r") as file:
        lines = file.readlines()

    data = []
    for line in lines:
        if line.startswith("occupancy"):
            continue

        values = line.strip().split()
        numeric_values = []
        for value in values:
            try:
                numeric_values.append(int(value))
            except ValueError:
                numeric_values.append(value)  #the string dac_name mesh up things
        data.append(numeric_values)

    dac_values = []
    num_pixels_firing = []

    for row in data:
        pixel_firing_counts = row[:1024]
        num_pulses = row[1028] #this is 100
        dac_value = row[1024]
        if isinstance(pixel_firing_counts[0], int):  #the string dac_name mesh up things
            num_firing_pixels = sum(pixel_firing_counts)
            num_pixels_firing.append(num_firing_pixels)
            dac_values.append(dac_value)

    return dac_values, num_pixels_firing, data


# read and process the data from the low and high local thresholds
dac_values_0, num_pixels_firing_0, data_0 = process_data("data_1.2V/noise_scan_0.txt")
dac_values_1, num_pixels_firing_1, data_1 = process_data("data_1.2V/noise_scan_1.txt")
dac_values_2, num_pixels_firing_2, data_2 = process_data("data_1.2V/noise_scan_2.txt")
dac_values_3, num_pixels_firing_3, data_3 = process_data("data_1.2V/noise_scan_3.txt")
dac_values_4, num_pixels_firing_4, data_4 = process_data("data_1.2V/noise_scan_4.txt")
dac_values_5, num_pixels_firing_5, data_5 = process_data("data_1.2V/noise_scan_5.txt")
dac_values_6, num_pixels_firing_6, data_6 = process_data("data_1.2V/noise_scan_6.txt")
dac_values_7, num_pixels_firing_7, data_7 = process_data("data_1.2V/noise_scan_7.txt")
dac_values_8, num_pixels_firing_8, data_8 = process_data("data_1.2V/noise_scan_8.txt")
dac_values_9, num_pixels_firing_9, data_9 = process_data("data_1.2V/noise_scan_9.txt")
dac_values_10, num_pixels_firing_10, data_10 = process_data("data_1.2V/noise_scan_10.txt")
dac_values_11, num_pixels_firing_11, data_11 = process_data("data_1.2V/noise_scan_11.txt")
dac_values_12, num_pixels_firing_12, data_12 = process_data("data_1.2V/noise_scan_12.txt")
dac_values_13, num_pixels_firing_13, data_13 = process_data("data_1.2V/noise_scan_13.txt")
dac_values_14, num_pixels_firing_14, data_14 = process_data("data_1.2V/noise_scan_14.txt")
dac_values_15, num_pixels_firing_15, data_15 = process_data("data_1.2V/noise_scan_15.txt")

plt.bar(dac_values_15, num_pixels_firing_15, align='center', label = 'DAC = 15')
plt.bar(dac_values_14, num_pixels_firing_14, align='center', label = 'DAC = 14')
plt.bar(dac_values_13, num_pixels_firing_13, align='center', label = 'DAC = 13')
plt.bar(dac_values_12, num_pixels_firing_12, align='center', label = 'DAC = 12')
plt.bar(dac_values_11, num_pixels_firing_11, align='center', label = 'DAC = 11')
plt.bar(dac_values_10, num_pixels_firing_10, align='center', label = 'DAC = 10')
plt.bar(dac_values_9, num_pixels_firing_9, align='center', label = 'DAC = 9')
plt.bar(dac_values_8, num_pixels_firing_8, align='center', label = 'DAC = 8')
plt.bar(dac_values_7, num_pixels_firing_7, align='center', label = 'DAC = 7')
plt.bar(dac_values_6, num_pixels_firing_6, align='center', label = 'DAC = 6')
plt.bar(dac_values_5, num_pixels_firing_5, align='center', label = 'DAC = 5')
plt.bar(dac_values_4, num_pixels_firing_4, align='center', label = 'DAC = 4')
plt.bar(dac_values_3, num_pixels_firing_3, align='center', label = 'DAC = 3')
plt.bar(dac_values_2, num_pixels_firing_2, align='center', label = 'DAC = 2')
plt.bar(dac_values_1, num_pixels_firing_1, align='center', label = 'DAC = 1')
plt.bar(dac_values_0, num_pixels_firing_0, align='center', label = 'DAC = 0')
plt.xlabel('DAC_VTHR')
plt.ylabel('Number of Pixels Firing')
plt.legend()
plt.show()

# get turn-on point for each pixel at each distributions
#data_0 is the first dac scan
# data_0[0] is the first value of DAC_VTHR in the first dac scan
# data_0[0][0] is the value of the first pixel in the ...
#data_{DAC}[{DAC_VTHR}][{pixel}]

# Create an output directory if it doesn't exist
output_directory = 'output_plots_new_new'
os.makedirs(output_directory, exist_ok=True)

output_directory_1 = 'output_plots_meanVsDAC'
os.makedirs(output_directory_1, exist_ok=True)


# Define the number of pixels, rows, and columns per canvas
num_pixels = 1024
pixels_per_canvas = 16
num_canvases = num_pixels // pixels_per_canvas
canvas_rows = 4
canvas_cols = 4
mean_0_values = []
sigma_0_values = []
mean_1_values = []
sigma_1_values = []
mean_2_values = []
sigma_2_values = []
mean_3_values = []
sigma_3_values = []
mean_4_values = []
sigma_4_values = []
mean_5_values = []
sigma_5_values = []
mean_6_values = []
sigma_6_values = []
mean_7_values = []
sigma_7_values = []
mean_8_values = []
sigma_8_values = []
mean_9_values = []
sigma_9_values = []
mean_10_values = []
sigma_10_values = []
mean_11_values = []
sigma_11_values = []
mean_12_values = []
sigma_12_values = []
mean_13_values = []
sigma_13_values = []
mean_14_values = []
sigma_14_values = []
mean_15_values = []
sigma_15_values = []
trimming_dac_values = []
# Loop through each canvas
for canvas_index in range(num_canvases):
    fig, axes = plt.subplots(canvas_rows, canvas_cols, figsize=(15, 10))
    fig.tight_layout()

    # loop through each pixel in the current canvas
    for relative_pixel_index in range(pixels_per_canvas):
        pixel_index = canvas_index * pixels_per_canvas + relative_pixel_index

        pixel_counts_0 = [row[pixel_index] for row in data_0]
        pixel_counts_1 = [row[pixel_index] for row in data_1]
        pixel_counts_2 = [row[pixel_index] for row in data_2]
        pixel_counts_3 = [row[pixel_index] for row in data_3]
        pixel_counts_4 = [row[pixel_index] for row in data_4]
        pixel_counts_5 = [row[pixel_index] for row in data_5]
        pixel_counts_6 = [row[pixel_index] for row in data_6]
        pixel_counts_7 = [row[pixel_index] for row in data_7]
        pixel_counts_8 = [row[pixel_index] for row in data_8]
        pixel_counts_9 = [row[pixel_index] for row in data_9]
        pixel_counts_10 = [row[pixel_index] for row in data_10]
        pixel_counts_11 = [row[pixel_index] for row in data_11]
        pixel_counts_12 = [row[pixel_index] for row in data_12]
        pixel_counts_13 = [row[pixel_index] for row in data_13]
        pixel_counts_14 = [row[pixel_index] for row in data_14]
        pixel_counts_15 = [row[pixel_index] for row in data_15]


        dac_values_0 = [row[1024] for row in data_0]
        dac_values_1 = [row[1024] for row in data_1]
        dac_values_2 = [row[1024] for row in data_2]
        dac_values_3 = [row[1024] for row in data_3]
        dac_values_4 = [row[1024] for row in data_4]
        dac_values_5 = [row[1024] for row in data_5]
        dac_values_6 = [row[1024] for row in data_6]
        dac_values_7 = [row[1024] for row in data_7]
        dac_values_8 = [row[1024] for row in data_8]
        dac_values_9 = [row[1024] for row in data_9]
        dac_values_10 = [row[1024] for row in data_10]
        dac_values_11 = [row[1024] for row in data_11]
        dac_values_12 = [row[1024] for row in data_12]
        dac_values_13 = [row[1024] for row in data_13]
        dac_values_14 = [row[1024] for row in data_14]
        dac_values_15 = [row[1024] for row in data_15]


        row = relative_pixel_index // canvas_cols
        col = relative_pixel_index % canvas_cols
        ax = axes[row, col]
        ax.plot(dac_values_0, pixel_counts_0, label='0')
        ax.plot(dac_values_1, pixel_counts_1, label='1')
        ax.plot(dac_values_2, pixel_counts_2, label='2')
        ax.plot(dac_values_3, pixel_counts_3, label='3')
        ax.plot(dac_values_4, pixel_counts_4, label='4')
        ax.plot(dac_values_5, pixel_counts_5, label='5')
        ax.plot(dac_values_6, pixel_counts_6, label='6')
        ax.plot(dac_values_7, pixel_counts_7, label='7')
        ax.plot(dac_values_8, pixel_counts_8, label='8')
        ax.plot(dac_values_9, pixel_counts_9, label='9')
        ax.plot(dac_values_10, pixel_counts_10, label='10')
        ax.plot(dac_values_11, pixel_counts_11, label='11')
        ax.plot(dac_values_12, pixel_counts_12, label='12')
        ax.plot(dac_values_13, pixel_counts_13, label='13')
        ax.plot(dac_values_14, pixel_counts_14, label='14')
        ax.plot(dac_values_15, pixel_counts_15, label='15')
        ax.set_title(f'Pixel {pixel_index}')
        ax.set_xlabel('DAC_VTHR')
        ax.set_ylabel('Number of times firing')
        ax.legend()

        # fit gaussians
        try:

            params_0, _ = curve_fit(gaussian, dac_values_0, pixel_counts_0, p0=[1, np.mean(dac_values_0), np.std(dac_values_0)])
            params_1, _ = curve_fit(gaussian, dac_values_1, pixel_counts_1, p0=[1, np.mean(dac_values_1), np.std(dac_values_1)])
            params_2, _ = curve_fit(gaussian, dac_values_2, pixel_counts_2, p0=[1, np.mean(dac_values_2), np.std(dac_values_2)])
            params_3, _ = curve_fit(gaussian, dac_values_3, pixel_counts_3, p0=[1, np.mean(dac_values_3), np.std(dac_values_3)])
            params_4, _ = curve_fit(gaussian, dac_values_4, pixel_counts_4, p0=[1, np.mean(dac_values_4), np.std(dac_values_4)])
            params_5, _ = curve_fit(gaussian, dac_values_5, pixel_counts_5, p0=[1, np.mean(dac_values_5), np.std(dac_values_5)])
            params_6, _ = curve_fit(gaussian, dac_values_6, pixel_counts_6, p0=[1, np.mean(dac_values_6), np.std(dac_values_6)])
            params_7, _ = curve_fit(gaussian, dac_values_7, pixel_counts_7, p0=[1, np.mean(dac_values_7), np.std(dac_values_7)])
            params_8, _ = curve_fit(gaussian, dac_values_8, pixel_counts_8, p0=[1, np.mean(dac_values_8), np.std(dac_values_8)])
            params_9, _ = curve_fit(gaussian, dac_values_9, pixel_counts_9, p0=[1, np.mean(dac_values_9), np.std(dac_values_9)])
            params_10, _ = curve_fit(gaussian, dac_values_10, pixel_counts_10, p0=[1, np.mean(dac_values_10), np.std(dac_values_10)])
            params_11, _ = curve_fit(gaussian, dac_values_11, pixel_counts_11, p0=[1, np.mean(dac_values_11), np.std(dac_values_11)])
            params_12, _ = curve_fit(gaussian, dac_values_12, pixel_counts_12, p0=[1, np.mean(dac_values_12), np.std(dac_values_12)])
            params_13, _ = curve_fit(gaussian, dac_values_13, pixel_counts_13, p0=[1, np.mean(dac_values_13), np.std(dac_values_13)])
            params_14, _ = curve_fit(gaussian, dac_values_14, pixel_counts_14, p0=[1, np.mean(dac_values_14), np.std(dac_values_14)])
            params_15, _ = curve_fit(gaussian, dac_values_15, pixel_counts_15, p0=[1, np.mean(dac_values_15), np.std(dac_values_15)])


            # save mean, sigma
            mean_0_values.append(params_0[1])
            sigma_0_values.append(params_0[2])
            mean_1_values.append(params_1[1])
            sigma_1_values.append(params_1[2])
            mean_2_values.append(params_2[1])
            sigma_2_values.append(params_2[2])
            mean_3_values.append(params_3[1])
            sigma_3_values.append(params_3[2])
            mean_4_values.append(params_4[1])
            sigma_4_values.append(params_4[2])
            mean_5_values.append(params_5[1])
            sigma_5_values.append(params_5[2])
            mean_6_values.append(params_6[1])
            sigma_6_values.append(params_6[2])
            mean_7_values.append(params_7[1])
            sigma_7_values.append(params_7[2])
            mean_8_values.append(params_8[1])
            sigma_8_values.append(params_8[2])
            mean_9_values.append(params_9[1])
            sigma_9_values.append(params_9[2])
            mean_10_values.append(params_10[1])
            sigma_10_values.append(params_10[2])
            mean_11_values.append(params_11[1])
            sigma_11_values.append(params_11[2])
            mean_12_values.append(params_12[1])
            sigma_12_values.append(params_12[2])
            mean_13_values.append(params_13[1])
            sigma_13_values.append(params_13[2])
            mean_14_values.append(params_14[1])
            sigma_14_values.append(params_14[2])
            mean_15_values.append(params_15[1])
            sigma_15_values.append(params_15[2])


            # calculate chi_squared/ndof for each gaussian fit
            chi_squared_0 = np.sum((pixel_counts_0 - gaussian(dac_values_0, *params_0)) ** 2)
            ndof_0 = len(dac_values_0) - len(params_0)
            chi_ndof_0 = chi_squared_0 / ndof_0
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_0} (0 Local Threshold)')

            chi_squared_1 = np.sum((pixel_counts_1 - gaussian(dac_values_1, *params_1)) ** 2)
            ndof_1= len(dac_values_1) - len(params_1)
            chi_ndof_1 = chi_squared_1 / ndof_1
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_1} (1 Local Threshold)')

            chi_squared_2 = np.sum((pixel_counts_2 - gaussian(dac_values_2, *params_2)) ** 2)
            ndof_2= len(dac_values_2) - len(params_2)
            chi_ndof_2 = chi_squared_2 / ndof_2
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_2} (2 Local Threshold)')

            chi_squared_3 = np.sum((pixel_counts_3 - gaussian(dac_values_3, *params_3)) ** 2)
            ndof_3= len(dac_values_3) - len(params_3)
            chi_ndof_3 = chi_squared_3 / ndof_3
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_3} (3 Local Threshold)')

            chi_squared_4 = np.sum((pixel_counts_4 - gaussian(dac_values_4, *params_4)) ** 2)
            ndof_4= len(dac_values_4) - len(params_4)
            chi_ndof_4 = chi_squared_4 / ndof_4
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_4} (4 Local Threshold)')

            chi_squared_5 = np.sum((pixel_counts_5 - gaussian(dac_values_5, *params_5)) ** 2)
            ndof_5= len(dac_values_5) - len(params_5)
            chi_ndof_5 = chi_squared_5 / ndof_5
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_5} (5 Local Threshold)')

            chi_squared_6 = np.sum((pixel_counts_6 - gaussian(dac_values_6, *params_6)) ** 2)
            ndof_6= len(dac_values_6) - len(params_6)
            chi_ndof_6 = chi_squared_6 / ndof_6
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_6} (6 Local Threshold)')

            chi_squared_7 = np.sum((pixel_counts_7 - gaussian(dac_values_7, *params_7)) ** 2)
            ndof_7= len(dac_values_7) - len(params_7)
            chi_ndof_7 = chi_squared_7 / ndof_7
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_7} (7 Local Threshold)')

            chi_squared_8 = np.sum((pixel_counts_8 - gaussian(dac_values_8, *params_8)) ** 2)
            ndof_8= len(dac_values_8) - len(params_8)
            chi_ndof_8 = chi_squared_8 / ndof_8
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_8} (8 Local Threshold)')

            chi_squared_9 = np.sum((pixel_counts_9 - gaussian(dac_values_9, *params_9)) ** 2)
            ndof_9= len(dac_values_9) - len(params_9)
            chi_ndof_9 = chi_squared_9 / ndof_9
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_9} (9 Local Threshold)')

            chi_squared_10 = np.sum((pixel_counts_10 - gaussian(dac_values_10, *params_10)) ** 2)
            ndof_10= len(dac_values_10) - len(params_10)
            chi_ndof_10 = chi_squared_10 / ndof_10
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_10} (10 Local Threshold)')

            chi_squared_11 = np.sum((pixel_counts_11 - gaussian(dac_values_11, *params_11)) ** 2)
            ndof_11= len(dac_values_11) - len(params_11)
            chi_ndof_11 = chi_squared_11 / ndof_11
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_11} (11 Local Threshold)')

            chi_squared_12 = np.sum((pixel_counts_12 - gaussian(dac_values_12, *params_12)) ** 2)
            ndof_12= len(dac_values_12) - len(params_12)
            chi_ndof_12 = chi_squared_12 / ndof_12
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_12} (12 Local Threshold)')

            chi_squared_13 = np.sum((pixel_counts_13 - gaussian(dac_values_13, *params_13)) ** 2)
            ndof_13= len(dac_values_13) - len(params_13)
            chi_ndof_13 = chi_squared_13 / ndof_13
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_13} (13 Local Threshold)')

            chi_squared_14 = np.sum((pixel_counts_14 - gaussian(dac_values_14, *params_14)) ** 2)
            ndof_14= len(dac_values_14) - len(params_14)
            chi_ndof_14 = chi_squared_14 / ndof_14
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_14} (14 Local Threshold)')

            chi_squared_15 = np.sum((pixel_counts_15 - gaussian(dac_values_15, *params_15)) ** 2)
            ndof_15= len(dac_values_15) - len(params_15)
            chi_ndof_15 = chi_squared_15 / ndof_15
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_15} (15 Local Threshold)')
            # display mean, sigma, and chi_squared/ndof values in the legend
            #legend_text = f'Low Local THR: $\mu$={params_low[1]:.2f}, $\sigma$={params_low[2]:.2f}, $\chi^2/ndof$={chi_ndof_low:.2f}\nHigh Local THR: $\mu$={params_high[1]:.2f}, $\sigma$={params_high[2]:.2f}, $\chi^2/ndof$={chi_ndof_high:.2f}'
            #ax.legend(title='Gaussian Fits', fontsize='small', labelspacing=0.8, borderpad=0.8).set_title(legend_text)


        except RuntimeError as e:
            print(f"Fitting failed for pixel {pixel_index}: {e}")
            mean_0_values.append(0)
            sigma_0_values.append(0)
            mean_1_values.append(0)
            sigma_1_values.append(0)
            mean_2_values.append(0)
            sigma_2_values.append(0)
            mean_3_values.append(0)
            sigma_3_values.append(0)
            mean_4_values.append(0)
            sigma_4_values.append(0)
            mean_5_values.append(0)
            sigma_5_values.append(0)
            mean_6_values.append(0)
            sigma_6_values.append(0)
            mean_7_values.append(0)
            sigma_7_values.append(0)
            mean_8_values.append(0)
            sigma_8_values.append(0)
            mean_9_values.append(0)
            sigma_9_values.append(0)
            mean_10_values.append(0)
            sigma_10_values.append(0)
            mean_11_values.append(0)
            sigma_11_values.append(0)
            mean_12_values.append(0)
            sigma_12_values.append(0)
            mean_13_values.append(0)
            sigma_13_values.append(0)
            mean_14_values.append(0)
            sigma_14_values.append(0)
            mean_15_values.append(0)
            sigma_15_values.append(0)
            continue  # Move to the next pixel

    #Save the current canvas to the output directory
    canvas_filename = os.path.join(output_directory, f'column_{canvas_index + 1}.png')
    plt.savefig(canvas_filename)
    plt.close()


import matplotlib.pyplot as plt


# Create a list of the mean arrays
mean_arrays = [mean_0_values, mean_1_values, mean_2_values, mean_3_values,
               mean_4_values, mean_5_values, mean_6_values, mean_7_values,
               mean_8_values, mean_9_values, mean_10_values, mean_11_values,
               mean_12_values, mean_13_values, mean_14_values, mean_15_values]
for array in mean_arrays:
    print(array[1])




with open("tuned.mask", "w") as output_file:
    output_file.write("# col row tb_enable mask tdac not top\n")

    for row in range(16):
        for col in range(64):
            pixel_index = row * 64 + col            # to check

            closest_mean = None
            closest_distance = float('inf')
            closest_mean_array_index = None

            for mean_index, mean_array in enumerate(mean_arrays):
                mean_value = mean_array[pixel_index]
                distance = abs(mean_value - target_baseline)

                if distance < closest_distance:
                    closest_distance = distance
                    closest_mean = mean_value
                    closest_mean_array_index = mean_index

            output_line = f"{col} {row} 1 0 {closest_mean_array_index} 0\n"     # to check
            output_file.write(output_line)



for canvas_index in range(num_canvases):
    fig, axes = plt.subplots(canvas_rows, canvas_cols, figsize=(15, 10))
    fig.tight_layout()

    # loop through each pixel in the current canvas
    for relative_pixel_index in range(pixels_per_canvas):
        pixel_index = canvas_index * pixels_per_canvas + relative_pixel_index

        mean_values_pixel = []  # Collect mean values for each DAC scan

        # Loop through all DAC scans and collect mean values for the current pixel
        for data in [data_0, data_1, data_2, data_3, data_4, data_5, data_6, data_7,
                     data_8, data_9, data_10, data_11, data_12, data_13, data_14, data_15]:
            pixel_counts = [row[pixel_index] for row in data]
            dac_values = [row[1024] for row in data]

            try:
                params, _ = curve_fit(gaussian, dac_values, pixel_counts, p0=[1, np.mean(dac_values), np.std(dac_values)])
                mean_values_pixel.append(params[1])
            except RuntimeError:
                mean_values_pixel.append(0)  # Fitting failed

        row = relative_pixel_index // canvas_cols
        col = relative_pixel_index % canvas_cols
        ax = axes[row, col]
        ax.plot([1, 13], [mean_values_pixel[1], mean_values_pixel[14]], color='red', linestyle='-', linewidth=2) # correcting for 2 identical values
        ax.plot([2, 14], [mean_values_pixel[1], mean_values_pixel[14]], color='red', linestyle='-', linewidth=2) # correcting for 2 identical values
        x_intersection_1 = (target_baseline - mean_values_pixel[1]) * (13 - 1) / (mean_values_pixel[14] - mean_values_pixel[1]) + 1
        x_intersection_2 = (target_baseline - mean_values_pixel[1]) * (14 - 2) / (mean_values_pixel[14] - mean_values_pixel[1]) + 2
        print(f"Pixel {pixel_index}: Intersection points: x1 = {x_intersection_1:.2f}, x2 = {x_intersection_2:.2f}")
        denominator = mean_values_pixel[14] - mean_values_pixel[1]
        if denominator != 0:
            x_intersection_1 = (target_baseline - mean_values_pixel[1]) * (13 - 1) / denominator + 1
            x_intersection_2 = (target_baseline - mean_values_pixel[1]) * (14 - 2) / denominator + 2
            if not np.isnan(x_intersection_1) and not np.isnan(x_intersection_2):
                trimming_dac = x_intersection_1 if x_intersection_1 <= 7 else x_intersection_2
            else:
                trimming_dac = 7 # if the calculation of the intersection fails, set trimming dac to 7
        else:
            trimming_dac = 7 # if there's an error calculating the intersection, set trimming dac to 7

        # check if the trimming_dac is within the valid DAC range
        if trimming_dac < -0.5:
            trimming_dac = 0
        if trimming_dac > 15.5:
            trimming_dac = 15

        trimming_dac_values.append(round(trimming_dac))
        print(f"trimming dac {round(trimming_dac)}")
        ax.plot(range(16), mean_values_pixel, marker='o', linestyle='-', color='blue')
        ax.set_title(f'Pixel {pixel_index}')
        ax.set_xlabel('DAC Index')
        ax.set_ylabel('Mean Value')
        ax.set_xticks(range(16))
        ax.set_xticklabels(range(16))
        print(f'plotting Pixel {pixel_index}')

    # add horizontal gray line for target baseline
    for ax_row in axes:
        for ax_single in ax_row:
            ax_single.axhline(y=target_baseline, color='gray', linestyle='dashed', label="Target baseline", alpha=0.5)



    # Save the current canvas to the output directory
    canvas_filename = os.path.join(output_directory_1, f'column_{canvas_index + 1}.png')
    plt.savefig(canvas_filename)
    plt.close()



pixel_index = 0

print(trimming_dac_values)
with open("tuned_new.mask", "w") as output_file:
    output_file.write("# col row tb_enable mask tdac not top\n")

    for row in range(16):
        for col in range(64):
            print(pixel_index)
            output_line = f"{col} {row} 1 0 {trimming_dac_values[pixel_index]} 0\n"
            output_file.write(output_line)
            pixel_index = pixel_index + 1


# read the contents of the two mask files with the two methods investigated:
# - having all the DACs distributions
# - only accesing the DAC 1 and 14, and extrapolate the rest.

with open('tuned.mask', 'r') as file1, open('tuned_new.mask', 'r') as file2:
    lines1 = file1.readlines()
    lines2 = file2.readlines()

# extract the 5th column values (where the local DAC is)
tdac_values_tunes = [int(line.split()[4]) for line in lines1[1:]]  # Skip the header line
tdac_values_tuned_new = [int(line.split()[4]) for line in lines2[1:]]  # Skip the header line

# calculate the differences between the two columns
differences = [tdac_tuned - tdac_tunes for tdac_tunes, tdac_tuned in zip(tdac_values_tunes, tdac_values_tuned_new)]

# create a histogram to visualize the distribution
plt.figure(figsize=(10, 6))
plt.hist(differences, bins=20, edgecolor='black', alpha=0.7)
plt.xlabel('Difference in Local DAC THR')
plt.ylabel('Frequency')
plt.title('Distribution of DAC Lcal THR Value Differences')
plt.show()
