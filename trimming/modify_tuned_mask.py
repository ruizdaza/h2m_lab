# read the file
file_path = 'tuned.mask'  # replace with the actual file path
with open(file_path, 'r') as file:
    lines = file.readlines()

# modify the second column
modified_lines = []
for line in lines:
    columns = line.strip().split()
    if len(columns) >= 5:   # check if the line has at least 5 columns, to avoid the weird ones
        print(columns[1])
        if columns[1]=='0':
            columns[1] = '12' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='1':
            columns[1] = '13' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='2':
            columns[1] = '14'# modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='3':
            columns[1] = '15' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='4':
            columns[1] = '8' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='5':
            columns[1] = '9' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='6':
            columns[1] = '10' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='7':
            columns[1] = '11' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='8':
            columns[1] = '4' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='9':
            columns[1] = '5' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='10':
            columns[1] = '6' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='11':
            columns[1] = '7' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='12':
            columns[1] = '0' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='13':
            columns[1] = '1' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='14':
            columns[1] = '2' # modify the seond column
            #modified_lines.append(' '.join(columns) + '\n')
        elif columns[1]=='15':
            columns[1] = '3' # modify the seond column
        modified_lines.append(' '.join(columns) + '\n')



print(modified_lines)
# write the modified content to a new file
new_file_path = 'tuned_tb_1-2V_extraCodition.mask'   # replace name
with open(new_file_path, 'w') as file:
    file.writelines(modified_lines)

# print a message once is done
print(f"Modified content saved to '{new_file_path}'")
