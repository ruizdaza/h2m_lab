import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import os

# gaussian function for fitting
def gaussian(x, amp, mean, sigma):
    return amp * np.exp(-(x - mean)**2 / (2 * sigma**2))

# reads and processes the data function
def process_data(filename):
    with open(filename, "r") as file:
        lines = file.readlines()

    data = []
    for line in lines:
        if line.startswith("occupancy"):
            continue

        values = line.strip().split()
        numeric_values = []
        for value in values:
            try:
                numeric_values.append(int(value))
            except ValueError:
                numeric_values.append(value)  #the string dac_name mesh up things
        data.append(numeric_values)

    dac_values = []
    num_pixels_firing = []

    for row in data:
        pixel_firing_counts = row[:1024]
        num_pulses = row[1028] #this is 100
        dac_value = row[1024]
        if isinstance(pixel_firing_counts[0], int):  #the string dac_name mesh up things
            num_firing_pixels = sum(pixel_firing_counts)
            num_pixels_firing.append(num_firing_pixels)
            dac_values.append(dac_value)

    return dac_values, num_pixels_firing, data


# read and process the data from the low and high local thresholds
dac_values_low, num_pixels_firing_low, data_low = process_data("noise_scan_0.txt")
dac_values_high, num_pixels_firing_high, data_high = process_data("noise_scan_15.txt")
dac_values_tuned, num_pixels_firing_tuned, data_tuned = process_data("noise_scan_tuned.txt")

try:
    # fit gaussian curves to the data
    params_low, _ = curve_fit(gaussian, dac_values_low, num_pixels_firing_low, p0=[1, np.mean(dac_values_low), np.std(dac_values_low)])
    params_high, _ = curve_fit(gaussian, dac_values_high, num_pixels_firing_high, p0=[1, np.mean(dac_values_high), np.std(dac_values_high)])
    params_tuned, _ = curve_fit(gaussian, dac_values_tuned, num_pixels_firing_tuned, p0=[1, np.mean(dac_values_tuned), np.std(dac_values_tuned)])


    plt.bar(dac_values_low, num_pixels_firing_low, align='center', color='darkmagenta', label=f'Lowest Local THR: $\mu$={params_low[1]:.2f}, $\sigma$={params_low[2]:.2f}', width=1)
    plt.bar(dac_values_high, num_pixels_firing_high, align='center', color='seagreen', label=f'Highest Local THR: $\mu$={params_high[1]:.2f}, $\sigma$={params_high[2]:.2f}', width=1)
    plt.bar(dac_values_tuned, num_pixels_firing_tuned, align='center', color='red', label=f'Trimming Local THR: $\mu$={params_tuned[1]:.2f}, $\sigma$={params_tuned[2]:.2f}', width=1)
    plt.xlabel('DAC_VTHR')
    plt.ylabel('Number of Pixels Firing')

    # plot the gaussian fits
    x_fit = np.linspace(min(dac_values_low + dac_values_high), max(dac_values_low + dac_values_high), 100)
    plt.plot(x_fit, gaussian(x_fit, *params_low), color='darkmagenta', linestyle='--')
    plt.plot(x_fit, gaussian(x_fit, *params_high), color='seagreen', linestyle='--')
    plt.plot(x_fit, gaussian(x_fit, *params_tuned), color='red', linestyle='--')

    # calculating the target baseline
    target_baseline = (params_low[1] + params_high[1])/2
    print(f'Target baseline is: {target_baseline:.2f}')
    plt.axvline(x = target_baseline, color='gray', linestyle='dashed', label = f"target baseline = {target_baseline:.2f}")

    # display mean and sigma values in the legend
    #legend_text = f'Data Low Local THR: $\mu$={params_low[1]:.2f}, $\sigma$={params_low[2]:.2f}\nData High Local THR: $\mu$={params_high[1]:.2f}, $\sigma$={params_high[2]:.2f}'
    plt.legend(loc='upper left', fontsize='small', labelspacing=0.8, borderpad=0.8)
    plt.tight_layout()
    plt.show()

except RuntimeError as e:
    plt.bar(dac_values_low, num_pixels_firing_low, align='center', color='darkmagenta', label=f'Lowest Local THR', width=1)
    plt.bar(dac_values_high, num_pixels_firing_high, align='center', color='seagreen', label=f'Highest Local THR', width=1)
    plt.bar(dac_values_tuned, num_pixels_firing_tuned, align='center', color='red', label=f'Trimming Local THR', width=1)
    plt.xlabel('DAC_VTHR')
    plt.ylabel('Number of Pixels Firing')
    plt.legend(loc='upper left', fontsize='small', labelspacing=0.8, borderpad=0.8)
    plt.tight_layout()
    plt.show()
    continue
