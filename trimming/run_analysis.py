from os import listdir, chdir
from os.path import join, abspath, dirname
from subprocess import run


def main(args=None):
    # settings
    input_dir = "data"
    script_directory = dirname(abspath(__file__))
    script = join(script_directory, "H2M_scanReader.C")

    # run analysis for individual files
    for f in listdir(input_dir):
        input_file = join(input_dir, f)
        # only consider the '.bin' files
        if not input_file.endswith('.bin'): continue

        command = ["root", "-l", "-x", "-b", f"{script}(\"{input_file}\",50)"]
        run(command)

    # create mask
    script = join(script_directory, "createTrimming.C")
    chdir(input_dir)
    command = ["root", "-l", "-x", "-b", f"{script}()"]
    run(command)


if __name__ == "__main__":
    main()
