# Trimming
## Get data
To get the distributions for the 16 different DACs:

Modify ```scripts/add_device``` with the correct threshold local DAC, i.e this line: ```generateMask_human 1 0 14 0 0```.
Here the 14 indicates that the local threshold DAC, and needs to be changed for each scan.
```
pearycli -r scripts/add_device.src -c config/noise_scans/tune0.cfg # in add_device generateMask_human 1 0 0 0 0
pearycli -r scripts/add_device.src -c config/noise_scans/tune1.cfg # in add_device generateMask_human 1 0 1 0 0
...
pearycli -r scripts/add_device.src -c config/noise_scans/tune15.cfg # in add_device generateMask_human 1 0 15 0 0
```
They will produce the ```noise_scan_0.txt, ..., noise_scan_15.txt``` and ```noise_scan_0.bin, ..., noise_scan_15``` data files.

There is also a convenience runner script which can be copied to the caribou with H2M and does the modification on the fly and runs the scans with peary. It uses `python3` and will fail if executed simply with `python`.

```bash
# copy to caribou
scp run_peary.py <caribou>

# if necessary, adapt paths in script

# run script
python3 run_peary.py

```

Note, that ```.bin``` files are appended and not overwritten.

## Analysis script
For the analysis, you can use the code in ```peary/devices/H2M/utils```.

1. Move to the folder when you have ```noise_scan_*.bin``` files. Modify the path to ``` H2M_scanReader.C```  and ```createTrimming.C```. And run:

```
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_0.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_1.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_2.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_3.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_4.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_5.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_6.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_7.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_8.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_9.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_10.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_11.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_12.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_13.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_14.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_15.bin",50)'

```

```
root -l -x '~/Desktop/peary/devices/H2M/utils/createTrimming.C()'
```
A mask file is created.

There is also a convenience runner script which executes the above commands in a sequence.

```bash
# create directory for data
mkdir -p data/

# copy data from caribou with H2M to directory
scp "<caribou>/noise_scan_*.bin" data/

# run
python3 run_analysis.py
```

## Get data with your trimming dacs mask
Run:
1. Since now we will apply the our mask ```mask/tuned.mask```, we need to comment the following lines in  ```scripts/add_device```:

```
add_device H2M
powerOn 0
setVoltage v_pwell 0 0
setVoltage v_sub 0 0
configure 0
# tp enable, mask, 4 bit tune (ordered from max neg to max pos shift), not_top, device 0
#generateMask_human 1 0 0 0 0
#programMask 0
#configureTP 0

#testpulse 1 1 0

parameterScan 0

```
Make sure that in ```config_noise_scans/tunedResult.cfg``` you have the correct path to your mask in: ```mask_filename=masks/tuned.mask```

2. Run:
```
pearycli -r scripts/add_device.src -c config_noise_scans/tunedResult.cfg
```

You will get ```noise_scan_tuned.txt``` and ```noise_scan_tuned.bin```

3. Run ```root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_tuned.bin",50)``` which will plot trimming distribution.

4. Run ```ploting.C``` will plot the lowest, highest and tuned distributions together.




# Additional scripts for Python
Here are additional scripts for plotting in Python.

1. Run ```python3 getTargetBaseline.py```. It plots the highest and lowest local DAC trimming distributions. The middle point between the mean of the two gaussian fits is defined as the "target baseline". It is included in the plot as discontinous line and also printed in the terminal. An example can be found in ```figures```.

The first commented part of the code can be uncommented and it will produce 64 canvas (one for each column of pixels) with 16 pixels each corresponding to the distribution of the lowest and highest DAC per pixel. The 16 canvas will be saved in an output folder called output_2dacs_perPixel.

2. Run ```python trimming.py``` modifying the ```target_baseline``` parameter obtained in the previous step.
It plots the 16 DACs distribution per pixel and save the 64 canvas (1 per column) in a folder.
It retursn ```tuned.mask```, you can copy in ```mask/tuned.mask```.

The code is very slow, very stupid, but it works...



# Some check Plots
```check_linearity.py``` plots the 16 distributions, calculates the mean and sigma of them, and plots these values as function of the local THR DAC. It also included the values in dash lines of the trimmed distribution. An example of plot is in figures.

```check_linearity_perPixel.py``` wants to study if it is possible to only run a scan for LOCAL THR DAC = 2 and = 14. And using the linear behaviour, extrapolate the other DAC values from a linear fit and intersection with the target baseline. It returns a tuned.mask which is similar to the one using all the dacs. The difference between the trimming DACs calculated in the two methods is returned, an example can be found in ```results```.


## An alternative fast trimming
Because of the studies in the previous section it is good enough to only take data from the DAC 1 and DAC 15. This method can be used:

```
python3 fast_trimming.py
```
It returns the tuned.mask.



# More plotting with python:

```
python3 plot_with_python
```
