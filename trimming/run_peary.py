from os import remove
from os.path import join, basename, splitext, exists
from subprocess import run


class NoiseScan:
    def __init__(self, scan_number):
        # Initialize the paths to the specified files
        self.scan_number = scan_number
        self.config_file_path = join("config", "noise_scans", f"tune{scan_number}.cfg")
        self.run_file_path = join("scripts", "add_device.src")
        self.executable = "pearycli"

        # create a temporary run file with the modified threshold
        self.update_runfile()


    def display(self):
        print(f"Running for scan number {self.scan_number}: ")
        print(f"- Config File Path: {self.config_file_path}")
        print(f"- Run File Path: {self.run_file_path}")
        print(f"- Executable: {self.executable}")


    def update_runfile(self):
        with open(self.run_file_path, 'r') as file:
            lines = file.readlines()

        for index, line in enumerate(lines):
            if "generateMask_human" in line:
                lines[index] = f"generateMask_human 1 0 {self.scan_number} 0 0\n"

        # write to /tmp
        run_file_name, ext = splitext(basename(self.run_file_path))
        output_path = join('/tmp', f"{run_file_name}_{self.scan_number}{ext}")
        with open(output_path, 'w') as file:
            file.writelines(lines)
        self.run_file_path = output_path


    def run(self):
        # show paths before running
        self.display()
        # run command 
        command = [self.executable, "-r", self.run_file_path, "-c", self.config_file_path]
        run(command)
        # clean up after running
        if exists(self.run_file_path) and '/tmp' in self.run_file_path:
            remove(self.run_file_path)


def main(args=None):

    noise_scans = [NoiseScan(i) for i in range(0, 15)]

    # run jobs
    for scan in noise_scans:
        scan.run()


if __name__ == "__main__":
    main()
