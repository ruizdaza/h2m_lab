import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import os

# gaussian function for fitting
def gaussian(x, amp, mean, sigma):
    return amp * np.exp(-(x - mean)**2 / (2 * sigma**2))

# reads and processes the data function
def process_data(filename):
    with open(filename, "rb") as file:
        lines = file.readlines()

    data = []
    for line in lines:
        if line.startswith("occupancy"):
            continue

        values = line.strip().split()
        numeric_values = []
        for value in values:
            try:
                numeric_values.append(int(value))
            except ValueError:
                numeric_values.append(value)  #the string dac_name mesh up things
        data.append(numeric_values)

    dac_values = []
    num_pixels_firing = []

    for row in data:
        pixel_firing_counts = row[:1024]
        num_pulses = row[1028] #this is 100
        dac_value = row[1024]
        if isinstance(pixel_firing_counts[0], int):  #the string dac_name mesh up things
            num_firing_pixels = sum(pixel_firing_counts)
            num_pixels_firing.append(num_firing_pixels)
            dac_values.append(dac_value)

    return dac_values, num_pixels_firing, data


# read and process the data from the low and high local thresholds
dac_values_low, num_pixels_firing_low, data_low = process_data("noise_scan_0.txt")
dac_values_high, num_pixels_firing_high, data_high = process_data("noise_scan_14.txt")

try:
    # fit gaussian curves to the data
    params_low, _ = curve_fit(gaussian, dac_values_low, num_pixels_firing_low, p0=[1, np.mean(dac_values_low), np.std(dac_values_low)])
    params_high, _ = curve_fit(gaussian, dac_values_high, num_pixels_firing_high, p0=[1, np.mean(dac_values_high), np.std(dac_values_high)])

    plt.bar(dac_values_low, num_pixels_firing_low, align='center', color='darkmagenta', label=f'Lowest Local THR: $\mu$={params_low[1]:.2f}, $\sigma$={params_low[2]:.2f}', width=1)
    plt.bar(dac_values_high, num_pixels_firing_high, align='center', color='seagreen', label=f'Highest Local THR: $\mu$={params_high[1]:.2f}, $\sigma$={params_high[2]:.2f}', width=1)
    plt.xlabel('DAC_VTHR')
    plt.ylabel('Number of Pixels Firing')

    # plot the gaussian fits
    x_fit = np.linspace(min(dac_values_low + dac_values_high), max(dac_values_low + dac_values_high), 100)
    plt.plot(x_fit, gaussian(x_fit, *params_low), color='darkmagenta', linestyle='--')
    plt.plot(x_fit, gaussian(x_fit, *params_high), color='seagreen', linestyle='--')

    # calculating the target baseline
    target_baseline = (params_low[1] + params_high[1])/2
    print(f'Target baseline is: {target_baseline:.2f}')
    plt.axvline(x = target_baseline, color='gray', linestyle='dashed', label = f"target baseline = {target_baseline:.2f}")

    # display mean and sigma values in the legend
    #legend_text = f'Data Low Local THR: $\mu$={params_low[1]:.2f}, $\sigma$={params_low[2]:.2f}\nData High Local THR: $\mu$={params_high[1]:.2f}, $\sigma$={params_high[2]:.2f}'
    plt.legend(loc='upper left', fontsize='small', labelspacing=0.8, borderpad=0.8)
    plt.tight_layout()
    plt.show()

except RuntimeError as e:
    print(f"Fitting failed: {e}")
    plt.bar(dac_values_low, num_pixels_firing_low, align='center', color='darkmagenta', label=f'Lowest Local THR', width=1)
    plt.bar(dac_values_high, num_pixels_firing_high, align='center', color='seagreen', label=f'Highest Local THR', width=1)
    plt.xlabel('DAC_VTHR')
    plt.ylabel('Number of Pixels Firing')
    plt.legend(loc='upper left', fontsize='small', labelspacing=0.8, borderpad=0.8)
    plt.tight_layout()
    plt.show()


'''
# Create an output directory if it doesn't exist
output_directory = 'output_2dacs_perPixel'
os.makedirs(output_directory, exist_ok=True)

# Define the number of pixels, rows, and columns per canvas
num_pixels = 1024
pixels_per_canvas = 16
num_canvases = num_pixels // pixels_per_canvas
canvas_rows = 4
canvas_cols = 4
mean_low_values = []
sigma_low_values = []
mean_high_values = []
sigma_high_values = []
# Loop through each canvas
for canvas_index in range(num_canvases):
    fig, axes = plt.subplots(canvas_rows, canvas_cols, figsize=(15, 10))
    fig.tight_layout()

    # loop through each pixel in the current canvas
    for relative_pixel_index in range(pixels_per_canvas):
        pixel_index = canvas_index * pixels_per_canvas + relative_pixel_index

        pixel_counts_low = [row[pixel_index] for row in data_low]
        pixel_counts_high = [row[pixel_index] for row in data_high]
        dac_values_low = [row[1024] for row in data_low]
        dac_values_high = [row[1024] for row in data_high]

        row = relative_pixel_index // canvas_cols
        col = relative_pixel_index % canvas_cols
        ax = axes[row, col]
        ax.plot(dac_values_low, pixel_counts_low, label='Low Local Threshold', color='purple')
        print(pixel_counts_low)
        print(dac_values_low)
        for dac in dac_values_low:
            if dac == 63:
                print("interested value", pixel_counts_low[dac])
        ax.plot(dac_values_high, pixel_counts_high, label='High Local Threshold', color='seagreen')
        ax.set_title(f'Pixel {pixel_index}')
        ax.set_xlabel('DAC_VTHR')
        ax.set_ylabel('Number of times firing')
        ax.legend()

        # fit gaussians
        try:

            params_low, _ = curve_fit(gaussian, dac_values_low, pixel_counts_low, p0=[1, np.mean(dac_values_low), np.std(dac_values_low)])
            params_high, _ = curve_fit(gaussian, dac_values_high, pixel_counts_high, p0=[1, np.mean(dac_values_high), np.std(dac_values_high)])


            # save mean, sigma
            mean_low_values.append(params_low[1])
            sigma_low_values.append(params_low[2])
            mean_high_values.append(params_high[1])
            sigma_high_values.append(params_high[2])

            # calculate chi_squared/ndof for each gaussian fit
            chi_squared_low = np.sum((pixel_counts_low - gaussian(dac_values_low, *params_low)) ** 2)
            ndof_low = len(dac_values_low) - len(params_low)
            chi_ndof_low = chi_squared_low / ndof_low
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_low} (Low Local Threshold)')

            chi_squared_high = np.sum((pixel_counts_high - gaussian(dac_values_high, *params_high)) ** 2)
            ndof_high = len(dac_values_high) - len(params_high)
            chi_ndof_high = chi_squared_high / ndof_high
            print(f' Chi/ndof for pixel {pixel_index} is {chi_ndof_high} (High Local Threshold)')

            # display mean, sigma, and chi_squared/ndof values in the legend
            legend_text = f'Low Local THR: $\mu$={params_low[1]:.2f}, $\sigma$={params_low[2]:.2f}, $\chi^2/ndof$={chi_ndof_low:.2f}\nHigh Local THR: $\mu$={params_high[1]:.2f}, $\sigma$={params_high[2]:.2f}, $\chi^2/ndof$={chi_ndof_high:.2f}'
            ax.legend(title='Gaussian Fits', fontsize='small', labelspacing=0.8, borderpad=0.8).set_title(legend_text)


        except RuntimeError as e:
            print(f"Fitting failed for pixel {pixel_index}: {e}")
            continue  # Move to the next pixel

    #Save the current canvas to the output directory
    canvas_filename = os.path.join(output_directory, f'column_{canvas_index + 1}.png')
    plt.savefig(canvas_filename)
    plt.close()

print('Plots saved in the output folder.')

'''








'''
# histograms with the mean threshold and noise for the two data
plt.hist(mean_low_values, color='darkmagenta', bins= 30, width=1)
plt.annotate(f'Entries: {len(mean_low_values)}', xy=(0.5, 0.95), xycoords='axes fraction', ha='center')
plt.xlabel('mean threshold [DAC]')
plt.title('Mean Threshold for Low Local THR')
plt.show()
plt.hist(mean_high_values, color='seagreen', bins= 30, width=1)
plt.annotate(f'Entries: {len(mean_high_values)}', xy=(0.5, 0.95), xycoords='axes fraction', ha='center')
plt.xlabel('mean threshold [DAC]')
plt.title('Mean Threshold for High Local THR')
plt.show()

plt.hist(sigma_low_values, color='darkmagenta', bins= 30, width=1, range = [-5, 5])
plt.annotate(f'Entries: {len(sigma_low_values)}', xy=(0.5, 0.95), xycoords='axes fraction', ha='center')
plt.xlabel('sigma [DAC]')
plt.title('Noise for Low Local THR')
plt.show()
plt.hist(sigma_high_values, color='seagreen', bins= 30, width=1, range = [-5, 5])
plt.annotate(f'Entries: {len(sigma_high_values)}', xy=(0.5, 0.95), xycoords='axes fraction', ha='center')
plt.xlabel('sigma [DAC]')
plt.title('Noise for High Local THR')
plt.show()
'''
