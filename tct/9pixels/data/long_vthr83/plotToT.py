import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import rotate

directory = ''  # Replace with the actual path to your files
x_range = np.arange(17.059, 17.199, 0.002)
y_range = np.arange(10.960, 11.540, 0.002)
z_value = 36.760
max_frames = 99  # Set the maximum number of frames to read


def calculate_mean_data(file_content):
    data_sum = 0
    frame_count = 0

    for line in file_content:
        if '-1  -1  -1  -1' in line:
            # Found the end of a frame
            frame_count += 1
            if frame_count >= max_frames:
                break  # Stop counting frames after reaching the limit
        elif 'col' not in line:
            # Skip lines with headers
            col, row, data, mode = map(int, line.split())
            data_sum += data
    print(data_sum, frame_count, data_sum / frame_count if frame_count > 0 else 0)
    return data_sum / frame_count if frame_count > 0 else 0

hitmap_data = np.zeros((len(y_range), len(x_range)))

for x_index, x in enumerate(x_range):
    for y_index, y in enumerate(y_range):
        filename = f'long_vth83_{x:.3f}_{y:.3f}_{z_value:.3f}.txt'
        file_path = os.path.join(directory, filename)

        if os.path.exists(file_path):
            print(f"File found: {file_path}.")
            with open(file_path, 'r') as file:
                file_content = file.readlines()
                mean_data = calculate_mean_data(file_content)
                hitmap_data[y_index, x_index] = mean_data
        else:
            print(f"File not found: {file_path}. Skipping...")

# Create the hitmap ROTATED to make people happy
rotated_hitmap_data = np.rot90(hitmap_data, 2)
plt.imshow(rotated_hitmap_data, extent=[min(x_range), max(x_range), min(y_range), max(y_range)], origin='lower', aspect='auto', cmap='viridis')
colorbar = plt.colorbar(label='Mean ToT Value [clock cycles]')
colorbar.set_label('Mean ToT Value [clock cycles]', fontsize=16)
plt.title('Mean Amplitude Value')
plt.xlabel('X Position [mm]', fontsize=16)
plt.ylabel('Y Position [mm]', fontsize=16)

# Set the maximum limit for the y-axis
y_min_limit = 11.1
#plt.ylim(min(y_range), y_max_limit)
plt.ylim(y_min_limit, max(y_range))

# Add grid to represent the pixel cell
pitch_size = 0.035
for i in range(4):
    x_grid = 17.083 + pitch_size * i
    plt.axvline(x=x_grid, color='grey', linestyle='--', linewidth=0.6)
for j in range(12):
    y_grid = 11.115 + pitch_size* j
    plt.axhline(y=y_grid, color='grey', linestyle='--', linewidth=0.6)


# Initialize a list to store mean ToT values for each grid cell
mean_tot_values = []

# Iterate over the grid cells
for i in range(3):
    for j in range(12):
        # Calculate the boundaries of the current grid cell
        x_start = 17.083 + pitch_size * i
        x_end = x_start + pitch_size
        y_start = 11.115 + pitch_size * j
        y_end = y_start + pitch_size

        # Extract the subset of hitmap data within the current grid cell
        subset_hitmap = rotated_hitmap_data[
            np.ix_((y_range >= y_start) & (y_range <= y_end), (x_range >= x_start) & (x_range <= x_end))
        ]

        # Calculate the mean ToT value for the current grid cell
        mean_tot = np.mean(subset_hitmap)

        # Append the mean ToT value to the list
        mean_tot_values.append(mean_tot)

        # Print the result
        print(f"Grid Cell (col = {i}, row = {j}): Mean ToT Value = {mean_tot:.2f} clock cycles")
        plt.text((x_start + x_end) / 2, (y_start + y_end) / 2, f"{mean_tot:.1f}", color='red',
                 ha='center', va='center', fontsize=6)


# We want square pixels
plt.gca().set_aspect('equal')
plt.savefig('ToT_rotated_with_values.pdf')

plt.show()
