import numpy as np
import matplotlib.pyplot as plt

def hitmap(filename='hello.txt'):
    # Read data from file
    data = np.genfromtxt(filename, skip_header=1, usecols=(0, 1, 2), dtype=int)

    # Create a 2D array for the hitmap
    hitmap = np.zeros((16, 64), dtype=int)

    # Fill the hitmap with data
    for row in data:
        col, r, count = row
        if 0 <= r < 16 and 0 <= col < 64:
            hitmap[15 - r, col] = count  # Adjusting the row index

    # Set the size of the figure
    fig, ax = plt.subplots(figsize=(16, 4))

    # Plot the hitmap with square pixels
    cax = ax.imshow(hitmap, cmap='viridis', interpolation='nearest', aspect='auto', vmin=0, vmax=np.max(hitmap))

    # Add colorbar
    cbar = plt.colorbar(cax, label="Counts")

    # Set axis labels
    ax.set_xlabel('Column')
    ax.set_ylabel('Row')

    # Set custom tick values for the axis
    plt.xticks(np.arange(0, 64, 8), np.arange(0, 64, 8))
    plt.yticks(np.arange(0, 16, 2), np.arange(15, -1, -2))  # Reverse the order for the row axis
    plt.title('Hitmap')

    # Show the plot
    plt.show()

# If the script is run as the main program
if __name__ == "__main__":
    hitmap()
