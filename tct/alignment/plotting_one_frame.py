import numpy as np
import matplotlib.pyplot as plt

# Read data
with open('0.txt', 'r') as file:
    lines = file.readlines()

# Initialize a dictionary to store the data for each pixel
data_dict = {}
frame_count = 0

# Initialize variables to track the highest mean value and its corresponding col, row
max_mean_value = 0
max_mean_col = 0
max_mean_row = 0

# Extract data from the file
for line in lines:
    if line.startswith('col'):
        frame_count += 1  # Increment frame count when a new frame starts
        continue  # Skip header lines

    if line.strip() != "-1 -1 -1 -1":
        col, row, value, mode = map(int, line.split()) # Convert values into integers
        index = (col, row)

        # Initialize an empty list for the index if it doesn't exist in the dictionary
        if index not in data_dict:
            data_dict[index] = []

        # Append the value to the list
        data_dict[index].append(value)

# Calculate the average data value for each index
averaged_data = []
for row in range(15, -1, -1):  # Reverse order of rows
    for col in range(1, 65):
        index = (col, row)
        values = data_dict.get(index, [])

        if values:
            mean_value = np.sum(values) / frame_count  # Divide by total number of frames
            rounded_mean_value = round(mean_value, 2)
            averaged_data.append(rounded_mean_value)

            # Update the maximum mean value and corresponding col, row
            if mean_value > max_mean_value:
                max_mean_value = mean_value
                max_mean_col = col
                max_mean_row = row

        else:
            averaged_data.append(np.nan)  # Use NaN for pixels with no entries

# Reshape data for plotting
hitmap_data = np.array(averaged_data).reshape(16, 64)

# Plotting the hitmap
fig, ax = plt.subplots(figsize=(16, 4))  # Set the figure size
im = ax.imshow(hitmap_data, cmap='viridis', interpolation='nearest', vmin=np.nanmin(hitmap_data), vmax=np.nanmax(hitmap_data))
colorbar = plt.colorbar(im)  # Create colorbar without label
colorbar.set_label('ToT [Clock Cycles]', fontsize=16)  # Set label and adjust fontsize

plt.xlabel('Column', fontsize=16)
plt.ylabel('Row', fontsize=16)

# Reduce the number of y-axis ticks to every 4 rows
yticks = np.arange(0, 16, 4)  # Adjust the step based on your preference
plt.yticks(yticks, np.arange(15, -1, -4))

# Set the title of the plot with col, row, and value information of the highest mean value
title_text = f"Max value: {max_mean_value:.2f} [{max_mean_col},{max_mean_row}]"
plt.title(title_text, fontsize=16)

plt.show()
