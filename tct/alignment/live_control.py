import sys
sys.path.append('/home/teleadm/peary/python')
from peary import PearyClient
#import TCTClient
import numpy as np
#from hitmap import hitmap
#import KeithleySMU2400Series
import yaml
import time
import sys
import os
import matplotlib.pyplot as plt

def hitmap(filename='hello.txt'):
    # Read data from file
    data = np.genfromtxt(filename, skip_header=1, usecols=(0, 1, 2), dtype=int)


    hitmap = None
    hitmap = np.zeros((16, 64), dtype=int)

    if len(data) == 0 or np.all(data == -1):
        # If data is empty or contains only -1 values, return an empty hitmap
        return np.zeros((16, 64), dtype=int)

    # Fill the hitmap with data
    for row in data:
        col, r, count = row
        if 0 <= r < 16 and 0 <= col < 64:
            hitmap[15 - r, col] = count  # Adjusting the row index

    return hitmap

def plot_hitmap(hitmap_data):
    global fig, ax, img, integral_text_obj
    if 'fig' not in globals():
        fig, ax = plt.subplots(figsize=(16, 4))
        img = ax.imshow(hitmap_data, cmap='viridis', interpolation='nearest')
        cbar = plt.colorbar(img, ax=ax)
        ax.set_xlabel('Column', fontsize=16)
        ax.set_ylabel('Row', fontsize=16)
        cbar.set_label('ToT [clock cycles]', rotation=90, labelpad=15, fontsize=16)

        integral_text_obj = ax.text(0.5, 1.05, '', transform=ax.transAxes, fontsize=14,
                                    verticalalignment='center', horizontalalignment='center')

    else:
        img.set_data(hitmap_data)
        img.set_clim(vmin=0, vmax=255)  # Update color limits

    integral_value = np.sum(hitmap_data)
    integral_text = f'Integral: {integral_value:.2f}'

    # Update the text with the new integral value
    integral_text_obj.set_text(integral_text)

    plt.xticks(np.arange(0, 64, 8), np.arange(0, 64, 8))
    plt.yticks(np.arange(0, 16, 2), np.arange(15, -1, -2))  # Reverse the order for the row axis
    plt.pause(0.1)

def main(host='localhost'):
    with PearyClient(host=host) as client:
        #tct = TCTClient('fhltctscan.desy.de', 8888)
        hitmap_data = None
        client.clear_devices()
        device = client.add_device('H2M', '/home/root/tct/config/tct.cfg')
        device.power_on()
        time.sleep(1)
        device.configure()
        time.sleep(1)
        device.set_register('dac_vthr', 69)
        time.sleep(1)

        for x in range(10000):
            device.set_register('acq_start', 1)
            time.sleep(0.1)
            device.set_register('matrix_ctrl', 1)
            time.sleep(0.1)
            device.set_memory('shutter', 1500)
            time.sleep(0.1)
            device.set_register('acq_start', 0)
            time.sleep(0.1)
            device.printFrames(1)
            hitmap_data = hitmap('../caribou_data/hello.txt')
            plot_hitmap(hitmap_data)
            os.remove("../caribou_data/hello.txt")
            time.sleep(1)

        device.power_off()

if __name__ == '__main__':
    import sys
    main(*sys.argv[1:])
