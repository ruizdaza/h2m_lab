#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFile.h>
#include <sstream>

using namespace std;

double extract_data(const string& file) {
    ifstream inputFile(file);
    vector<string> lines;

    if (inputFile.is_open()) {
        string line;
        while (getline(inputFile, line)) {
            lines.push_back(line);
        }
        inputFile.close();
    } else {
        cerr << "Unable to open file: " << file << endl;
        return 0.0; // Or handle the case of missing file as appropriate
    }

    vector<string> currentGroup;
    vector<double> dataValues;
    int count = 0;

    for (const string& line : lines) {
        stringstream ss(line);
        string word;
        vector<string> tokens;

        while (ss >> word) {
            tokens.push_back(word);
        }

        if (tokens.size() == 4 && tokens[0] == "col" && tokens[1] == "row" && tokens[2] == "data" && tokens[3] == "mode") {
            currentGroup.clear();
        } else if (tokens.size() == 4 && tokens[0] == "-1" && tokens[1] == "-1" && tokens[2] == "-1" && tokens[3] == "-1") {
            for (const string& entry : currentGroup) {
                stringstream entrySS(entry);
                vector<string> entryTokens;
                string entryWord;

                while (entrySS >> entryWord) {
                    entryTokens.push_back(entryWord);
                }

                if (entryTokens.size() >= 3 ) {
                    if((entryTokens[0] == "0" && entryTokens[1] == "15") ||
                       (entryTokens[0] == "0" && entryTokens[1] == "14") ||
                       (entryTokens[0] == "0" && entryTokens[1] == "13") ||
                       (entryTokens[0] == "0" && entryTokens[1] == "12") ||
                       (entryTokens[0] == "1" && entryTokens[1] == "15") ||
                       (entryTokens[0] == "1" && entryTokens[1] == "14") ||
                       (entryTokens[0] == "1" && entryTokens[1] == "14") ||
                       (entryTokens[0] == "1" && entryTokens[1] == "13") ||
                       (entryTokens[0] == "1" && entryTokens[1] == "12") ||
                       (entryTokens[0] == "2" && entryTokens[1] == "15") ||
                       (entryTokens[0] == "2" && entryTokens[1] == "14") ||
                       (entryTokens[0] == "2" && entryTokens[1] == "14") ||
                       (entryTokens[0] == "2" && entryTokens[1] == "13") ||
                       (entryTokens[0] == "2" && entryTokens[1] == "12") ||
                       (entryTokens[0] == "3" && entryTokens[1] == "15") ||
                       (entryTokens[0] == "3" && entryTokens[1] == "14") ||
                       (entryTokens[0] == "3" && entryTokens[1] == "14") ||
                       (entryTokens[0] == "3" && entryTokens[1] == "13") ||
                       (entryTokens[0] == "3" && entryTokens[1] == "12")){
                      dataValues.push_back(1); // Append the corresponding value as double
                      count++;
                      break; // Break once the data is found for this group
                    }
                }
            }
        } else {
            currentGroup.push_back(line);
        }

        if (count == 100) {
            break;
        }
    }

    double averageValue = 0.001;
    if(!dataValues.empty()){
        double sum = 0.0;
        for(const double& value : dataValues){
            sum += value;
        }
        averageValue = sum / 100; // Calculate average value
    }

    return averageValue;
}

void createHistogram(const string& folderName, const double zCoordinate, const string& outputFileName) {
    const int numBinsX = 70;
    const int numBinsY = 70;
    const double minX = 17.079;
    const double maxX = 17.217;
    const double minY = 10.960;
    const double maxY = 11.098;
    const double stepX = 0.002;
    const double stepY = 0.002;

    ostringstream histogramName;
    histogramName << "Histogram_Z_" << fixed << setprecision(3) << zCoordinate;

    TH2D *histogram = new TH2D(histogramName.str().c_str(), "Your Histogram Title",
                               numBinsX, minX - 0.001, maxX + 0.001, numBinsY, minY - 0.001, maxY + 0.001);

    for (int i = 0; i < numBinsX; ++i) {
        for (int j = 0; j < numBinsY; ++j) {
            double x = minX + i * stepX;
            double y = minY + j * stepY;

            ostringstream ossX, ossY, ossZ;
            ossX << fixed << setprecision(3) << x;
            ossY << fixed << setprecision(3) << y;
            ossZ << fixed << setprecision(3) << zCoordinate;

            string filename = ossX.str() + "_" + ossY.str() + "_" + ossZ.str() + ".txt";

            double value = extract_data(filename);

            histogram->Fill(x, y, value);
        }
    }

    // Set the COLZ option for the histogram
    histogram->SetOption("COLZ");
    // Save the histograms to the output file
    TFile *outputFile = new TFile(outputFileName.c_str(), "UPDATE");
    histogram->Write("", TObject::kOverwrite);
    outputFile->Close();
    //TCanvas *canvas = new TCanvas("canvas", "Your Canvas Title", 800, 600);
    //histogram->Draw("colz");
}

void plot2D() {
    //vector<string> folders = {
    //    "36.760", "36.770", "36.780", "36.790", "36.800",
    //    "36.810", "36.820", "36.830", "36.840", "36.850"
    //};

    //vector<double> zCoordinates = {
    //    36.760, 36.770, 36.780, 36.790, 36.800,
    //    36.810, 36.820, 36.830, 36.840, 36.850
    //};

    // Specify the output file name
    string outputFileName = "output_histograms.root";

    // Loop through folders and create histograms
    //for (size_t i = 0; i < folders.size(); ++i) {
        createHistogram("" , 36.760, outputFileName);
    //}
}
