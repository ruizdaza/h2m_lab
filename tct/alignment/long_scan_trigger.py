from peary import PearyClient
from tct_remote_client import TCTClient
import numpy as np
#import hitmap
#import KeithleySMU2400Series
import yaml
import time
import sys
import os
import matplotlib.pyplot as plt
import shutil

tct = TCTClient('fhltctscan.desy.de', 8888)

def main(host='localhost'):
    with PearyClient(host=host) as client:
        hitmap_data = None
        client.clear_devices()
        device = client.add_device('H2M', '/home/root/tct/config/tct_trigger.cfg')
        device.power_on()
        time.sleep(1)
        device.configure()
        time.sleep(1)
        vthr = [80, 81, 82]
        #vthr = [83, 87, 90, 95]
        for vatt in vthr:
            device.set_memory('sc_ctrl_mode', 0)
            device.set_register('dac_vthr', vatt)
            time.sleep(1)

            z_array = [36.76]
            #x_array = [17.304, 17.2]
            #y_array = [10.807, 10.945]
            x_array = [
                17.217, 17.215, 17.213, 17.211, 17.209, 17.207, 17.205, 17.203, 17.201, 17.199,
                17.197, 17.195, 17.193, 17.191, 17.189, 17.187, 17.185, 17.183, 17.181, 17.179,
                17.177, 17.175, 17.173, 17.171, 17.169, 17.167, 17.165, 17.163, 17.161, 17.159,
                17.157, 17.155, 17.153, 17.151, 17.149, 17.147, 17.145, 17.143, 17.141, 17.139,
                17.137, 17.135, 17.133, 17.131, 17.129, 17.127, 17.125, 17.123, 17.121, 17.119,
                17.117, 17.115, 17.113, 17.111, 17.109, 17.107, 17.105, 17.103, 17.101, 17.099,
                17.097, 17.095, 17.093, 17.091, 17.089, 17.087, 17.085, 17.083, 17.081, 17.079
            ]

            y_array = [
                10.960, 10.962, 10.964, 10.966, 10.968, 10.970, 10.972, 10.974, 10.976, 10.978,
                10.980, 10.982, 10.984, 10.986, 10.988, 10.990, 10.992, 10.994, 10.996, 10.998,
                11.000, 11.002, 11.004, 11.006, 11.008, 11.010, 11.012, 11.014, 11.016, 11.018,
                11.020, 11.022, 11.024, 11.026, 11.028, 11.030, 11.032, 11.034, 11.036, 11.038,
                11.040, 11.042, 11.044, 11.046, 11.048, 11.050, 11.052, 11.054, 11.056, 11.058,
                11.060, 11.062, 11.064, 11.066, 11.068, 11.070, 11.072, 11.074, 11.076, 11.078,
                11.080, 11.082, 11.084, 11.086, 11.088, 11.090, 11.092, 11.094, 11.096, 11.098
            ]

            #x_array = [17.304, 17.2]
            #y_array = [10.807, 19.945]

            for zatt in z_array:
                for yatt in y_array:
                    for xatt in x_array:
                        print(xatt, yatt, zatt)
                        tct.move(xatt, yatt, zatt)
                        time.sleep(1)

                        device.set_memory('sc_ctrl_mode', 1)
                        device.startPG(100) #for each position, record 100 frames

                        while device.get_memory('pg_running') == 0:
                            time.sleep(0.1)

                        device.printFrames(100,f"vth{vatt}_{xatt:.3f}_{yatt:.3f}_{zatt:.3f}.txt")
                        #shutil.copyfile("../caribou_data/hello.txt", f"../caribou_data/{xatt:.3f}_{yatt:.3f}_{zatt:.3f}.txt")
                        #time.sleep(0.2)
                        #os.remove("../caribou_data/hello.txt")

        device.power_off()

if __name__ == '__main__':
    import sys
    main(*sys.argv[1:])

