import glob
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # Importing 3D plotting library

# Define the function to extract data
def extract_data(file):
    with open(file, 'r') as f:
        lines = f.readlines()

    # Initialize variables
    data_values = []
    current_group = []
    count = 0

    # Loop through lines in the file
    for line in lines:
        line = line.strip().split()

        # Check for the start of a group
        if line == ['col', 'row', 'data', 'mode']:
            current_group = []
        # Check for the end of a group
        elif line == ['-1', '-1', '-1', '-1']:
            # Check if the group contains the required data
            for entry in current_group:
                if entry[:2] == ['28', '8']:
                    data_values.append(float(entry[2]))  # Appending the corresponding value as float
                    count += 1
                    break  # Break once the data is found for this group

        # Append lines within the group
        else:
            current_group.append(line)

        # Break if 100 groups are found
        if count == 100:
            break

    return data_values

# List all .txt files in the directory
files = glob.glob("scan/*.txt")

# Initialize dictionaries to store coordinates and average data values
coordinates = {}
averages = {}

# Loop through each file and extract data
for file in files:
    data = extract_data(file)
    average_value = sum(data) / 100 if data else 0  # Calculate average value

    # Extract coordinates from file name
    filename = file.split("/")[-1]  # Get the file name from the path
    coordinates[filename[:-4]] = tuple(map(float, filename[:-4].split("_")))  # Store coordinates
    averages[filename[:-4]] = average_value  # Store average value

# Extract x, z coordinates and corresponding average values
x = [coord[0] for coord in coordinates.values()]
z = [coord[2] for coord in coordinates.values()]
average_values = list(averages.values())

# Create a 3D scatter plot
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')  # Create a 3D subplot

# Plot 3D scatter with X and Z as base, and value as amplitude
ax.scatter(x, z, average_values, c=average_values, cmap='viridis', s=100)

# Set labels and title
ax.set_xlabel('X')
ax.set_ylabel('Z')
ax.set_zlabel('ToT [clock cycle]')
#ax.set_title('3D Plot with X and Z as Base')

# Show plot
plt.show()
