#!/bin/bash

# Loop through the specified entries in steps of 0.01
for ((i=36400; i<=38300; i=i+10)); do
    # Convert the number to the format required in the filename
    val=$(bc <<< "scale=3; $i / 1000")
    
    # Check if files matching the pattern exist
    if [ -n "$(shopt -s nullglob; echo *"$val".txt)" ]; then
        # Create a directory with the $val name
        mkdir "$val"
        
        # Move files matching the pattern to the created directory
        mv *"$val".txt "$val"/
    fi
done
