from peary import PearyClient
#import TCTClient
from tct_remote_client import TCTClient
import numpy as np
#from hitmap import hitmap
#import KeithleySMU2400Series
import yaml
import time
import sys
import os
import matplotlib.pyplot as plt

def hitmap(filename='hello.txt'):
    # Read data from file
    data = np.genfromtxt(filename, skip_header=1, usecols=(0, 1, 2), dtype=int)


    hitmap = None
    hitmap = np.zeros((16, 64), dtype=int)

    if len(data) == 0 or np.all(data == -1):
        # If data is empty or contains only -1 values, return an empty hitmap
        return np.zeros((16, 64), dtype=int)

    # Fill the hitmap with data
    for row in data:
        col, r, count = row
        if 0 <= r < 16 and 0 <= col < 64:
            hitmap[15 - r, col] = count  # Adjusting the row index

    return hitmap

def plot_hitmap(hitmap_data):
    print(np.max(hitmap_data))

def main(host='localhost'):
    with PearyClient(host=host) as client:
        tct = TCTClient('fhltctscan.desy.de', 8888)
        hitmap_data = None
        client.clear_devices()
        device = client.add_device('H2M', '/home/root/tct/config/tct_trigger.cfg')
        device.power_on()
        time.sleep(1)
        device.configure()
        time.sleep(1)
        tct.move(17.13, 11.06, 36.76)
        device.set_register('dac_vthr', 83)
        time.sleep(1)

        device.set_memory('sc_ctrl_mode', 1)
        #device.configurePG("/home/root/tct/config/pg_tct.conf")

        # while device.get_memory("pg_running"):
        step = 0.005
        yatt = 11.06
        for x in range(40):
            yatt = 11.06+x*step
            tct.move(17.13, yatt, 36.76)
            time.sleep(1)
            device.startPG(1)
            time.sleep(0.5)
            device.printFrames(1,"hello.txt")
            hitmap_data = hitmap('../caribou_data/hello.txt')
            plot_hitmap(hitmap_data)
            os.remove("../caribou_data/hello.txt")
            time.sleep(0.5)

        device.power_off()

if __name__ == '__main__':
    import sys
    main(*sys.argv[1:])
