## Available lasers at DESY

- Red laser: 650 nm (orange cable)

- Infrared laser: 1064 nm


## How to turn on the laser?
- Frecuency of the laser controlled by a pulse generator. So far, 1 KHz, 0-3V splitted in two (one for Caribou, one for the laser). Square waveform. Low = 0, high = 3V

- Delay module:

    - To Caribou directly (3ns long cable).

    - For the laser, we delay with the delay module. Everything ~200 ns.

- Power source:

    - For the red laser: 1 MHz in external trigger mode

- Filters to have a MIP: 0.3, 0.5, 2 from left to right

- Port 1 to the reference (91.5%). Port 2  to the laser (8.5% of the light). Port T to the trigger (yellow cable).

- 12 V to bias the reference diode. The output is connected to the oscilloscope.

- Close the cover, arm with the key, turn on the laser.




## How to move the stage?
```
cd tct-controllers
python3 gui.py  
```

Good initial z position: Z = 36.760 mm, X = 17 mm, Y = 15.2 mm.

Measurements/ Remote control via network / Configure and run

To close it: Abort + clic to close window


## Set up the measurements

Mount in caribou, being in the home folder:  `sudo mount 131.169.133.67:/home/teleuser/h2m_lab/tct/caribou_data /home/root/tct/data/ -o rw`


In carboard:
cd `/home/root/tct/data`
Run: `pearyd`

In the lab PC:
`cd /home/teleuser/h2m_lab/tct/alignment`
`python3 live_control.py 133.169.133.243`, where the last IP corresponds to caribou0.

## Available scripts

- `hitmap.py`. Plots the hitmap reading the output file.

- `live_control.py`. Online monitor that plots a hitmap during data taking based on random open and closing shutter of H2M.

- `live_control_trigger.py`. Online monitor that plots a hitmap based on the opening of the shutter with the incoming trigger signal.

- `long_scan_trigger.py`. Data taking script that syncronizes with the stage. The output files can be analysed with the following scripts.

-  
