## DAC scans

# Introduction

The H2M analog periphery consists of 6 biasing DACs that are used for biasing the
front-end to the desired operating point.

The user can monitor one of the internally generated bias voltages by writing to
```ANALOG_OUT_CTRL``` register. A function in Peary called ```read_analog_out``` has
been created to get the voltage value. Furthermore, the function ```scanDAC``` allows for flexible DAC values scans.

# Example
The following example runs a scan for the ```dac_ibias```. It goes from 0 to 255 in steps of 1, and each measurement is repeated 100 times with a delay of 1000 ms. The output is saved in ```my_scan_result.csv``` file.

```
add_device H2M
powerOn 0
configure 0
setRegister analog_out_ctrl 0x1 0
scanDAC dac_ibias 0 1000 100 1 my_scan_result read_analog_out 0
```

The last two lines must be changed for each DAC, according to Table 8 in the manual. For example, a scan of the ```dac_vthr```, can be done:
```
add_device H2M
powerOn 0
configure 0
setRegister analog_out_ctrl 0x4 0
scanDAC dac_vthr 0 255 1000 100 my_scan_result read_analog_out 0
```

# plotting
The output .csv can be plotted using the ```analysis.py``` script.
```
python3 analysis.py my_scan_result.csv
```
