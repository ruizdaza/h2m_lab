## DAC scans

# Introduction

The H2M analog periphery consists of 6 biasing DACs that are used for biasing the
front-end to the desired operating point.

The user can monitor one of the internally generated bias voltages by writing to
```ANALOG_OUT_CTRL``` register. A function in Peary called ```readAnalogOut``` has
been created to get the voltage value. In this way automatized measurements can be done.


# Available scripts

- ```generate_dac_scan.py``` is a python script that generates dac scans.

- ```dac_*.txt``` are examples of the txt file obtained after running ```python3 generate_dac_scan.py```.

- ```*_scan.txt``` are examples of the output obtained in the terminal (copied and pasted in a txt file) after running ```pearycli -r dac_*.txt```.

- ```analysis_dac.py``` is a python script the plots the voltage vs DACs.

- ```*.pdf``` are examples of plots obtained after running ```python3 analysis_dac.py```.

All the Python scripts must be modified depending on the DAC value desired to be measured.
Lines that should be changed are indicated in each script.

All the results included in this file are with H2M-2, without biasing the chip.
In the folder ```other_studies``` are results for biasing the chip at -4.8V and for H2M-3.
