from matplotlib import pyplot as plt

# read data from the given txt file name
with open('vthr_scan_h2m3.txt', 'r') as file:        #change for each dac
    lines = file.readlines()

default = 70                                         #change for each dac
# 32 for ibias, itrim, ikrum
# 70 for vthr, vref
# 0 for vtpulse

# get voltage values
v_values = []

for line in lines:
    if 'ADC =' in line:
        v_value = float(line.split('=')[1].strip())
        v_values.append(v_value)

# create adc list
adc_values = list(range(len(v_values)))

# plot
plt.plot(adc_values, v_values, label = "measurement")
plt.xlabel("DAC", fontsize = 16)
plt.ylabel("voltage [V]", fontsize = 16)
plt.title("DAC_VTHR", fontsize = 16)                    #change for each dac

# adding default values
plt.axvline(x = default, color='gray', linestyle='dashed', label = "default DAC value")

plt.legend()

plt.savefig('vthr_scan_h2m3.pdf')                       #change for each dac
plt.show()
#print("V = ", v_values)
#print("ADC = ", adc_values)
