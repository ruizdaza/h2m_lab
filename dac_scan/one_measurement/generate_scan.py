with open('dac_ibias.txt', 'w') as file: #this overwrites, change for each dac
	file.write(f"add_device H2M \n")
	file.write(f"powerOn 0 \n")
	file.write(f"setVoltage v_pwell 0 0 \n")
	file.write(f"setVoltage v_sub 0 0 \n")
	file.write(f"configure 0 \n")
	file.write(f"setRegister analog_out_ctrl 0x1 0 \n") #change for each dac (Manual, table 8)
	file.write("delay 1000 \n")
	for dac_value in range(256):
		file.write(f"setRegister dac_ibias {dac_value} 0 \n") #change for each dac
		file.write("delay 1000 \n")
		file.write(f"readAnalogOut 0 \n")
