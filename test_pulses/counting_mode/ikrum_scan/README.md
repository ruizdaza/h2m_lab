# ikrum scan

- In ```config``` there are examples of configurations and scripts to run the ikrum scan (with and without) test pulses

- ```generate_scan.py``` generates a script that can be used to run the scan.
 ```
 pearycli -c configs/scanKrumCurrent_tpOn.cfg -r scripts/ikrum_scan.src
 ```

- ```H2M_scanReader.C``` reads the binnary noise folders, so those runned with ```ikrum_scan_tpOff.src```, and generates the root files
```
root -l 'H2M_scanReader.C("data_tpOff/ikrum.bin")'
```

-  ```H2M_scanReader_erf.C``` reads the measurements folders, so those runned with ```ikrum_scan_tpOff.src```
```
root -l 'H2M_scanReader.C("data_tp/ikrum.bin")'
```

- ```create_root_files.py``` runs the H2M_scanReader over all the binnary files, creating root files.

- ```draw_ikrum_scan.C``` draw the amplitude, noise, SNR... as function of the ikrum.

- 'data_tp' and 'data_tpOff' can be found in '/pnfs/desy.de/ftx-tb/ruizdaza/h2m/lab/ikrum/' and in CERNbox.
