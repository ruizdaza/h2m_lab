import matplotlib.pyplot as plt
import numpy as np

# Assuming your data is stored in a file named 'data.txt'
# Load the data from the file
data = np.loadtxt('output_data_perpixel.txt')

# Filter data for the desired pixel
filtered_data = data[(data[:, 0] == 2) & (data[:, 1] == 12)]

# Extract the columns for amplitude and dac_vtpulse
amplitude = filtered_data[:, 2]
dac_vtpulse = filtered_data[:, 3]

# Get the indices that would sort dac_vtpulse
sorted_indices = np.argsort(dac_vtpulse)

# Sort dac_vtpulse and amplitude using the sorted indices
dac_vtpulse_sorted = dac_vtpulse[sorted_indices]
amplitude_sorted = amplitude[sorted_indices]

# Plot the data
plt.plot(dac_vtpulse_sorted, amplitude_sorted, marker='o', linestyle='-')
plt.xlabel('dac_vtpulse')
plt.ylabel('amplitude [dac_vthr]')
plt.xlim(0, 254)
plt.ylim(70, 260)

plt.show()
