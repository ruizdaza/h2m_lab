import matplotlib.pyplot as plt
import numpy as np

# Read data for 0 V
data_0V = np.loadtxt('/Users/atlaslap37/Desktop/h2m_lab/test_pulses/counting_mode/vtpulse_scan/data_h2m3/data_1V2_ikrum38/output_data_h2m3.txt')
vtpulse_0V = data_0V[:, 0]
amplitude_0V = data_0V[:, 1]
error_vtpulse_0V = data_0V[:, 2]
error_amplitude_0V = data_0V[:, 3]
plt.errorbar(vtpulse_0V, amplitude_0V, yerr=error_amplitude_0V, marker='^', linestyle='None', capsize=5, color='green', label='ikrum = 38')
plt.xlim(0, 254)

# Read data for -3.6 V
data_3V6 = np.loadtxt('/Users/atlaslap37/Desktop/h2m_lab/test_pulses/counting_mode/vtpulse_scan/data_h2m3/data_1V2_ikrum32/output_data_h2m3.txt')
vtpulse_3V6 = data_3V6[:, 0]
amplitude_3V6 = data_3V6[:, 1]
error_vtpulse_3V6 = data_3V6[:, 2]
error_amplitude_3V6 = data_3V6[:, 3]
plt.errorbar(vtpulse_3V6, amplitude_3V6, yerr=error_amplitude_3V6, marker='s', linestyle='None', capsize=5, color='blue', label='ikrum = 32')

# Read data for -2.4 V
data_2V4 = np.loadtxt('/Users/atlaslap37/Desktop/h2m_lab/test_pulses/counting_mode/vtpulse_scan/data_h2m3/data_1V2_ikrum21/output_data_h2m3.txt')
vtpulse_2V4 = data_2V4[:, 0]
amplitude_2V4 = data_2V4[:, 1]
error_vtpulse_2V4 = data_2V4[:, 2]
error_amplitude_2V4 = data_2V4[:, 3]
plt.errorbar(vtpulse_2V4, amplitude_2V4, yerr=error_amplitude_2V4, marker='o', linestyle='None', capsize=5, color='red', label='ikrum = 21')




# Read data for -1.2 V
data_1V2 = np.loadtxt('/Users/atlaslap37/Desktop/h2m_lab/test_pulses/counting_mode/vtpulse_scan/data_h2m3/data_1V2_ikrum19/output_data_h2m3.txt')
vtpulse_1V2 = data_1V2[:, 0]
amplitude_1V2 = data_1V2[:, 1]
error_vtpulse_1V2 = data_1V2[:, 2]
error_amplitude_1V2 = data_1V2[:, 3]
plt.errorbar(vtpulse_1V2, amplitude_1V2, yerr=error_amplitude_1V2, marker='v', linestyle='None', capsize=5, color='black', label='ikrum = 19')






# Set labels and display the plot
plt.xlabel('dac_vtpulse', fontsize=16)
plt.ylabel('amplitude [dac_vthr]', fontsize=16)
plt.legend(fontsize=16, frameon=False)
plt.show()
