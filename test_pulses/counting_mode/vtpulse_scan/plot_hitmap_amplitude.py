import matplotlib.pyplot as plt
import numpy as np

n_col = 3  # Adjusted the number of columns
n_row_start = 4
n_row_end = 16

# Read data from the text file
data = np.loadtxt("hitmap_amplitude_values.txt", skiprows=1)
col, row, amplitude = data[:, 0], data[:, 1], data[:, 2]

# Filter data based on specified column and row ranges
filtered_data = data[(col >= 0) & (col <= n_col) & (row >= n_row_start) & (row <= n_row_end) & (col < n_col)]

# Extract filtered columns and rows
filtered_col, filtered_row, filtered_amplitude = filtered_data[:, 0], filtered_data[:, 1], filtered_data[:, 2]

# Create a 2D histogram (hitmap) with proper binning for the filtered data
plt.hist2d(filtered_col, filtered_row, weights=filtered_amplitude,
           bins=[np.arange(0, n_col + 1, 1), np.arange(n_row_start, n_row_end + 1, 1)],
           cmin=1, cmap='viridis')
cbar = plt.colorbar(label='Amplitude [dav_vthr]')
cbar.ax.tick_params(labelsize=12)  # Set the fontsize for colorbar ticks
cbar.ax.set_ylabel('Mean Amplitude Value [dav_vthr]', fontsize=16)  # Set the fontsize for colorbar label
plt.title('Mean Amplitude Value')
plt.xlabel('Column', fontsize=16)
plt.ylabel('Row', fontsize=16)

# Adjust ticks per unit on the x-axis
plt.xticks(np.arange(0, n_col + 1, 1), fontsize=12)

# Adjust ticks per unit on the y-axis
plt.yticks(np.arange(n_row_start, n_row_end + 1, 1), fontsize=12)

# We want square pixels
plt.gca().set_aspect('equal')
plt.tight_layout()
plt.show()
plt.savefig('hitmap_amplitude.pdf')
