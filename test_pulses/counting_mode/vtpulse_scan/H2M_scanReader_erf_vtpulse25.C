// Analysis macro for H2M dac-scans
#include <TLatex.h>


void H2M_scanReader_erf_vtpulse25(string filename = "", int plotAt = 255) {

  size_t strip_from = filename.find_last_of(".");
  std::string rootFileName = Form("%s.root", filename.substr(0, strip_from).c_str());

  // H2M chip parameters
  const int n_col = 64;
  const int n_row = 16;
  const int n_dac = 256;
  const int n_tdc = 256;

  TH2D* h_curve_2D =
    new TH2D("curve_2D", ";amplitude;occupancy;entries", n_dac, 0 - 0.5, n_dac - 0.5, n_dac, 0 - 0.5, n_dac - 0.5);
  TH1D* h_curve_sum = new TH1D("curve_sum", ";amplitude;occupancy;entries", n_dac, 0 - 0.5, n_dac - 0.5);
  TH2D* occupancy = new TH2D("occupancy", "row; column; entries", n_col, 0 - 0.5, n_col - 0.5, n_row, 0 - 0.5, n_row - 0.5);

  TH2D* occupancy_sum =
    new TH2D("occupancy_sum", "row; column; entries", n_col, 0 - 0.5, n_col - 0.5, n_row, 0 - 0.5, n_row - 0.5);
  TH2D* failed_fits =
      new TH2D("failed fits", "row; column; entries", n_col, 0 - 0.5, n_col - 0.5, n_row, 0 - 0.5, n_row - 0.5);
  TH1D* h_response_sum = new TH1D("response_sum", ";response [tdc];entries", n_tdc, 0 - 0.5, n_tdc - 0.5);

  TH1D* h_pixel_responses[n_col][n_row];
  for(int col = 0; col < n_col; col++) {
    for(int row = 0; row < n_row; row++) {
      h_pixel_responses[col][row] =
        new TH1D(Form("h_pixel_response_%i_%i", col, row), ";response [tdc];entries", n_tdc, 0 - 0.5, n_tdc - 0.5);
    }
  }

  // fill arrays in loops
  int i = 0;
  double occ_map_pulse[n_col][n_row][n_dac] = {0};
  std::vector<double> ampl;
  ampl.resize(n_dac, 0);

  // fit parameter
  float amp[n_col][n_row];
  float noise[n_col][n_row];
  float chi[n_col][n_row];
  float err[n_col][n_row];

  // open data file
  std::ifstream data_in;
  data_in.open(filename.c_str(), std::ios::ate | std::ios::binary);

  // output file to save histogram
  TFile* fout = new TFile(rootFileName.c_str(), "RECREATE");
  TDirectory* dir = fout->GetDirectory("");
  TDirectory* dir_single = dir->mkdir("single_pixel_plots");
  TTree* T = new TTree("trim_data", "pixel thresholds for trimming");
  T->SetAutoSave(0);
  double tcol, trow, tamp, tsigma, terr;
  T->Branch("col", &tcol);
  T->Branch("row", &trow);
  T->Branch("amp", &tamp);
  T->Branch("sigma", &tsigma);
  T->Branch("error", &terr); // uncertainty for mean value

  std::cout << filename.c_str() << std::endl;

  if(data_in) {

    std::cout << filename.c_str() << std::endl;

    // get length of file:
    data_in.seekg(0, data_in.end);
    auto length = data_in.tellg();
    data_in.seekg(0, data_in.beg);
    std::cout << "Reading " << length << " bytes " << std::endl;

    // buffer for one block of testpulses
    std::streamsize blocksize = n_row * n_col + 6;
    uint16_t block[blocksize];

    // containers for the occupancy map and amplitude
    uint16_t occupancy_map[n_col][n_row] = {0};
    uint16_t amplitude = 0;
    uint16_t scan_dac_i = 0;
    uint16_t scan_start = 0;
    uint16_t scan_step = 0;
    uint16_t scan_end = 0;
    uint16_t scan_n = 0;

    // reading data from binary file
    int counts;
    while(data_in.read(reinterpret_cast<char*>(&block), 2 * blocksize)) {

      counts = 0;

      for(size_t col = 0; col < n_col; col++) {
        for(size_t row = 0; row < n_row; row++) {
          occupancy_map[col][row] = block[col + row * n_col];
        }
      }
      amplitude = block[n_col * n_row + 0];
      scan_start = block[n_col * n_row + 1];
      scan_step = block[n_col * n_row + 2];
      scan_end = block[n_col * n_row + 3];
      scan_n = block[n_col * n_row + 4];
      scan_dac_i = block[n_col * n_row + 5];
      if(amplitude==scan_start)
        continue;
      for(int col = 0; col < n_col; col++) {
        for(int row = 0; row < n_row; row++) {

          // fill containers and histograms for data analysis and plotting
          h_curve_2D->Fill(amplitude, occupancy_map[col][row]);
          h_curve_sum->Fill(amplitude, occupancy_map[col][row]);

          // makes sense for test pulse injection with scan_n = number of testpulses but not for noise scans
          //h_curve_2D->Fill(amplitude, 100.0 * occupancy_map[col][row] / scan_n);
          //h_curve_sum->Fill(amplitude, 100.0 * occupancy_map[col][row] / scan_n);

          // this makes only sense for e.g. TOT spectra
          h_pixel_responses[col][row]->Fill(occupancy_map[col][row]);
          if(!((col == 28 && row == 7) || (col == 44 && row == 15))) { // exclude noisy
            h_response_sum->Fill(occupancy_map[col][row]);
          }

          // this makes only sense for dac_scans
          if(i < n_dac) {
            ampl[i] = amplitude;

            occ_map_pulse[col][row][i] = occupancy_map[col][row];
            // makes sense for test pulse injection with scan_n = number of testpulses but not for noise scans
            //occ_map_pulse[col][row][i] = 100.0 * occupancy_map[col][row] / scan_n;
          }
        }
      }

      // this counts the number of block read (0== scan_start)
      i = i + 1;
    }

    TF1* fitfunction;
    fitfunction = new TF1("fitfunction", "0.5*[2]*(1-erf((x-[0])/[1]))", scan_start, scan_end);

    TH1D* h_amplitude = new TH1D("h_amplitude", ";amplitude;entries", n_dac * 10, 0 - 0.5, n_dac - 0.5);
    TH1D* h_noise = new TH1D("h_noise", ";noise;entries", n_dac * 10, 0 - 0.5, n_dac - 0.5);
    TH2D* hitmap_amplitude = new TH2D("hitmap_amplitude", "amplitude [dac_vthr]; row; column", n_col, 0 - 0.5, n_col - 0.5, n_row, 0 - 0.5, n_row - 0.5);

    for(int col = 0; col < n_col; col++) {
      for(int row = 0; row < n_row; row++) {

        // make one histogram per pixel
        TH1D* h_pixel_curve =
          new TH1D(Form("h_pixel_curve_%i_%i", col, row), ";amplitude;occupancy", n_dac, 0 - 0.5, n_dac - 0.5);

        for(int i = 0; i < n_dac; i++) {
          h_pixel_curve->SetBinContent(h_pixel_curve->FindBin(ampl[i]), occ_map_pulse[col][row][i]);
          occupancy_sum->Fill(col, row, occ_map_pulse[col][row][i]);
        }
        int map_index = find(ampl.begin(), ampl.end(), plotAt) - ampl.begin();
        occupancy->Fill(col, row, occ_map_pulse[col][row][map_index]);

        bool failed = false;

	// lets keep the church in the village
	double assumed_amp = 99;
	double assumed_width = 2;
	double edge_at = h_pixel_curve->GetBinCenter(h_pixel_curve->FindLastBinAbove(assumed_amp/2.));
	fitfunction->SetParameters(edge_at, assumed_width, assumed_amp);

	// very strict on parameter limits otherwise fits fail
  fitfunction->SetParLimits(0, edge_at - assumed_width, edge_at + assumed_width);
	fitfunction->SetParLimits(1,0,5);
	fitfunction->SetParLimits(2,0.5*assumed_amp,2*assumed_amp);

	auto fit_result = h_pixel_curve->Fit("fitfunction", "RIQ", "", edge_at - assumed_width*3, edge_at + assumed_width*3);
	if(fit_result != 0) {
	  std::cout << "FAILED FIT! col row: " << col << " " << row << std::endl;
	  failed = true;
	}
  // Add the fit result as a label to the plot
  double fitResultAmp = fitfunction->GetParameter(0);
  TLatex label;
  label.SetTextSize(0.03);
  label.SetTextAlign(22);  // Centered
  label.SetTextColor(kRed);  // You can choose a different color if needed
  label.DrawLatex(fitResultAmp, h_pixel_curve->GetMaximum(), Form("Amp: %.2f", fitResultAmp));

          err[col][row] = h_pixel_curve->GetRMS()/sqrt(h_pixel_curve->Integral(0,255));

        dir_single->cd();
        h_pixel_curve->Write();
        h_pixel_responses[col][row]->Write();

	// do not store fit results of failed fits
        if(!failed){
          amp[col][row] = fitfunction->GetParameter(0);
          noise[col][row] = fitfunction->GetParameter(1);
          chi[col][row] = fitfunction->GetChisquare() / fitfunction->GetNDF();

          dir->cd();
          tcol = col;
          trow = row;
          tamp  = fitfunction->GetParameter(0);
          tsigma = fitfunction->GetParameter(1);
          terr = err[col][row];
          T->Fill();
        }
        else{
          dir->cd();
          failed_fits->Fill(col,row);
          amp[col][row] = n_dac;
          noise[col][row] = n_dac;
	  chi[col][row] = 1000;
        }
      }
    } // col

    for(int col = 0; col < n_col; col++) {
      for(int row = 0; row < n_row; row++) {

        h_amplitude->Fill(amp[col][row]);
        h_noise->Fill(noise[col][row]);
        std::cout << "Filling hitmap_amplitude for col " << col << ", row " << row << ": amp = " << amp[col][row] << std::endl;

        hitmap_amplitude->Fill(col, row, amp[col][row]);
        std::cout << "col: " << col << " row: " << row << std::endl;
        if(chi[col][row] > 5) {
          std::cout << "  WARNING: bad chi squared / ndof: " << chi[col][row] << std::endl;
        }
        std::cout << "  threshold " << amp[col][row] << std::endl;
        std::cout << "  noise " << noise[col][row] << std::endl;
        std::cout << "  chi squared / ndof " << chi[col][row] << std::endl;
        //std::cout << "  statistical uncertainty for mean " << err[col][row] << std::endl;
      }
    }

    // Open a text file for writing
    std::ofstream outputFile("hitmap_amplitude_values.txt");

    // Check if the file is open
    if (outputFile.is_open()) {
      outputFile << "col\trow\tamplitude\n";

      for (int col = 0; col < n_col; col++) {
        for (int row = 0; row < n_row; row++) {
          double amplitudeValue = amp[col][row];
          outputFile << col << "\t" << row << "\t" << amplitudeValue << "\n";
        }
      }

      // Close the output file
      outputFile.close();
      std::cout << "Hitmap_amplitude values saved to 'hitmap_amplitude_values.txt'" << std::endl;
    } else {
      std::cerr << "Failed to open the output file for hitmap_amplitude values." << std::endl;
    }


    h_curve_2D->Write();
    h_curve_sum->Write();
    h_response_sum->Write();
    occupancy->Write();
    occupancy_sum->Write();
    h_amplitude->Write();
    hitmap_amplitude->Write();
    h_noise->Write();
    failed_fits->Write();
    T->Write();
    fout->Close();

  } // if open
  else {
    std::cout << "Failed to open " << filename << std::endl;
  }
  gSystem->Exit(0);
}
