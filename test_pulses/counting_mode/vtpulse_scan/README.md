# DAC_VTPULSE scan

```
pearycli -r scripts/vtpulse_scan.src -c config/scan.conf
```

This produce .bin files. Root files can be created out of them:

``` python3 create_root_files.py ```

This runs ```H2M_scanReader_erf.C``` for all the root files. The data from the root files is analysed using ```draw_vtpulse_scan.C```. Nicer plots with Python can be done, because it also saves the data in a .txt file that can be read by ```draw_vtpulse_scan.py```, with assumed_amp = 20.
It also creates a txt file with the amplitude per pixel, which can be plotted using ```plot_amplitude_vs_vtpulse_perpixel.py```


Individual root files can be created by using ```root -l 'H2M_scanReader_erf_vtpulse25.C("data/vtpulse_25.bin")' ```, which has optimazed parameters for the fit.

