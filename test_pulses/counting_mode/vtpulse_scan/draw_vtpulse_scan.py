import matplotlib.pyplot as plt
import numpy as np

# Read data from the file
data = np.loadtxt('output_data.txt')

# Extract columns
vtpulse = data[:, 0]
amplitude = data[:, 1]
error_vtpulse = data[:, 2]
error_amplitude = data[:, 3]

# Create the plot
plt.errorbar(vtpulse, amplitude, xerr=error_vtpulse, yerr=error_amplitude, fmt='o-')
plt.xlabel('dac_vtpulse', fontsize=12)
plt.ylabel('amplitude [dac_vthr]', fontsize=12)
plt.show()
