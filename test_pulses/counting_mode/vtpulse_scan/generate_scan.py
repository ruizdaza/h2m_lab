with open('vtpulse_scan.src', 'w') as file: #this overwrites, change for each dac
        file.write(f"add_device H2M \n")
        file.write(f"powerOn 0 \n")
        file.write(f"setVoltage v_pwell 0 0 \n")
        file.write(f"setVoltage v_sub 0 0 \n")
        file.write(f"configure 0 \n")
        file.write("delay 1000 \n")
        for dac_vtpulse in range(0, 264, 5):
                file.write(f"setRegister dac_vtpulse {dac_vtpulse} 0 \n")
                file.write(f"parameterScan dac_vthr 0 255 1 10 vtpulse_{dac_vtpulse} 0 \n") #change for each dac
                file.write("delay 100 \n")
        # for dac_ikrum in range(30, 50, 2):
        #       file.write(f"setRegister dac_ikrum {dac_ikrum} 0 \n")
        #       file.write(f"parameterScan dac_vthr 0 255 1 10 tpOff_ikrum_{dac_ikrum} 0 \n") #change for each dac
        #       file.write("delay 100 \n")
        # for dac_ikrum in range(50, 150, 5):
        #       file.write(f"setRegister dac_ikrum {dac_ikrum} 0 \n")
        #       file.write(f"parameterScan dac_vthr 0 255 1 10 tpOff_ikrum_{dac_ikrum} 0 \n") #change for each dac
        #       file.write("delay 100 \n")




