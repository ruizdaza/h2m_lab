## Test pulse structure

# Configuration files

- 'counting_100frames_1pulse.cfg': 1 pulse, 100 frames, '''parameterScan dac_vthr 0 255 1 100 output_file 0'''data was recorded with tp_polarity = 0 and 1, and shutter = 100 and 200 (short and exact shutter)

- 'test_shutter_tp.cfg': 10 pulses, 10 frames. '''parameterScan dac_vthr 0 255 1 10 output_file 0'''. data was recorded with tp_polarity = 0 and 1. Output files: '*100frames*'

Unless another thing is indicated: shutter window is 200 (2us), tp_on is 40 (1us) and tp_off is 40 (1us), dac_vtpulse is 40, tp_polarity is 1

# Ploting

- 'H2M_scanReader.C': generate the root files.

- 'plotHistograms': plots 2 curves. ''' root -l 'plotHistograms.C("exact_shutter.root", "short_shutter.root")' ''''

# Data
CernBox: in H2M_lab/shutter_test
