## TOA measurements with test pulse injection

# ToA measurement
100 pulses can be injected and recorded using:

```
pearycli -c config/test_pulse.cfg -r scripts/add_device_tp.src
```

The analysis of the output .txt file can be done with ```plotToA_firstPixels.py```.
