import os
import numpy as np
import matplotlib.pyplot as plt

directory = '/Users/atlaslap37/Desktop/h2m_lab/test_pulses/toa_mode/data'  # Change
start_col = 0
end_col = 2
start_row = 4
end_row = 15

def calculate_mean_and_rms_data(file_content):
    data_sum = np.zeros((end_row - start_row + 1, end_col - start_col + 1))
    data_sum_squares = np.zeros((end_row - start_row + 1, end_col - start_col + 1))
    frame_count = np.zeros((end_row - start_row + 1, end_col - start_col + 1), dtype=int)

    for line in file_content:
        if '-1  -1  -1  -1' in line:
            # Found the end of a frame
            frame_count += 1
        elif 'col' not in line:
            # Skip lines with headers
            col, row, data, mode = map(int, line.split())
            if start_col <= col <= end_col and start_row <= row <= end_row:
                data_sum[row - start_row, col - start_col] += data
                data_sum_squares[row - start_row, col - start_col] += data**2

    mean_data = data_sum / frame_count
    rms_data = np.sqrt(data_sum_squares / frame_count - mean_data**2)

    return mean_data, rms_data

hitmap_mean_data = np.zeros((end_row - start_row + 1, end_col - start_col + 1))
hitmap_rms_data = np.zeros((end_row - start_row + 1, end_col - start_col + 1))

filename = 'testpulseData.txt'  # Update with the correct filename
file_path = os.path.join(directory, filename)

if os.path.exists(file_path):
    print(f"File found: {file_path}.")
    with open(file_path, 'r') as file:
        file_content = file.readlines()
        hitmap_mean_data[:, :], hitmap_rms_data[:, :] = calculate_mean_and_rms_data(file_content)
else:
    print(f"File not found: {file_path}. Exiting...")

# Plot Mean ToA Value
plt.figure(figsize=(12, 5))
plt.subplot(1, 2, 1)
plt.imshow(hitmap_mean_data, extent=[start_col , end_col +1, start_row , end_row + 1], origin='lower', aspect='equal', cmap='viridis')  # Set aspect='equal'
colorbar_mean = plt.colorbar(label='Mean ToA Value [clock cycles]')
colorbar_mean.set_label('Mean ToA Value [clock cycles]', fontsize=16)
plt.xticks(np.arange(start_col, end_col + 1, 1), fontsize=12)
plt.yticks(np.arange(start_row, end_row + 1, 1), fontsize=12)
plt.xlabel('Column', fontsize=16)
plt.ylabel('Row', fontsize=16)
plt.title('Mean ToA Value')
plt.tight_layout()

# Add ToA values in the plot
for i in range(start_row, end_row + 1):
    for j in range(start_col, end_col + 1):
        mean_toa_value = hitmap_mean_data[i - start_row, j - start_col]
        #plt.text(j + 0.5, i + 0.5, f"{mean_toa_value:.1f}", color='red', ha='center', va='center', fontsize=8)

# Save Mean toA values to a text file
output_file_path = os.path.join(directory, 'correction_factors_toa.txt')
with open(output_file_path, 'w') as output_file:
    output_file.write("col\trow\ttoa\n")
    for i in range(start_row, end_row + 1):
        for j in range(start_col, end_col + 1):
            mean_toa_value = hitmap_mean_data[i - start_row, j - start_col]
            output_file.write(f"{j}\t{i}\t{mean_toa_value:.1f}\n")



# Plot RMS of toA Value
plt.subplot(1, 2, 2)
plt.imshow(hitmap_rms_data, extent=[start_col , end_col +1, start_row, end_row + 1], origin='lower', aspect='equal', cmap='viridis')  # Set aspect='equal'
colorbar_rms = plt.colorbar(label='RMS of toA Value [clock cycles]')
colorbar_rms.set_label('RMS of toA Value [clock cycles]', fontsize=16)
plt.xticks(np.arange(start_col, end_col + 1, 1), fontsize=12)
plt.yticks(np.arange(start_row, end_row + 1, 1), fontsize=12)
plt.xlabel('Column', fontsize=16)
plt.ylabel('Row', fontsize=16)
plt.title('RMS of toA Value')

# Add text annotations for mean toA values
for i in range(start_row, end_row + 1):
    for j in range(start_col, end_col + 1):
        rms_value = hitmap_rms_data[i - start_row, j - start_col]
        #plt.text(j + 0.5, i + 0.5, f"{rms_value:.1f}", color='red', ha='center', va='center', fontsize=8)


# Ensure square pixels and centered axes
plt.tight_layout()
plt.savefig('ToA_Mean_RMS_Plots_Limited_withValue.pdf')
plt.show()
