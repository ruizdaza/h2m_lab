with open('scan_vtpulse.txt', 'w') as file: #this overwrites, change for each dac
	file.write(f"add_device H2M \n")
	file.write(f"powerOn 0 \n")
	file.write(f"configure 0 \n")
	file.write("delay 100 \n")
	file.write(f"\n")


	for dac_value in range(0, 256, 5):   #change the steps
		file.write(f"setRegister dac_vtpulse {dac_value} 0 \n") #change for each dac
		file.write("delay 100 \n")
		file.write(f"testpulse 100 1 0 \n")
		file.write(f"\n")
