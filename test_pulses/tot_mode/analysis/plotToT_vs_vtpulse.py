import os
import numpy as np
import matplotlib.pyplot as plt

directory = '/Users/atlaslap37/Desktop/h2m_lab/test_pulses/tot_mode/data'  # Change
num_columns = 64
num_rows = 16
frames_per_pulse = 100

def calculate_mean_and_rms_data(chunk):
    data_sum = np.zeros((num_rows, num_columns))
    data_sum_squares = np.zeros((num_rows, num_columns))
    frame_count = np.zeros((num_rows, num_columns), dtype=int)

    for line in chunk:
        if '-1  -1  -1  -1' in line:
            # Found the end of a frame
            frame_count += 1
        elif 'col' not in line:
            # Skip lines with headers
            col, row, data, mode = map(int, line.split())
            data_sum[row, col] += data
            data_sum_squares[row, col] += data**2

    mean_data = data_sum / frame_count
    variance_data = data_sum_squares / frame_count - mean_data**2
    variance_data[variance_data < 0] = 0  # Replace negative values with 0
    rms_data = np.sqrt(variance_data)

    return mean_data, rms_data

def read_chunk(file, frames_per_pulse):
    chunk = []
    for _ in range(frames_per_pulse):
        frame_lines = []
        while True:
            line = file.readline()
            if not line or '-1  -1  -1  -1' in line:
                break
            frame_lines.append(line)
        if not line:
            break
        frame_lines.append(line)  # Include the line with '-1 -1 -1 -1' in the frame
        chunk.extend(frame_lines)
    return chunk

hitmap_mean_data_list = []
hitmap_rms_data_list = []
dac_vtpulse_values = list(range(0, 255, 5))

filename = 'testpulseData_scan_vtpulse.txt'  # Update with the correct filename
file_path = os.path.join(directory, filename)

if os.path.exists(file_path):
    print(f"File found: {file_path}.")
    with open(file_path, 'r') as file:
        for dac_vtpulse in dac_vtpulse_values:
            chunk = read_chunk(file, frames_per_pulse)
            if not chunk:
                break
            hitmap_mean_data, hitmap_rms_data = calculate_mean_and_rms_data(chunk)

            # Check for NaN values and handle them
            if np.isnan(np.sum(hitmap_mean_data)) or np.isnan(np.sum(hitmap_rms_data)):
                print(f"Skipping dac_vtpulse {dac_vtpulse} due to NaN values.")
                continue

            mean_val = np.nanmean(hitmap_mean_data)
            rms_val = np.nanmean(hitmap_rms_data)

            hitmap_mean_data_list.append(mean_val)
            hitmap_rms_data_list.append(rms_val)

            print(f"dac_vtpulse: {dac_vtpulse}, Mean ToT: {mean_val}, RMS ToT: {rms_val}")

else:
    print(f"File not found: {file_path}. Exiting...")

# Plot Mean ToT Value as a function of dac_vtpulse
plt.figure(figsize=(12, 5))
plt.subplot(1, 2, 1)
plt.plot(dac_vtpulse_values[:len(hitmap_mean_data_list)], hitmap_mean_data_list, marker='o')
plt.xlabel('dac_vtpulse', fontsize=12)
plt.ylabel('Mean ToT Value [clock cycles]', fontsize=12)
plt.title('Mean ToT Value vs dac_vtpulse')
plt.xlim(0, 240)  # Set the maximum x-axis value

# Plot RMS of ToT Value as a function of dac_vtpulse
plt.subplot(1, 2, 2)
plt.plot(dac_vtpulse_values[:len(hitmap_rms_data_list)], hitmap_rms_data_list, marker='o')
plt.xlabel('dac_vtpulse', fontsize=12)
plt.ylabel('RMS of ToT Value [clock cycles]', fontsize=12)
plt.title('RMS of ToT Value vs dac_vtpulse')
#plt.xlim(0, 240)  # Set the maximum x-axis value
#plt.ylim(0, 10)  # Set the maximum x-axis value

plt.tight_layout()
plt.savefig('ToT_Mean_RMS_vs_dac_vtpulse.pdf')
plt.show()
