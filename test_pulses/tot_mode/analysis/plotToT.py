import os
import numpy as np
import matplotlib.pyplot as plt

directory = '/Users/atlaslap37/Desktop/h2m_lab/test_pulses/tot_mode/data'  # Change
num_columns = 64
num_rows = 16

def calculate_mean_and_rms_data(file_content):
    data_sum = np.zeros((num_rows, num_columns))
    data_sum_squares = np.zeros((num_rows, num_columns))
    frame_count = np.zeros((num_rows, num_columns), dtype=int)

    for line in file_content:
        if '-1  -1  -1  -1' in line:
            # Found the end of a frame
            frame_count += 1
        elif 'col' not in line:
            # Skip lines with headers
            col, row, data, mode = map(int, line.split())
            data_sum[row, col] += data
            data_sum_squares[row, col] += data**2

    mean_data = data_sum / frame_count
    rms_data = np.sqrt(data_sum_squares / frame_count - mean_data**2)

    return mean_data, rms_data

hitmap_mean_data = np.zeros((num_rows, num_columns))
hitmap_rms_data = np.zeros((num_rows, num_columns))

filename = 'testpulseData.txt'  # Update with the correct filename
file_path = os.path.join(directory, filename)

if os.path.exists(file_path):
    print(f"File found: {file_path}.")
    with open(file_path, 'r') as file:
        file_content = file.readlines()
        hitmap_mean_data[:, :], hitmap_rms_data[:, :] = calculate_mean_and_rms_data(file_content)
else:
    print(f"File not found: {file_path}. Exiting...")

# Plot Mean ToT Value
plt.figure(figsize=(12, 5))
plt.subplot(1, 2, 1)
plt.imshow(hitmap_mean_data, extent=[0, num_columns, 0, num_rows], origin='lower', aspect='auto', cmap='viridis')
colorbar_mean = plt.colorbar(label='Mean ToT Value [clock cycles]')
colorbar_mean.set_label('Mean ToT Value [clock cycles]', fontsize=12)
plt.xlabel('Column', fontsize=12)
plt.ylabel('Row', fontsize=12)
plt.gca().set_aspect('equal')
plt.tight_layout()
plt.title('Mean ToT Value')

# Plot RMS of ToT Value
plt.subplot(1, 2, 2)
plt.imshow(hitmap_rms_data, extent=[0, num_columns, 0, num_rows], origin='lower', aspect='auto', cmap='viridis')
colorbar_rms = plt.colorbar(label='RMS of ToT Value [clock cycles]')
colorbar_rms.set_label('RMS of ToT Value [clock cycles]', fontsize=12)
plt.xlabel('Column', fontsize=12)
plt.ylabel('Row', fontsize=12)
plt.title('RMS of ToT Value')

# We want square pixels
plt.gca().set_aspect('equal')
plt.tight_layout()
plt.savefig('ToT_Mean_RMS_Plots.pdf')
plt.show()
