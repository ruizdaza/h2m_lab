import os
import numpy as np
import matplotlib.pyplot as plt

directory = '/Users/atlaslap37/Desktop/h2m_lab/test_pulses/tot_mode/data'  # Change
start_col = 0
end_col = 3
start_row = 4
end_row = 15

def calculate_mean_and_rms_data(file_content):
    data_sum = np.zeros((end_row - start_row + 1, end_col - start_col + 1))
    data_sum_squares = np.zeros((end_row - start_row + 1, end_col - start_col + 1))
    frame_count = np.zeros((end_row - start_row + 1, end_col - start_col + 1), dtype=int)

    for line in file_content:
        if '-1  -1  -1  -1' in line:
            # Found the end of a frame
            frame_count += 1
        elif 'col' not in line:
            # Skip lines with headers
            col, row, data, mode = map(int, line.split())
            if start_col <= col <= end_col and start_row <= row <= end_row:
                data_sum[row - start_row, col - start_col] += data
                data_sum_squares[row - start_row, col - start_col] += data**2

    mean_data = data_sum / frame_count
    rms_data = np.sqrt(data_sum_squares / frame_count - mean_data**2)

    return mean_data, rms_data

hitmap_mean_data = np.zeros((end_row - start_row + 1, end_col - start_col + 1))
hitmap_rms_data = np.zeros((end_row - start_row + 1, end_col - start_col + 1))

filename = 'testpulseData.txt'  # Update with the correct filename
file_path = os.path.join(directory, filename)

if os.path.exists(file_path):
    print(f"File found: {file_path}.")
    with open(file_path, 'r') as file:
        file_content = file.readlines()
        hitmap_mean_data[:, :], hitmap_rms_data[:, :] = calculate_mean_and_rms_data(file_content)
else:
    print(f"File not found: {file_path}. Exiting...")

# Plot Mean ToT Value
plt.figure(figsize=(12, 5))
plt.subplot(1, 2, 1)
plt.imshow(hitmap_mean_data, extent=[start_col, end_col, start_row, end_row], origin='lower', aspect='auto', cmap='viridis')
colorbar_mean = plt.colorbar(label='Mean ToT Value [clock cycles]')
colorbar_mean.set_label('Mean ToT Value [clock cycles]', fontsize=12)
plt.xlabel('Column', fontsize=12)
plt.ylabel('Row', fontsize=12)
plt.title('Mean ToT Value')
plt.gca().set_aspect('equal')
plt.tight_layout()

# Plot RMS of ToT Value
plt.subplot(1, 2, 2)
plt.imshow(hitmap_rms_data, extent=[start_col, end_col, start_row, end_row], origin='lower', aspect='auto', cmap='viridis')
colorbar_rms = plt.colorbar(label='RMS of ToT Value [clock cycles]')
colorbar_rms.set_label('RMS of ToT Value [clock cycles]', fontsize=12)
plt.xlabel('Column', fontsize=12)
plt.ylabel('Row', fontsize=12)
plt.title('RMS of ToT Value')

# We want square pixels
plt.gca().set_aspect('equal')
plt.tight_layout()
plt.savefig('ToT_Mean_RMS_Plots_Limited.pdf')
plt.show()
