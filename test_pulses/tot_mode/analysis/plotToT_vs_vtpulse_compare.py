import os
import numpy as np
import matplotlib.pyplot as plt

directory_1V2 = '/Users/atlaslap37/Desktop/h2m_lab/test_pulses/tot_mode/data/h2m3/1V2_ikrum19'  # Change
directory_2V4 = '/Users/atlaslap37/Desktop/h2m_lab/test_pulses/tot_mode/data/h2m3/1V2_ikrum21'  # Change
directory_3V6 = '/Users/atlaslap37/Desktop/h2m_lab/test_pulses/tot_mode/data/h2m3/1V2_ikrum32'  # Change
directory_0V = '/Users/atlaslap37/Desktop/h2m_lab/test_pulses/tot_mode/data/h2m3/1V2_ikrum38'  # Change


num_columns = 64
num_rows = 16
frames_per_pulse = 100

def calculate_mean_and_rms_data(chunk):
    data_sum = np.zeros((num_rows, num_columns))
    data_sum_squares = np.zeros((num_rows, num_columns))
    frame_count = np.zeros((num_rows, num_columns), dtype=int)

    for line in chunk:
        if '-1  -1  -1  -1' in line:
            # Found the end of a frame
            frame_count += 1
        elif 'col' not in line:
            # Skip lines with headers
            col, row, data, mode = map(int, line.split())
            data_sum[row, col] += data
            data_sum_squares[row, col] += data**2

    mean_data = data_sum / frame_count
    variance_data = data_sum_squares / frame_count - mean_data**2
    variance_data[variance_data < 0] = 0  # Replace negative values with 0
    rms_data = np.sqrt(variance_data)

    return mean_data, rms_data

def read_chunk(file, frames_per_pulse):
    chunk = []
    for _ in range(frames_per_pulse):
        frame_lines = []
        while True:
            line = file.readline()
            if not line or '-1  -1  -1  -1' in line:
                break
            frame_lines.append(line)
        if not line:
            break
        frame_lines.append(line)  # Include the line with '-1 -1 -1 -1' in the frame
        chunk.extend(frame_lines)
    return chunk

hitmap_mean_data_list_newsettings = []
hitmap_rms_data_list_newsettings = []
mean_errors_list_newsettings = []  # List to store standard deviation as errors
dac_vtpulse_values = list(range(0, 255, 5))

# Filename for new settings
filename_newsettings = 'testpulseData_scan_vtpulse.txt'
file_path_newsettings = os.path.join(directory_1V2, filename_newsettings)

if os.path.exists(file_path_newsettings):
    print(f"File found: {file_path_newsettings}.")
    with open(file_path_newsettings, 'r') as file:
        for dac_vtpulse in dac_vtpulse_values:
            chunk = read_chunk(file, frames_per_pulse)
            if not chunk:
                break
            hitmap_mean_data, hitmap_rms_data = calculate_mean_and_rms_data(chunk)

            # Check for NaN values and handle them
            if np.isnan(np.sum(hitmap_mean_data)) or np.isnan(np.sum(hitmap_rms_data)):
                print(f"Skipping dac_vtpulse {dac_vtpulse} due to NaN values.")
                continue

            mean_val = np.nanmean(hitmap_mean_data)
            rms_val = np.nanmean(hitmap_rms_data)

            hitmap_mean_data_list_newsettings.append(mean_val)
            hitmap_rms_data_list_newsettings.append(rms_val)

            # Calculate standard deviation of mean values as error
            mean_errors_list_newsettings.append(np.nanstd(hitmap_mean_data))

            print(f"dac_vtpulse: {dac_vtpulse}, Mean ToT: {mean_val}, RMS ToT: {rms_val}")

else:
    print(f"File not found: {file_path_newsettings}. Exiting...")

# Filename for old settings
filename_2V4 = 'testpulseData_scan_vtpulse.txt'
file_path_2V4 = os.path.join(directory_2V4, filename_2V4)

hitmap_mean_data_list_2V4 = []
hitmap_rms_data_list_2V4 = []
mean_errors_list_2V4 = []  # List to store standard deviation as errors

if os.path.exists(file_path_2V4):
    print(f"File found: {file_path_2V4}.")
    with open(file_path_2V4, 'r') as file:

        for dac_vtpulse in dac_vtpulse_values:
            chunk = read_chunk(file, frames_per_pulse)
            if not chunk:
                break
            hitmap_mean_data, hitmap_rms_data = calculate_mean_and_rms_data(chunk)

            # Check for NaN values and handle them
            if np.isnan(np.sum(hitmap_mean_data)) or np.isnan(np.sum(hitmap_rms_data)):
                print(f"Skipping dac_vtpulse {dac_vtpulse} due to NaN values.")
                continue

            mean_val = np.nanmean(hitmap_mean_data)
            rms_val = np.nanmean(hitmap_rms_data)

            hitmap_mean_data_list_2V4.append(mean_val)
            hitmap_rms_data_list_2V4.append(rms_val)

            # Calculate standard deviation of mean values as error
            mean_errors_list_2V4.append(np.nanstd(hitmap_mean_data))

            print(f"dac_vtpulse: {dac_vtpulse}, Mean ToT: {mean_val}, RMS ToT: {rms_val}")

else:
    print(f"File not found: {file_path_2V4}. Exiting...")

# Filename for old settings
filename_3V6 = 'testpulseData_scan_vtpulse.txt'
file_path_3V6 = os.path.join(directory_3V6, filename_3V6)

hitmap_mean_data_list_3V6 = []
hitmap_rms_data_list_3V6 = []
mean_errors_list_3V6 = []  # List to store standard deviation as errors

if os.path.exists(file_path_3V6):
    print(f"File found: {file_path_3V6}.")
    with open(file_path_3V6, 'r') as file:


        for dac_vtpulse in dac_vtpulse_values:
            chunk = read_chunk(file, frames_per_pulse)
            if not chunk:
                break
            hitmap_mean_data, hitmap_rms_data = calculate_mean_and_rms_data(chunk)

            # Check for NaN values and handle them
            if np.isnan(np.sum(hitmap_mean_data)) or np.isnan(np.sum(hitmap_rms_data)):
                print(f"Skipping dac_vtpulse {dac_vtpulse} due to NaN values.")
                continue

            mean_val = np.nanmean(hitmap_mean_data)
            rms_val = np.nanmean(hitmap_rms_data)

            hitmap_mean_data_list_3V6.append(mean_val)
            hitmap_rms_data_list_3V6.append(rms_val)

            # Calculate standard deviation of mean values as error
            mean_errors_list_3V6.append(np.nanstd(hitmap_mean_data))

            print(f"dac_vtpulse: {dac_vtpulse}, Mean ToT: {mean_val}, RMS ToT: {rms_val}")

else:
    print(f"File not found: {file_path_3V6}. Exiting...")



# Filename for old settings
filename_0V = 'testpulseData_scan_vtpulse.txt'
file_path_0V = os.path.join(directory_0V, filename_0V)

hitmap_mean_data_list_0V = []
hitmap_rms_data_list_0V = []
mean_errors_list_0V = []  # List to store standard deviation as errors


if os.path.exists(file_path_0V):
    print(f"File found: {file_path_0V}.")
    with open(file_path_0V, 'r') as file:

        for dac_vtpulse in dac_vtpulse_values:
            chunk = read_chunk(file, frames_per_pulse)
            if not chunk:
                break
            hitmap_mean_data, hitmap_rms_data = calculate_mean_and_rms_data(chunk)

            # Check for NaN values and handle them
            if np.isnan(np.sum(hitmap_mean_data)) or np.isnan(np.sum(hitmap_rms_data)):
                print(f"Skipping dac_vtpulse {dac_vtpulse} due to NaN values.")
                continue

            mean_val = np.nanmean(hitmap_mean_data)
            rms_val = np.nanmean(hitmap_rms_data)

            hitmap_mean_data_list_0V.append(mean_val)
            hitmap_rms_data_list_0V.append(rms_val)

            # Calculate standard deviation of mean values as error
            mean_errors_list_0V.append(np.nanstd(hitmap_mean_data))

            print(f"dac_vtpulse: {dac_vtpulse}, Mean ToT: {mean_val}, RMS ToT: {rms_val}")

else:
    print(f"File not found: {file_path_0V}. Exiting...")



# Plot Mean ToT Value as a function of dac_vtpulse with error bars for new settings
plt.errorbar(dac_vtpulse_values[:len(hitmap_mean_data_list_0V)], hitmap_mean_data_list_0V, yerr=hitmap_rms_data_list_0V, marker='^', linestyle='None', capsize=5, label='ikrum = 38', color='green')
plt.errorbar(dac_vtpulse_values[:len(hitmap_mean_data_list_3V6)], hitmap_mean_data_list_3V6, yerr=hitmap_rms_data_list_3V6, marker='s', linestyle='None', capsize=5, label='ikrum = 32', color='blue')
plt.errorbar(dac_vtpulse_values[:len(hitmap_mean_data_list_2V4)], hitmap_mean_data_list_2V4, yerr=hitmap_rms_data_list_2V4, marker='o', linestyle='None', capsize=5, label='ikrum = 21', color='red')
plt.errorbar(dac_vtpulse_values[:len(hitmap_mean_data_list_newsettings)], hitmap_mean_data_list_newsettings, yerr=hitmap_rms_data_list_newsettings, marker='v', linestyle='None', capsize=5, label='ikrum = 19', color='black')
plt.xlabel('dac_vtpulse', fontsize=16)
plt.ylabel('Mean ToT Value [clock cycles]', fontsize=16)
plt.xlim(0, 240)  # Set the maximum x-axis value
plt.legend(fontsize=16, frameon=False)




plt.show()
