## TOT measurements with test pulse injection

# ToT measurement
100 pulses can be injected and recorded using:

```
pearycli -c config/test_pulse.cfg -r scripts/add_device_tp.src
```

The analysis of the output .txt file can be done with ```plotToT.py``` or with ```plotToT_firstPixels.py```


# Test pulse amplitude
The test pulse amplitude is controlled with a ```dac_vtpulse```.

ToT vs. test pulse amplitude can be done running:
```
pearycli -c config/test_pulse.cfg -r scripts/scan_vtpulse.txt
```
The scan can be greate with ```scripts/generate_scan.py```


The ToT vs. test pulse amplitude can be obtained with: ```plotToT_vs_vtpulse.py```.
