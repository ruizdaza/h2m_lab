with open('thr_scan.txt', 'w') as file: #this overwrites, change for each dac
	file.write(f"add_device H2M \n")
	file.write(f"powerOn 0 \n")
	file.write(f"setVoltage v_pwell 0 0 \n")
	file.write(f"setVoltage v_sub 0 0 \n")
	file.write(f"configure 0 \n")
	file.write("delay 100 \n")
	file.write(f"\n")


	for dac_value in range(0, 256, 10):   #change the steps

		file.write(f"setRegister conf 1 0 \n")
		file.write(f"setRegister tp_polarity 1 0 \n")
		file.write(f"setRegister link_shutter_tp 1 0 \n")
		file.write(f"setRegister shutter_tp_lat 0x8 0 \n")
		file.write(f"setRegister tp_num 4 0 \n")

		file.write(f"setRegister tp_on 0x5 0 \n")
		file.write(f"setRegister tp_off 0x5 0 \n")

		file.write(f"setRegister dac_vtpulse 0xFF 0 \n")

		file.write(f"setRegister dac_vthr {dac_value} 0 \n") #change for each dac

		file.write(f"setRegister acq_mode 2 0 \n")

		file.write(f"setRegister px_cnt_11 0xBDBDBDBD 0 \n")
		file.write(f"setRegister px_cnt_11 0xBDBDBDBD 0 \n")
		file.write(f"setRegister px_cnt_11 0xBDBDBDBD 0 \n")
		file.write(f"setRegister px_cnt_11 0xBDBDBDBD 0 \n")

		file.write(f"setRegister conf 0 0 \n")

		file.write(f"getRegister px_cnt_11 0 \n")
		file.write(f"getRegister px_cnt_11 0 \n")
		file.write(f"getRegister px_cnt_11 0 \n")
		file.write(f"getRegister px_cnt_11 0 \n")

		file.write(f"setRegister acq_start 1 0 \n")
		file.write(f"setRegister matrix_ctrl 1 0 \n")
		file.write(f"setMemory shutter 150 0 \n")
		file.write(f"setRegister acq_start 0 0 \n")

		file.write(f"getRegister px_cnt_11 0 \n")
		file.write(f"getRegister px_cnt_11 0 \n")
		file.write(f"getRegister px_cnt_11 0 \n")
		file.write(f"getRegister px_cnt_11 0 \n")
		file.write(f"\n")
