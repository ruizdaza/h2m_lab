# Test pulse injection

## Configurable registers
Some parameters taht can be found in ```tet_injection_testing``` are:

```tp_polarity 1```: test pulse with positive polarity

```link_shutter_tp 1```: this is waiting until we open the shutter to start the pulse injection.

```shutter_tp_lat N```: we wait N clock cycles after the shutter is open. In the slow control clock (40 MHz = 25ns/clock cycle)

```tp_num N```: N test pulses are injected

```tp_on N```: For N clock cycles the test pulse is ON (in the 40 MHz clock)

```tp_off N```: For N clock cycles the test pulse is OFF (in the 40 MHz clock)

```dac_vtpulse N```: amplitude of the test pulse. It has been check that it has a linear response

```matrix_ctrl 1```: the test pulse can be initialized

```shutter N```: for N clock cycles the shutter is open (in the 100 MHz clock)


## Study the threshold effect

```generate_scan```: generates a threshold scan, which output file ```thr_scan.txt```.
It can be run using: ```pearycli -r thr_scan.txt```.

So far, the analysis is been done manually.
