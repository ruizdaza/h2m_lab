import uproot
import numpy as np
import matplotlib.pyplot as plt

# Function to extract mean data from a given file path
def extract_mean_data(file_path):
    with uproot.open(file_path) as file:
        hist2d = file["all_pixels_2D_response"]
        mean_graph = file["mean_pixel_response"]

        # Extract data for the mean plot
        mean_x = mean_graph.values()[0]  # x-axis data
        mean_y = mean_graph.values()[1]  # y-axis data

    return mean_x, mean_y

# Define the paths to the result.root files in each directory
path_thr110 = "data_thr110/result.root"
path_thr115 = "data_thr115/result.root"
path_thr120 = "data_thr120/result.root"

# Extract data for each threshold
mean_x_110, mean_y_110 = extract_mean_data(path_thr110)
mean_x_115, mean_y_115 = extract_mean_data(path_thr115)
mean_x_120, mean_y_120 = extract_mean_data(path_thr120)

# Plot all mean plots together
plt.figure(figsize=(8, 6))

plt.plot(mean_x_110, mean_y_110, 'r-', marker='o', markersize=0.2, label='THL =  110 DAC')
plt.plot(mean_x_115, mean_y_115, 'g-', marker='o', markersize=0.2, label='THL =  115 DAC')
plt.plot(mean_x_120, mean_y_120, 'b-', marker='o', markersize=0.2, label='THL = 120 DAC')

plt.xlabel('dac_vtpulse [DAC]', fontsize = 16)
plt.ylabel('ToT [clock cycles]', fontsize = 16)
plt.xlim(0, 260)
plt.ylim(0, 110)
plt.legend()

plt.show()
