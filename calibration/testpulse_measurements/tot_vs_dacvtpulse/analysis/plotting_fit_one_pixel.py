import ROOT
import matplotlib.pyplot as plt
import numpy as np

# Open the ROOT file
file = ROOT.TFile.Open("result_fit_cal.root")

# Access the graph from the ROOT file
graph = file.Get("single_pixel_plot/h_pixel_response_2_14")

# Create arrays to hold the data points (ToT, amplitude)
x_data = []
y_data = []
y_errors = []

# Loop through the points in the ROOT graph and store them in arrays
for i in range(graph.GetN()):
    x = graph.GetX()[i]
    y = graph.GetY()[i]
    y_err = abs(graph.GetErrorY(i))  # Ensure y-error is non-negative
    x_data.append(float(x))
    y_data.append(float(y))
    y_errors.append(float(y_err))

# Access the fit
fit_function = graph.GetFunction("fitFunc")  # Replace 'fitFunc' with the actual fit function name

# Get the fit range from the fit function
fit_x_min = fit_function.GetXmin()
fit_x_max = fit_function.GetXmax()

# Extract the fit parameters and their errors
fit_parameters = [fit_function.GetParameter(i) for i in range(fit_function.GetNpar())]
fit_errors = [fit_function.GetParError(i) for i in range(fit_function.GetNpar())]

# Get the chi-square and ndof
chi_square = fit_function.GetChisquare()
ndof = fit_function.GetNDF()

# Create the fit line within the correct range
x_fit = np.linspace(fit_x_min, fit_x_max, 500)
y_fit = [fit_function.Eval(x) for x in x_fit]

# Plot graph
plt.figure(figsize=(8, 6))
plt.errorbar(x_data, y_data, yerr=0, fmt='o', color="black")

# Plot the fit function
plt.plot(x_fit, y_fit, label="Surrogate function fit", color="orange", linestyle='--')

# Define parameter labels
param_labels = ['A', 'B', 'C', 'D']

formatted_params = [
    f"A = {fit_parameters[0]:.4f} ± {fit_errors[0]:.4f}",
    f"B = {fit_parameters[1]:.1f} ± {fit_errors[1]:.1f}",
    f"C = {fit_parameters[2]:.0f} ± {fit_errors[2]:.0f}",
    f"D = {fit_parameters[3]:.0f} ± {fit_errors[3]:.0f}"
]

param_text = "\n".join(formatted_params)

# Annotate fit parameters
fit_info_text = f"$\\chi^2$/ndof: {chi_square:.0f} / {ndof} "
plt.text(0.05, 0.85, param_text + "\n" + fit_info_text, transform=plt.gca().transAxes,
         fontsize=12, verticalalignment='top', color='orange')

# Set plot labels and title
plt.xlabel("Amplitude [$e^-$]", fontsize=16)
plt.ylabel("ToT [clock cycles]", fontsize=16)

plt.legend()
plt.show()
file.Close()
