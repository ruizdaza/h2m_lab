## ToT vs dac_vtpulse
Record ToT value for each dac_vtpulse.

# Taking data
In Caribou:

```
python3 run_peary.py
```
This produces the .bin files. It takes 100 frames for each dac_vtpulse. In ```run_peary.py``` you can modify the range and steps of the scan in dac_vtpulse.

# Analysis
Copy the .bin files into your lab PC in the folder ```data```. Convert all the .bin files into root files:

```
python3 analysis/run_analysis.py
```

This runs the ```analysis/H2M_scanReader.C``` for all the .bin files.


The root files are combined with:
```
root -l analysis/plotting.C
```
In python this can be plotted nicer using ```python3 analysis/plotting.py```

If you want to include the surrogate function for each pixel:
```
root -l analysis/plotting_cal_fits.C
```
which includes the surrogate function fits for each pixel in electrons. You need to modify ```conversionFactor``` from the value obtained in the linear fit obtained in the amplitude (electrons) vs dac_vtpulse (DAC). The parameter fits for each pixel are saved in the fit_parameters.txt file. Histograms of the parameters fits are also included. In python this can be plotted nicer using ```python3 analysis/plotting._cal.py```, also you need to modify the ```conversionFactor```. Individual pixel plots can be obtained using ```python3 analysis/plotting_fit_one_pixel.py```, where you need to select the desired pixel to plot. 
