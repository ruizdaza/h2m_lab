import uproot
import numpy as np
import matplotlib.pyplot as plt

# Open the ROOT file
file_name = "result.root"
with uproot.open(file_name) as file:
    # Get the 2D histogram
    hist2d = file["all_pixels_amp_vs_tot_2D"]

    # Extract data from the 2D histogram
    hist_values = hist2d.values()
    x_edges = hist2d.axis(0).edges()
    y_edges = hist2d.axis(1).edges()

    # Mask the data to limit z-values to a maximum of 500 and remove zero values
    masked_values = np.clip(hist_values, 0, 500)
    masked_values[masked_values == 0] = np.nan  # Set zero values to NaN for transparency

    # Calculate the mean ToT for each Amplitude bin
    mean_tot = []
    amplitude_centers = []

    for i in range(len(x_edges) - 1):
        bin_values = hist_values[i, :]
        if np.sum(bin_values) > 0:  # Avoid division by zero
            mean_y = np.average(y_edges[:-1], weights=bin_values)
            mean_tot.append(mean_y)
            amplitude_centers.append((x_edges[i] + x_edges[i + 1]) / 2)

    # Plot the 2D histogram
    plt.figure(figsize=(8, 6))
    plt.pcolormesh(x_edges, y_edges, masked_values.T, shading='auto', cmap='viridis', vmin=0)
    cbar = plt.colorbar()
    cbar.set_label("pixels", fontsize=16)

    # Overlay the calculated mean
    plt.plot(amplitude_centers, mean_tot, 'r-', label='Mean ToT')

    # Label the axes
    plt.xlabel("Amplitude [THL DAC]", fontsize=16)
    plt.ylabel("ToT [clock cycles]", fontsize=16)

    # Set the x and y axis limits
    plt.xlim(99, 260)
    plt.ylim(0, 110)

    # Set the legend
    plt.legend(loc='lower right')

    # Define baseline and factor
    baseline = 99
    factor = 34

    # Function to convert THL DAC to electrons
    def thl_dac_to_electron(x):
        return (x - baseline) * factor

    # Function to convert electrons to THL DAC (for inverse transformation)
    def electron_to_thl_dac(x):
        return x / factor + baseline

    # Create a secondary x-axis on top
    secax = plt.gca().secondary_xaxis('top', functions=(thl_dac_to_electron, electron_to_thl_dac))
    secax.set_xlabel('Amplitude [e-]', fontsize=16)

    # Set the limits for the secondary x-axis
    secax.set_xlim(thl_dac_to_electron(50), thl_dac_to_electron(260))

    # Show the plot
    plt.show()
