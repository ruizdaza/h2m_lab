Using analog test pulse injection we can obtain the following relations to calibrate the ToT:


1) Amplitude [THL DAC or electrons] vs dac_vtpulse [DAC]


2) ToT [clock cycles or ns] vs dac_vtpulse [DAC]


3) ToT [clock cycles or ns] vs amplitude [THL DAC or electrons]


In each of the folders there is documentation. 
