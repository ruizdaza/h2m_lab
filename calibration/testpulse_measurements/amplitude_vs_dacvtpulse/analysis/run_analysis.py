#!/usr/bin/env python

from os import listdir, chdir
from os.path import join, abspath, dirname
from subprocess import run
from shutil import move

def main(args=None):
    # settings
    input_dir = "data"
    script_directory = dirname(abspath(__file__))
    script = join(script_directory, "", "H2M_scanReader_erf.C")

    # run analysis for individual files
    for f in listdir(input_dir):
        input_file = join(input_dir, f)
        # only consider the '.bin' files
        if not input_file.endswith('.bin'): continue

        command = ["root", "-l", "-x", "-b", f"{script}(\"{input_file}\",50)"]
        run(command)

if __name__ == "__main__":
    main()

