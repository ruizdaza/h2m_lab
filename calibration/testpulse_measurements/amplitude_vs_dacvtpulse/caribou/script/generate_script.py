with open('take_data.txt', 'w') as file: #this overwrites, change for each dac
	file.write(f"add_device H2M \n")
	file.write(f"powerOn 0 \n")
	file.write(f"configure 0 \n")
	file.write("delay 1000 \n")

	for dac_vtpulse in range(0, 254, 1):
		file.write(f"setRegister dac_vtpulse {dac_vtpulse} 0 \n")
		file.write("delay 1000 \n")
		file.write(f"parameterScan dac_vthr 0 255 1 100 dacvtpulse_{dac_vtpulse} 0 \n") #change for each dac
		file.write("delay 100 \n")
	
	file.write("exit \n")
