## Amplitude vs dac_vtpulse

We can get the amplitude of a test pulse using the counting mode and performing a threshold scan. When we stop collecting counts, it means that we have reached the amplitude of the injected test pulse. Therefore we can obtain the amplitude [THL DAC or electrons] vs dac_vtpulse [DAC]. Note, we are assuming that the relation between THL DAC and electrons has been obtained from the threshold calibration.

# Taking data
In Caribou:

Create the ```caribou/script/take_data.txt``` file with your desired ranges using ```python3 caribou/script/generate_data.py```.

Then:
```
pearycli -c config/config.cfg -r script/take_data.txt
```
This produces the .bin files.

# Analysis
Copy the .bin files into your lab PC in the folder ```data```. Convert all the .bin files into root files:

```
python3 analysis/run_analysis.py
```
This runs the ```analysis/H2M_scanReader_erf.C``` for all the .bin files. It performs an s-curve fit to find the amplitude of the test pulse for each of the files.


The root files are combined with:
```
root -l analysis/plotting.C
```
This reads all the root files and makes final plots and saved them in ```result.root```.

# Extra plotting

If you don't like root style, you can get nicer plots using
```
python3 plotting.py
```
This just reads the ```result.root``` and makes the final plot suing the python style. It also includes the second x-axis in electrons. So please, modify at the beginning of the script the ```baseline``` and ```calibration_factor``` with the obtained values after trimming and threshold calibration. Finally, it gives the calibration factor between electrons and dac_vtpulse from a linear fit in the interesting region. 
