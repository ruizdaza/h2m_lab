After getting the data in a ```.bin``` file, copy to the lab PC and analyse it:

```
root -l -x 'H2M_scanReader_short.C("data/occupancyData.bin",50)'
```

Or for more plots, but taking longer the analysis:

```
root -l -x 'H2M_scanReader.C("data/occupancyData.bin",50)'
```

For plottting the ToT spectrum:
```
python3 plottting.py
```

For applying the calibration:
```
root -l -x 'H2M_scanReader_cal.C("data/occupancyData_h2m2_1V2_ikrum21.bin",50)'
```

And plotting the amplitude spectrum:
```
python3 plotting_cal.py
```
