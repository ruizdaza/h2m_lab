void H2M_scanReader_short(string filename = "", int plotAt = 255) {

  size_t strip_from = filename.find_last_of(".");
  std::string rootFileName = Form("%s_short.root", filename.substr(0, strip_from).c_str());

  // H2M chip parameters
  const int n_col = 64;
  const int n_row = 16;
  const int n_dac = 256;
  const int n_tdc = 256;

  TH2D* h_curve_2D =
    new TH2D("curve_2D", ";Comparator Threshold [DAC];Pixel Counts;entries", n_dac, 0 - 0.5, n_dac - 0.5, n_dac, 0 - 0.5, n_dac - 0.5);
  TH1D* h_threshold = new TH1D("Pixel Count Sum", ";Comparator Threshold [DAC];Pixel Count Sum;entries", n_dac, 0 - 0.5, n_dac - 0.5);
  TH2D* occupancy = new TH2D("occupancy", "Occupancy; Column; Row", n_col, 0 - 0.5, n_col - 0.5, n_row, 0 - 0.5, n_row - 0.5);
  TH2D* occupancy_sum =
    new TH2D("occupancy_sum", "Occupancy Sum;Column; Row", n_col, 0 - 0.5, n_col - 0.5, n_row, 0 - 0.5, n_row - 0.5);
  TH2D* failed_fits =
      new TH2D("failed fits", "Failed Fits;Column; Row", n_col, 0 - 0.5, n_col - 0.5, n_row, 0 - 0.5, n_row - 0.5);
  TH1D* h_response_sum = new TH1D("response_sum", ";ToT [clock cycle];entries", n_tdc, 0 - 0.5, n_tdc - 0.5);

  // Commenting out the creation of individual pixel response histograms to avoid writing them later
  // TH1D* h_pixel_responses[n_col][n_row];
  // for(int col = 0; col < n_col; col++) {
  //   for(int row = 0; row < n_row; row++) {
  //     h_pixel_responses[col][row] =
  //       new TH1D(Form("h_pixel_response_%i_%i", col, row), ";response [tdc];entries", n_tdc, 0 - 0.5, n_tdc - 0.5);
  //   }
  // }

  // fill arrays in loops
  int i = 0;
  double occ_map_pulse[n_col][n_row][n_dac] = {0};
  std::vector<double> ampl;
  ampl.resize(n_dac, 0);


  // open data file
  std::ifstream data_in;
  data_in.open(filename.c_str(), std::ios::ate | std::ios::binary);

  // output file to save histogram
  TFile* fout = new TFile(rootFileName.c_str(), "RECREATE");
  TDirectory* dir = fout->GetDirectory("");
  // Commenting out the creation of the single_pixel_plots directory
  // TDirectory* dir_single = dir->mkdir("single_pixel_plots");
  TTree* T = new TTree("trim_data", "pixel thresholds for trimming");
  T->SetAutoSave(0);
  double tcol, trow, tmean, tsigma, terr;
  T->Branch("col", &tcol);
  T->Branch("row", &trow);
  T->Branch("mean", &tmean);
  T->Branch("sigma", &tsigma);
  T->Branch("error", &terr); // uncertainty for mean value

  std::cout << filename.c_str() << std::endl;

  if(data_in) {

    std::cout << filename.c_str() << std::endl;

    // get length of file:
    data_in.seekg(0, data_in.end);
    auto length = data_in.tellg();
    data_in.seekg(0, data_in.beg);
    std::cout << "Reading " << length << " bytes " << std::endl;

    // buffer for one block of testpulses
    std::streamsize blocksize = n_row * n_col + 6;
    uint16_t block[blocksize];

    // containers for the occupancy map and amplitude
    uint16_t occupancy_map[n_col][n_row] = {0};
    uint16_t amplitude = 0;
    uint16_t scan_dac_i = 0;
    uint16_t scan_start = 0;
    uint16_t scan_step = 0;
    uint16_t scan_end = 0;
    uint16_t scan_n = 0;

    // reading data from binary file
    int counts;
    while(data_in.read(reinterpret_cast<char*>(&block), 2 * blocksize)) {

      counts = 0;

      for(size_t col = 0; col < n_col; col++) {
        for(size_t row = 0; row < n_row; row++) {
          occupancy_map[col][row] = block[col + row * n_col];
        }
      }
      amplitude = block[n_col * n_row + 0];
      scan_start = block[n_col * n_row + 1];
      scan_step = block[n_col * n_row + 2];
      scan_end = block[n_col * n_row + 3];
      scan_n = block[n_col * n_row + 4];
      scan_dac_i = block[n_col * n_row + 5];
      if(amplitude == scan_start)
        continue;
      for(int col = 0; col < n_col; col++) {
        for(int row = 0; row < n_row; row++) {

          // fill containers and histograms for data analysis and plotting
          h_curve_2D->Fill(amplitude, occupancy_map[col][row]);
          h_threshold->Fill(amplitude, occupancy_map[col][row]);

          // Commenting out the pixel response filling
          // h_pixel_responses[col][row]->Fill(occupancy_map[col][row]);
          if(!((col == 28 && row == 7) || (col == 44 && row == 15))) { // exclude noisy
            h_response_sum->Fill(occupancy_map[col][row]);
          }

          if(i < n_dac) {
            ampl[i] = amplitude;

            occ_map_pulse[col][row][i] = occupancy_map[col][row];
          }
        }
      }

      // this counts the number of block read (0== scan_start)
      i = i + 1;
    }

    for(int col = 0; col < n_col; col++) {
      for(int row = 0; row < n_row; row++) {

        // make one histogram per pixel, but don't write it
        TH1D* h_pixel_curve =
          new TH1D(Form("h_pixel_curve_%i_%i", col, row), ";Pixel Baseline [DAC];Counts", n_dac, 0 - 0.5, n_dac - 0.5);

        for(int i = 0; i < n_dac; i++) {
          h_pixel_curve->SetBinContent(h_pixel_curve->FindBin(ampl[i]), occ_map_pulse[col][row][i]);
          occupancy_sum->Fill(col, row, occ_map_pulse[col][row][i]);
        }
        int map_index = find(ampl.begin(), ampl.end(), plotAt) - ampl.begin();
        occupancy->Fill(col, row, occ_map_pulse[col][row][map_index]);

        // Not writing individual pixel response histograms
        // h_pixel_responses[col][row]->GetXaxis()->SetRange(2,256);
        // dir_single->cd();
        // h_pixel_curve->Write();
        // h_pixel_responses[col][row]->Write();

      }
    } // col

    h_threshold->Write();
    h_response_sum->GetXaxis()->SetRange(2,256);
    h_response_sum->Write();
    occupancy->Write();
    occupancy_sum->Write();

    T->Write();
    fout->Close();

  } // if open
  else {
    std::cout << "Failed to open " << filename << std::endl;
  }
  gSystem->Exit(0);
}
