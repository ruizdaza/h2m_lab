import ROOT
import matplotlib.pyplot as plt

def load_histogram(file_path, hist_name):
    # Open the ROOT file
    root_file = ROOT.TFile.Open(file_path)
    if not root_file or root_file.IsZombie():
        raise FileNotFoundError(f"Cannot open file: {file_path}")

    # Get the histogram from the ROOT file
    histogram = root_file.Get(hist_name)
    if not histogram:
        raise ValueError(f"Cannot find histogram '{hist_name}' in file: {file_path}")

    # Convert histogram data to a list of values (except first bin, because ToT = 0 is just noise)
    values = [histogram.GetBinContent(i) for i in range(2, histogram.GetNbinsX()+1)]

    # Get the total number of entries for normalization
    n_entries = histogram.GetEntries()
    print(f"The value of entries is: {n_entries}")

    return values, n_entries

# File paths
file1 = "data/occupancyData_short_h2m2_1V2_ikrum21.root"
histogram_name = "response_sum"

# Load histograms and get their values and number of entries
values1, entries1 = load_histogram(file1, histogram_name)


# Plotting
plt.figure(figsize=(8, 6))
plt.plot(values1 , marker='o', color = 'black')
plt.xlabel("ToT [clock cycles]", fontsize=16)
plt.ylabel("Entries", fontsize=16)

plt.xlim(0, 150)


plt.show()
