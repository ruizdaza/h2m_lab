import ROOT
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

def gaussian(x, a, mu, sigma):
    """Gaussian function."""
    return a * np.exp(-(x - mu)**2 / (2 * sigma**2))

def load_histogram(file_path, hist_name):
    # Open ROOT file
    root_file = ROOT.TFile.Open(file_path)
    if not root_file or root_file.IsZombie():
        raise FileNotFoundError(f"Cannot open file: {file_path}")

    # Get histo
    histogram = root_file.Get(hist_name)
    if not histogram:
        raise ValueError(f"Cannot find histogram '{hist_name}' in file: {file_path}")

    # Get the bins
    x_values = [histogram.GetBinCenter(i) for i in range(1, histogram.GetNbinsX() + 1)]
    y_values = [histogram.GetBinContent(i) for i in range(1, histogram.GetNbinsX() + 1)]

    # Get the total number of entries for normalization
    n_entries = histogram.GetEntries()
    print(f"The value of entries is: {n_entries}")

    return np.array(x_values), np.array(y_values), n_entries

# File paths
file1 = "data/occupancyData_cal_peak1.root"
file2 = "data/occupancyData_cal_peak2.root"
histogram_name = "response_sum_electrons"
#histogram_name = "h_pixel_response_electrons_4_11"

# Load histograms and get their x and y values and number of entries
x_values1, y_values1, entries1 = load_histogram(file1, histogram_name)
x_values2, y_values2, entries2 = load_histogram(file2, histogram_name)

# Fit range. MODIFY!
fit_min_peak1, fit_max_peak1 = 1500, 1800
fit_min_peak2, fit_max_peak2 = 2000, 2300

# Select data within the fit range for the first file
mask1 = (x_values1 >= fit_min_peak1) & (x_values1 <= fit_max_peak1)
x_fit1 = x_values1[mask1]
y_fit1 = y_values1[mask1]

# Check if data is available for peak 1
if len(x_fit1) == 0 or len(y_fit1) == 0:
    raise ValueError("No data points found in the fit range for Peak 1.")

# Perform Gaussian fit for the first file for peak 1
popt1, pcov1 = curve_fit(gaussian, x_fit1, y_fit1, p0=[max(y_fit1), np.mean(x_fit1), np.std(x_fit1)])
a_fit1, mean_fit1, sigma_fit1 = popt1
perr1 = np.sqrt(np.diag(pcov1))  # errors on the fit parameters

# Select data within the fit range for the second file for peak 2
mask2 = (x_values2 >= fit_min_peak2) & (x_values2 <= fit_max_peak2)
x_fit2 = x_values2[mask2]
y_fit2 = y_values2[mask2]

# Check if data is available for peak 2
if len(x_fit2) == 0 or len(y_fit2) == 0:
    raise ValueError("No data points found in the fit range for Peak 2.")

# Perform Gaussian fit for the second file
popt2, pcov2 = curve_fit(gaussian, x_fit2, y_fit2, p0=[max(y_fit2), np.mean(x_fit2), np.std(x_fit2)])
a_fit2, mean_fit2, sigma_fit2 = popt2
perr2 = np.sqrt(np.diag(pcov2))  # errors on the fit parameters

# Plot histogram data for the first file
plt.figure(figsize=(8, 6))
plt.plot(x_values1, y_values1, marker='o', color='black', label='Peak 1')

# Plot Gaussian fit for the first file
x_fit_line1 = np.linspace(fit_min_peak1, fit_max_peak1, 1000)
y_fit_line1 = gaussian(x_fit_line1, *popt1)
plt.plot(x_fit_line1, y_fit_line1, color='orange', linestyle='--')

# Plot histogram data for the second file
plt.plot(x_values2, y_values2, marker='o', color='gray', label='Peak 2')

# Plot Gaussian fit for the second file
x_fit_line2 = np.linspace(fit_min_peak2, fit_max_peak2, 1000)
y_fit_line2 = gaussian(x_fit_line2, *popt2)
plt.plot(x_fit_line2, y_fit_line2, color='red', linestyle='--')

# Set axis labels and limits
plt.xlabel("Amplitude [$e^-$]", fontsize=16)
plt.ylabel("Entries", fontsize=16)
plt.xlim(0, 5000)

# Print fit parameters
textstr1 = '\n'.join((
    r'$\mu=%.0f \pm %.0f$' % (mean_fit1, perr1[1]),
    r'$\sigma=%.0f \pm %.0f$' % (sigma_fit1, perr1[2])))
plt.text(0.95, 0.75, textstr1, transform=plt.gca().transAxes, fontsize=12,
         verticalalignment='top', horizontalalignment='right',
         color='orange')

textstr2 = '\n'.join((
    r'$\mu=%.0f \pm %.0f$' % (mean_fit2, perr2[1]),
    r'$\sigma=%.0f \pm %.0f$' % (sigma_fit2, perr2[2])))
plt.text(0.95, 0.65, textstr2, transform=plt.gca().transAxes, fontsize=12,
         verticalalignment='top', horizontalalignment='right',
         color='red')


plt.legend(fontsize=14)
plt.show()
