import ROOT
import matplotlib.pyplot as plt
import numpy as np
import os
from scipy.optimize import curve_fit

# Threshold range. CHANGE IT!!
start_index = 140
end_index = 175

# Initialize vectors
thresholds = []
pixel_count_sums = []
errors = []

# Loop over the file indices
for i in range(start_index, end_index + 1):
    filename = f"data_analysed_200frames_400times_10ms_ikrum21_1V2_h2h2/occupancy_filename_{i}.root"

    if not os.path.exists(filename):
        print(f"Warning: File {filename} does not exist. Skipping...")
        continue

    root_file = ROOT.TFile.Open(filename)

    histogram = root_file.Get("Pixel Count Sum")

    if not histogram:
        print(f"Warning: Histogram 'Pixel Count Sum' not found in {filename}. Skipping...")
        root_file.Close()
        continue

    counts = histogram.Integral()
    error = np.sqrt(counts)

    thresholds.append(i)
    pixel_count_sums.append(counts)
    errors.append(error)

    root_file.Close()

thresholds = np.array(thresholds)
pixel_count_sums = np.array(pixel_count_sums)
errors = np.array(errors)

derivative = np.gradient(pixel_count_sums, thresholds)
second_derivative = np.gradient(derivative, thresholds)

# First plot: Counts vs Threshold with error bars

# Define s-curve
def sigmoid(x, L, x0, sigma, b):
    k = 1 / (sigma * np.sqrt(2 * np.log(2))) # compatible interpretation of sigma with the gaussian fit
    return L / (1 + np.exp(-k * (x - x0))) + b

# Select the fitting range
fit_mask = (thresholds >= 142) & (thresholds <= 149)
fit_thresholds = thresholds[fit_mask]
fit_pixel_counts = pixel_count_sums[fit_mask]

# Initial guess for the parameters: L (max value), x0 (midpoint), sigma (width), b (offset)
initial_guess_sigmoid = [np.max(fit_pixel_counts), np.mean(fit_thresholds), 1.0, np.min(fit_pixel_counts)]

# Fit the s-curve
try:
    popt_sigmoid, pcov_sigmoid = curve_fit(sigmoid, fit_thresholds, fit_pixel_counts, p0=initial_guess_sigmoid)
    L_fit, x0_fit, sigma_fit, b_fit = popt_sigmoid
    perr_sigmoid = np.sqrt(np.diag(pcov_sigmoid))

    # Generate the s-curve for plotting
    x_fit_sigmoid = np.linspace(142, 149, 300)
    y_fit_sigmoid = sigmoid(x_fit_sigmoid, *popt_sigmoid)

    # First plot: Counts vs Threshold with error bars
    plt.figure()
    plt.errorbar(thresholds, pixel_count_sums, yerr=errors, marker='o', linestyle='', capsize=5, color='black')
    plt.plot(x_fit_sigmoid, y_fit_sigmoid, color='orange', linestyle='--', label='S-curve fit')
    plt.xlabel('Threshold [DAC]', fontsize=16)
    plt.ylabel('Counts', fontsize=16)
    plt.xticks(range(start_index, end_index + 1, 5))

    # Annotate the plot with the fit parameters
    textstr = '\n'.join((
        r'$\mu=%.1f \pm %.1f$' % (x0_fit, 0.1),
        r'$\sigma=%.1f \pm %.1f$' % (2*sigma_fit, perr_sigmoid[2])))
    plt.text(0.95, 0.75, textstr, transform=plt.gca().transAxes, fontsize=12,
             verticalalignment='bottom', horizontalalignment='right',
              color='orange')

    plt.legend()

    print("Sigmoid Fit Parameters:")
    print(f"L: {L_fit:.2f} ± {perr_sigmoid[0]:.2f}")
    print(f"Mean (μ): {x0_fit:.2f} ± {perr_sigmoid[1]:.2f}")
    print(f"Sigma (σ): {sigma_fit:.2f} ± {perr_sigmoid[2]:.2f}")
    print(f"Offset (b): {b_fit:.2f} ± {perr_sigmoid[3]:.2f}")

except Exception as e:
    print(f"Sigmoid fit failed: {e}")


# Second plot: -dCounts/dThreshold vs Threshold
plt.figure()
plt.plot(thresholds, -derivative, marker='o', linestyle='-', color='black')
plt.xlabel('Threshold [DAC]', fontsize=16)
plt.ylabel('-dCounts/dThreshold', fontsize=16)
plt.xticks(range(start_index, end_index + 1, 5))

# Define Gaussian function
def gaussian(x, amplitude, mean, sigma):
    return amplitude * np.exp(- (x - mean)**2 / (2 * sigma**2))

fit_mask = (thresholds >= 142) & (thresholds <= 149)
fit_thresholds = thresholds[fit_mask]
fit_derivative = -derivative[fit_mask]

initial_guess = [np.max(fit_derivative), fit_thresholds[np.argmax(fit_derivative)], 1.0]

try:
    popt, pcov = curve_fit(gaussian, fit_thresholds, fit_derivative, p0=initial_guess)
    amplitude_fit, mean_fit, sigma_fit = popt
    perr = np.sqrt(np.diag(pcov))

    x_fit = np.linspace(142, 149, 300)
    y_fit = gaussian(x_fit, *popt)

    plt.plot(x_fit, y_fit, color='orange', linestyle='--', label='Gaussian fit')

    plt.legend()
    textstr = '\n'.join((
        r'$\mu=%.1f \pm %.1f$' % (mean_fit, perr[1]),
        r'$\sigma=%.1f \pm %.1f$' % (sigma_fit, perr[2])))
    plt.text(0.95, 0.75, textstr, transform=plt.gca().transAxes, fontsize=12,
             verticalalignment='bottom', horizontalalignment='right',
              color='orange')

    print("Gaussian Fit Parameters:")
    print(f"Amplitude (A): {amplitude_fit:.2f} ± {perr[0]:.2f}")
    print(f"Mean (μ): {mean_fit:.2f} ± {perr[1]:.2f}")
    print(f"Sigma (σ): {sigma_fit:.2f} ± {perr[2]:.2f}")
except Exception as e:
    print(f"Gaussian fit failed: {e}")

# Third plot: d²Counts/d²Threshold vs Threshold
plt.figure()
plt.plot(thresholds, second_derivative, marker='o', linestyle='-', color='black')
plt.axhline(y=0, color='gray', linestyle='--')
plt.axvline(x=mean_fit, color='orange', linestyle='--')
plt.xlabel('Threshold [DAC]', fontsize=16)
plt.ylabel('d²Counts/d²Threshold', fontsize=16)
plt.xticks(range(start_index, end_index + 1, 5))

# Fourth plot: Energy [electrons] vs Threshold [DAC]

baseline = 99  # CHANGE IT!!
k_alpha = 5890 / 3.66  # k_alpha /Ee/h
print(k_alpha)
x_points = [baseline, mean_fit]
y_points = [0, k_alpha]
x_points_error = [1.43, 0.1]
y_points_error = [3, 14]

# Manually calculate slope and intercept with errors
slope = (y_points[1] - y_points[0]) / (x_points[1] - x_points[0])
slope_err = slope * np.sqrt((x_points_error[0] / (x_points[1] - x_points[0]))**2 + (y_points_error[0] / (y_points[1] - y_points[0]))**2)
intercept = y_points[0] - slope * x_points[0]

# Generate linear fit line
x_fit_line = np.linspace(min(x_points), max(x_points), 300)
y_fit_line = slope * x_fit_line + intercept

# Plot with error bars
plt.figure()
plt.errorbar(x_points, y_points, xerr=x_points_error, yerr=y_points_error, fmt='o', capsize=5, color='black')
plt.plot(x_fit_line, y_fit_line, color='orange', linestyle='--', label='Linear fit')

# Annotate plot with the slope and its error
textstr = r'$slope=%.0f \pm %.0f \, e^-/DAC$' % (slope, slope_err)
plt.text(0.05, 0.75, textstr, transform=plt.gca().transAxes, fontsize=12,
         verticalalignment='top', horizontalalignment='left', color='orange')

plt.xlabel('Threshold [DAC]', fontsize=16)
plt.ylabel('Energy [$e^-$]', fontsize=16)
plt.legend()

# Show all plots
plt.show()
