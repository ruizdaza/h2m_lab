1. Generate the ```take_data.txt``` using ```python3 generate_script.py``` with the desired values. Currently 100 frames are taken x400. 

2. Modify in ```run_peary``` the threshold range to be scanned.

3. Modify in ```config/config.cfg``` the desired chip parameters and load the correct mask file.

4. Take data:

```
python3 run_peary.py
```

