import uproot
import matplotlib.pyplot as plt
import numpy as np

# Open ROOT files
file_tuned = uproot.open("data/tuned_result.root")
file_small = uproot.open("data/noise_scan_0.root")
file_large = uproot.open("data/noise_scan_14.root")

# Get histograms
h_tuned = file_tuned["Pixel Count Sum"]
h_small = file_small["Pixel Count Sum"]
h_large = file_large["Pixel Count Sum"]

# Convert uproot histograms to NumPy arrays
values_tuned, bin_edges_tuned = h_tuned.to_numpy()
values_small, bin_edges_small = h_small.to_numpy()
values_large, bin_edges_large = h_large.to_numpy()

# Calculate bin centers
bin_centers_tuned = (bin_edges_tuned[:-1] + bin_edges_tuned[1:]) / 2
bin_centers_small = (bin_edges_small[:-1] + bin_edges_small[1:]) / 2
bin_centers_large = (bin_edges_large[:-1] + bin_edges_large[1:]) / 2

# Create a plot using matplotlib
plt.figure(figsize=(8, 6)) # Modify for different desired figsize

# Plot histograms
plt.plot(bin_centers_tuned, values_tuned, color='red', label='Equalised', drawstyle='steps-mid', zorder=2)
plt.plot(bin_centers_small, values_small, color='darkmagenta', label='Lowest Trim Value', drawstyle='steps-mid', zorder=2)
plt.plot(bin_centers_large, values_large, color='seagreen', label='Highest Trim Value', drawstyle='steps-mid', zorder=2)

# Fill the area under the curves
plt.fill_between(bin_centers_tuned, 0, values_tuned, color='red', alpha=1, zorder=1, step='mid')
plt.fill_between(bin_centers_small, 0, values_small, color='darkmagenta', alpha=1, zorder=1, step='mid')
plt.fill_between(bin_centers_large, 0, values_large, color='seagreen', alpha=1, zorder=1, step='mid')

# Set axis labels
plt.xlabel('Threshold [DAC]', fontsize=16)
plt.ylabel('Frequency', fontsize=16)

# Set axis limits
plt.ylim(0, plt.ylim()[1])  # Set y-axis limit to start at 0
plt.xlim(60, 140)  # Set x-axis limit from 30 to 110. Modify for different baselines.

# Add legend
plt.legend(loc='upper right', fontsize='large')

# Show the plot
plt.show()
