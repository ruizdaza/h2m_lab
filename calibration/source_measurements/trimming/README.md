0) Make a folder called ```trimming``` in your folder in Caribou and in your lab PC. Inside the ```trimming``` folders, in Caribou you will need the folder ```Caribou```. In you lab PC you will need the folder ```analysis```
1) Take data for the 16 DACs in Caribou.
```
python3 run_peary.py
```

2) Copy the folders ```.bin``` files into your lab PC, in the folder ```trimming/data```. You can use ```scp```.

3) Run the analysis in your pc
```
python3 run_analysis.py
```

4) Create the mask file: tuned.mask
```
root -l -x createTrimming.C
```

5) Copy the mask file into the masks file in Caribou. You can use ```scp```.

6) Run the final measurement:
```
pearycli -c config/tuned_result.cfg -r scripts/run_tuned.src
```
7) Copy the ```.bin``` file into you lab PC, in the folder ```trimming/data```. You can use ```scp```

8) Analyse it.
```
root -l -x 'H2M_scanReader.C("data/tuned_result.bin",50)'
```

9) Plot the result.
```
python3 plotting.py
```
