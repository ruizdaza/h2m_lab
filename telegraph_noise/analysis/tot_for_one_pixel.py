import uproot
import matplotlib.pyplot as plt
import numpy as np
import glob
import os

# Get ROOT files
folder_path = "data_h2m3_1V2_thr111_dacvtpulse18_tot/"

root_files = glob.glob(os.path.join(folder_path, "tuned_result_*.root"))

# Sort files
root_files.sort(key=lambda x: int(os.path.basename(x).split('_')[-1].split('.')[0]))

means = []
errors = []

# Loop over all ROOT files
for file_path in root_files:
    file = uproot.open(file_path)

    # Extract the histogram. pixels interesting [3,4], [3,5], [14,3], [23,10]
    h_hist = file["single_pixel_plots/h_pixel_response_21_12"]
    #h_hist = file["response_sum"]
    #h_hist = file["Pixel Count Sum"]
    # Convert histogram to NumPy arrays
    values, bin_edges = h_hist.to_numpy()

    # Calculate bin centers
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2

    # Calculate the mean of the histogram
    mean = np.sum(bin_centers * values) / np.sum(values)

    # Calculate the error as mean/sqrt(mean)
    error = mean / np.sqrt(mean)

    means.append(mean)
    errors.append(error)

# Create the x-axis as the measurement number
measurements = np.arange(1, len(means) + 1)

# Create plot
plt.figure(figsize=(8, 6))
plt.errorbar(measurements, means, yerr=errors, fmt='o', linestyle='', color='blue', capsize=5, elinewidth=2, markeredgewidth=2)

plt.xlabel('Measurement number', fontsize=16)
plt.ylabel('ToT [clock cycles]', fontsize=16)

plt.xlim(0.5, len(measurements) + 0.5)

# Show the plot
plt.show()
