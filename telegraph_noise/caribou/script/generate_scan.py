with open('take_data.txt', 'w') as file: #this overwrites, change for each dac
        file.write(f"add_device H2M \n")
        file.write(f"powerOn 0 \n")
        file.write(f"configure 0 \n")
        file.write("delay 1000 \n")
        file.write("\n")

        for dac_vtpulse in range(0, 100, 1):
            file.write(f"parameterScan dac_vthr 116 117 1 1 tuned_result_{dac_vtpulse} 0 \n") #change for each dac
            file.write("\n")
