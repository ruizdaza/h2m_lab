# Run trimming

In caribou (run scan for the 16 DACs):
```
pearycli -r scripts/run_trimming.src -c config/trimming.cfg
```

In the computer (create root files and the tuned mask):
```
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_0.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_1.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_2.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_3.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_4.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_5.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_6.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_7.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_8.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_9.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_10.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_11.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_12.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_13.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_14.bin",50)'
root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("noise_scan_15.bin",50)'
```

```
root -l -x '~/Desktop/peary/devices/H2M/utils/createTrimming.C()'
```

In caribou (run the measurement with the tuned mask):
```
pearycli -r scripts/run_tuned.src -c config/tuned_result.cfg
```

In the computer (create root file):
```
noise_scan_tuned.bin

root -l -x '~/Desktop/peary/devices/H2M/utils/H2M_scanReader.C("tuned_result.bin",50)'
````

And final plotting:

```
root -l  ~/Desktop/h2m_lab/trimming/ploting.C
```
